package com.xianzaishi.couponcentrer;

import com.xianzaishi.couponcenter.client.coupon.CouponService;
import com.xianzaishi.couponcenter.component.usercoupon.UserCouponServiceImpl;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping(value = "/coupontest")
public class TestController {
  
  @Autowired
  private UserCouponServiceImpl userCouponServiceImpl;

  @Autowired
  private CouponService couponService;

  @RequestMapping(value = "/querycoupon", produces = {"application/json;charset=UTF-8"})
  @ResponseBody
  public String queryCoupon(){

    System.out.println("hello");

//    return JackSonUtil.getJson(couponService.queryCouponByID(1L));
    return JackSonUtil.getJson(couponService.getCouponById(1L));
//    return JackSonUtil.getJson(userCouponServiceImpl.getCouponByTypes(Lists.newArrayList((short)11)));
  }
}
