package com.xianzaishi.couponcenter;

import com.caucho.hessian.client.HessianProxyFactory;
import com.xianzaishi.couponcenter.client.coupon.CouponService;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.coupon.query.CouponsQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.junit.Test;

/**
 * Created by quyang on 2017/4/14.
 */
public class MyTest {

  @Test
  public void couponInsert() {

    String url = "http://localhost:8088/couponcenter/hessian/couponService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      CouponService service = (CouponService) factory.create(CouponService.class, url);

      CouponDTO couponDTO = new CouponDTO();
      couponDTO.setTitle("nihao1");
      couponDTO.setType((short) 12);//指定商品优惠
      couponDTO.setStatus((short) 1);//未使用
      couponDTO.setChannelType((short) 1);//使用渠道
      couponDTO.setGmtStart(new Date());//开始时间
      couponDTO.setGmtEnd(new Date());//失效时间
      couponDTO.setRules("使用贵阿");//使用规则
      couponDTO.setAmount(Double.parseDouble(11 + ""));//优惠金额
      couponDTO.setAmountLimit(Double.parseDouble(22 + ""));//最大优惠金额

      Result<Long> insert = service.insert(couponDTO);
      System.out.println(JackSonUtil.getJson(insert));

    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void couponQuery() {

    String url = "http://localhost:8088/couponcenter/hessian/couponService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      CouponService service = (CouponService) factory.create(CouponService.class, url);
      CouponDTO couponDTO = new CouponDTO();
      
      CouponsQuery couponQuery = new CouponsQuery();
      couponQuery.setGmtStart(format("2017-04-14 14:25:04"));
      couponQuery.setGmtEnd(format("2017-04-14 14:49:14"));
//      couponQuery.setPageNum(1);
//      couponQuery.setPageSize(1);
      Result<List<CouponDTO>> query = service.query(couponQuery);
      Result<Integer> queryCount = service.queryCount(couponQuery);
      
      System.out.println(JackSonUtil.getJson(queryCount));

//      System.out.println(JackSonUtil.getJson(query));
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void couponUpdage() {

    String url = "http://localhost:8088/couponcenter/hessian/couponService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      CouponService service = (CouponService) factory.create(CouponService.class, url);
      CouponsQuery couponQuery = new CouponsQuery();
      couponQuery.setTitle("nihao");
      Result<List<CouponDTO>> query = service.query(couponQuery);

      CouponDTO dto = query.getModule().get(0);
      dto.setRules("ddddddddddddd");

      Result<Boolean> update = service.update(dto);
      System.out.println(JackSonUtil.getJson(update));

    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }
  
  
  /**
   * 格式化时间
   * @param gmtStartString
   * @return
   */
  public Date format(String gmtStartString) {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      return sdf.parse(gmtStartString);
    } catch (Exception e) {
      e.printStackTrace();
      return null;
    }
  }
}
