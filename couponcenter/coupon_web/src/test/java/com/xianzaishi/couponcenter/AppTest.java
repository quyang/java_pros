package com.xianzaishi.couponcenter;

import java.net.MalformedURLException;
import java.util.Date;

public class AppTest {


  public static void main(String[] args) throws MalformedURLException {

    /*
     * <servlet> <!-- 配置 HessianServlet，Servlet的名字随便配置，例如这里配置成ServiceServlet-->
     * <servlet-name>ServiceServlet</servlet-name>
     * <servlet-class>com.caucho.hessian.server.HessianServlet</servlet-class>
     * 
     * <!-- 配置接口的具体实现类 --> <init-param> <param-name>service-class</param-name>
     * <param-value>gacl.hessian.service.impl.ServiceImpl</param-value> </init-param> </servlet>
     * <!-- 映射 HessianServlet的访问URL地址--> <servlet-mapping>
     * <servlet-name>ServiceServlet</servlet-name> <url-pattern>/ServiceServlet</url-pattern>
     * </servlet-mapping>
     */
    // 在服务器端的web.xml文件中配置的HessianServlet映射的访问URL地址
//     String url = "http://localhost/couponcenter/hessian/userCouponService";
//     HessianProxyFactory factory = new HessianProxyFactory();
//     UserCouponService service = (UserCouponService) factory.create(UserCouponService.class, url);// 创建IService接口的实例对象
    // String i = service.sayHello("111111");
    // System.out.println(i);
//    List<Integer> userIdList = Lists.newArrayList();
//    userIdList.add(10);
//    Result<List<BackGroundUserDTO>> userResult =
//        service.queryUserDTOByIdList(userIdList);
//    System.out.println(JackSonUtil.getJson(service.sendCouponPackage(10130l, (short) 11, 2592000000l)));
    Date now = new Date();
    System.out.println(new Date(now.getTime()
              + (30 * 24 * 3600 * 1000)));
  }
}
