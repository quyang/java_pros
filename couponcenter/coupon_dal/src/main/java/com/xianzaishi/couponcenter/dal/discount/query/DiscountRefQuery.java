package com.xianzaishi.couponcenter.dal.discount.query;

import java.util.List;

public class DiscountRefQuery {

  /**
   * 目标id列表
   */
  List<Long> targetIdList;
  
  /**
   * 关系类型
   */
  List<Short> typeList;
  
  /**
   * 当前关系状态
   */
  Short status;
  
  /**
   * 优惠id
   */
  Integer discountId;

  public List<Long> getTargetIdList() {
    return targetIdList;
  }

  public void setTargetIdList(List<Long> targetIdList) {
    this.targetIdList = targetIdList;
  }

  public List<Short> getTypeList() {
    return typeList;
  }

  public void setTypeList(List<Short> typeList) {
    this.typeList = typeList;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Integer getDiscountId() {
    return discountId;
  }

  public void setDiscountId(Integer discountId) {
    this.discountId = discountId;
  }
  
}
