package com.xianzaishi.couponcenter.dal.order.dataobject;

public class OrderTmpDO {

  private String time;
  private String sumLianlian;
  private String sumXianjin;
  private String sumPoswx;
  private String sumPoszfb;
  private String sumWx;
  private String sumZfb;
  private String sumJf;
  private String sumYhq;

  public String getTime() {
    return time;
  }

  public void setTime(String time) {
    this.time = time;
  }

  public String getSumLianlian() {
    return sumLianlian;
  }

  public void setSumLianlian(String sumLianlian) {
    this.sumLianlian = sumLianlian;
  }

  public String getSumXianjin() {
    return sumXianjin;
  }

  public void setSumXianjin(String sumXianjin) {
    this.sumXianjin = sumXianjin;
  }

  public String getSumPoswx() {
    return sumPoswx;
  }

  public void setSumPoswx(String sumPoswx) {
    this.sumPoswx = sumPoswx;
  }

  public String getSumPoszfb() {
    return sumPoszfb;
  }

  public void setSumPoszfb(String sumPoszfb) {
    this.sumPoszfb = sumPoszfb;
  }

  public String getSumWx() {
    return sumWx;
  }

  public void setSumWx(String sumWx) {
    this.sumWx = sumWx;
  }

  public String getSumZfb() {
    return sumZfb;
  }

  public void setSumZfb(String sumZfb) {
    this.sumZfb = sumZfb;
  }

  public String getSumJf() {
    return sumJf;
  }

  public void setSumJf(String sumJf) {
    this.sumJf = sumJf;
  }

  public String getSumYhq() {
    return sumYhq;
  }

  public void setSumYhq(String sumYhq) {
    this.sumYhq = sumYhq;
  }
  
  public String toString(){
    StringBuffer sb = new StringBuffer();
    sb.append("time="+this.getTime()+",").
    append("sumLianlian="+this.getSumLianlian()+",").
    append("sumXianjin="+this.getSumXianjin()+",").
    append("sumPoswx="+this.getSumPoswx()+",").
    append("sumPoszfb="+this.getSumPoszfb()+",").
    append("sumWx="+this.getSumWx()+",").
    append("sumZfb="+this.getSumZfb()+",").
    append("sumJf="+this.getSumJf()+",").
    append("sumYhq="+this.getSumYhq()+",")
    ;
    return sb.toString();
  }

}
