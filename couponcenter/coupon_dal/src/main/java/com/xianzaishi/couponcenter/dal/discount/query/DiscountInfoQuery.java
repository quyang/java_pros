package com.xianzaishi.couponcenter.dal.discount.query;

import java.util.List;

/**
 * 查询参数
 *
 * @author jianmo
 * @create 2016-12-27 下午 6:25
 **/
public class DiscountInfoQuery {

    private int type;

    private int channelType;

    private int status;

    /**
     * 实际的件数或者金额或者重量,钱按照分为单位，件数为实际件数*100
     */
    private int itemCount;

    /**
     * 优惠具体的id
     */
    private List<Integer> detailId;

    public double getItemCount() {
        return itemCount;
    }

    public void setItemCount(int itemCount) {
        this.itemCount = itemCount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getChannelType() {
        return channelType;
    }

    public void setChannelType(int channelType) {
        this.channelType = channelType;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Integer> getDetailId() {
      return detailId;
    }

    public void setDetailId(List<Integer> detailId) {
      this.detailId = detailId;
    }
}
