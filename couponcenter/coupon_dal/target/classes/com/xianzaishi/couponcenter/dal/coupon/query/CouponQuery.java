package com.xianzaishi.couponcenter.dal.coupon.query;

import java.util.Collection;
import java.util.Date;

import com.xianzaishi.couponcenter.client.usercoupon.query.Status;

public class CouponQuery {
  /**
   * 优惠券类型
   */
  private Collection<Short> types;
  
  /**
   * 优惠券id
   */
  private Collection<Long> couponIds;
  
  /**
   * 优惠券状态
   */
  private short status = Status.VALID.getValue();
  
  /**
   * 优惠券结束时间
   */
  private Date gmtEndGreater;

  public Collection<Short> getTypes() {
    return types;
  }

  public void setType(Collection<Short> types) {
    this.types = types;
  }

  public short getStatus() {
    return status;
  }

  public void setStatus(short status) {
    this.status = status;
  }

  public Date getGmtEndGreater() {
    return gmtEndGreater;
  }

  public void setGmtEndGreater(Date gmtEndGreater) {
    this.gmtEndGreater = gmtEndGreater;
  }

  public Collection<Long> getCouponIds() {
    return couponIds;
  }

  public void setCouponIds(Collection<Long> couponIds) {
    this.couponIds = couponIds;
  }

}
