package com.xianzaishi.couponcenter.dal.discount.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountRefDO;
import com.xianzaishi.couponcenter.dal.discount.query.DiscountRefQuery;

public interface DiscountRefDOMapper {
  
  /**
   * 主键查询
   * @param id
   * @return
   */
  DiscountRefDO selectByPrimaryKey(Integer id);
  
  /**
   * 根据目标id和类型，获取可能存在的优惠列表
   * @param targetIdList
   * @param id
   * @return
   */
  List<DiscountRefDO> select(@Param("query") DiscountRefQuery query);
  
  /**
   * 插入数据
   * @param record
   * @return
   */
  int insert(DiscountRefDO record);
  
  /**
   * 插入数据
   * @param record
   * @return
   */
  int batchinsert(List<DiscountRefDO> record);
  /**
   * 更新数据
   * @param record
   * @return
   */
  int updateByPrimaryKeySelective(DiscountRefDO record);
  
  /**
   * 更新数据
   * @param record
   * @return
   */
  int updateByUserList(@Param("discountDetailIds") List<Integer> discountDetailIds,@Param("userId") Long userId, @Param("status") Short status);

  /**
   * 批量获取可用的引用
   * @param discountDetailIds
   * @return
     */
  List<DiscountRefDO> batchQuery(@Param("discountDetailIds") List<Integer> discountDetailIds);
  
}