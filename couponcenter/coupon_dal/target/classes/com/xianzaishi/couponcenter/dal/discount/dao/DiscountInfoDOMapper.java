package com.xianzaishi.couponcenter.dal.discount.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountInfoDO;
import com.xianzaishi.couponcenter.dal.discount.query.DiscountInfoQuery;


public interface DiscountInfoDOMapper {
    /**
     *  根据主键删除数据库的记录,discount
     *
     * @param id
     */
    /*int deleteByPrimaryKey(Integer id);

    *//**
     *  新写入数据库记录,discount
     *
     * @param record
     *//*
    int insert(DiscountInfoDO record);

    *//**
     *  动态字段,写入数据库记录,discount
     *
     * @param record
     *//*
    int insertSelective(DiscountInfoDO record);

    *//**
     *  根据指定主键获取一条数据库记录,discount
     *
     * @param id
     *//*
    DiscountInfoDO selectByPrimaryKey(Integer id);

    *//**
     *  动态字段,根据主键来更新符合条件的数据库记录,discount
     *
     * @param record
     *//*
    int updateByPrimaryKeySelective(DiscountInfoDO record);

    *//**
     *  根据主键来更新符合条件的数据库记录,discount
     *
     * @param record
     *//*
    int updateByPrimaryKey(DiscountInfoDO record);


    */

    /**
     * 根据类型获取规则信息
     * @param query
     * @return
     */
    List<DiscountInfoDO> selectByType(@Param("query") DiscountInfoQuery query);

    /**
     * 取最适的可享受的一条优惠规则
     * @param query
     * @return
     */
    DiscountInfoDO querySuitableFirstByType(@Param("query") DiscountInfoQuery query);

    /**
     * 取最适的不可享受的一条优惠规则
     * @param query
     * @return
     */
    DiscountInfoDO queryUNSuitableFirstByType(@Param("query")  DiscountInfoQuery query);

    /**
     * 根据id取当前状态正常的优惠规则
     * @param query
     * @return
     */
    List<DiscountInfoDO> querySuitableByDetailIds(@Param("query")  DiscountInfoQuery query);
}