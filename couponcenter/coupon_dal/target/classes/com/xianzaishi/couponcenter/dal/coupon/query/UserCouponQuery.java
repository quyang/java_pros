package com.xianzaishi.couponcenter.dal.coupon.query;

import java.util.Date;
import java.util.List;

import com.xianzaishi.couponcenter.client.usercoupon.query.Status;


public class UserCouponQuery {
  /**
   * 用户编号
   */
  private long userId;
  
  /**
   * 订单编号
   */
  private long oid;
  
  /**
   * 优惠券类型
   */
  private List<Short> couponTypes;
  
  /**
   * 优惠券id组
   */
  private List<Long> couponIds;
  
  /**
   * 优惠券状态
   */
  private Short status = Status.VALID.getValue();
  
  /**
   * 优惠券截止范围开始时间
   */
  private Date couponEndTimeRangeStart;
  
  /**
   * 优惠券截止范围截止时间
   */
  private Date couponEndTimeRangeEnd;
  
  private String sortBy;

  public long getUserId() {
    return userId;
  }

  public void setUserId(long userId) {
    this.userId = userId;
  }

  public long getOid() {
    return oid;
  }

  public void setOid(long oid) {
    this.oid = oid;
  }

  public List<Short> getCouponTypes() {
    return couponTypes;
  }

  public void setCouponTypes(List<Short> couponTypes) {
    this.couponTypes = couponTypes;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public List<Long> getCouponIds() {
    return couponIds;
  }

  public void setCouponIds(List<Long> couponIds) {
    this.couponIds = couponIds;
  }

  public Date getCouponEndTimeRangeStart() {
    return couponEndTimeRangeStart;
  }

  public void setCouponEndTimeRangeStart(Date couponEndTimeRangeStart) {
    this.couponEndTimeRangeStart = couponEndTimeRangeStart;
  }

  public Date getCouponEndTimeRangeEnd() {
    return couponEndTimeRangeEnd;
  }

  public void setCouponEndTimeRangeEnd(Date couponEndTimeRangeEnd) {
    this.couponEndTimeRangeEnd = couponEndTimeRangeEnd;
  }

  public String getSortBy() {
    return sortBy;
  }

  protected void setSortBy(String sortBy) {
    this.sortBy = sortBy;
  }
  
  public void setSortById(){
    this.sortBy = "id";
  }
  
  public void setSortByAmount(){
    this.sortBy = "amount";
  }
}
