package com.xianzaishi.couponcenter.dal.order.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.couponcenter.dal.order.dataobject.OrderTmpDO;

public interface OrderTmpDOMapper {

  List<OrderTmpDO> selectByQuery(@Param("start")String start, @Param("end")String end, @Param("id")Long id);

}
