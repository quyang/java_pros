package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.greenpineyu.fel.Expression;
import com.greenpineyu.fel.FelEngine;
import com.greenpineyu.fel.FelEngineImpl;
import com.greenpineyu.fel.context.FelContext;
import com.greenpineyu.fel.function.Function;
import com.greenpineyu.fel.optimizer.Optimizer;
import com.greenpineyu.fel.parser.FelNode;

/**
 * 
 * 2016年7月16日
 */
public class ExpressionCalculatorUtil {

	private static FelEngine fel = new FelEngineImpl();
	
	private static ConcurrentMap<String, Expression> expressionCompileMap = new ConcurrentHashMap<String, Expression>();
	
	public static void addFun(Function fun) {
		fel.addFun(fun);
	}
	
	public static Object calcul(String expression,FelContext context) {
		try {
			Expression expressionCompile = expressionCompileMap.get(expression);
			if (null == expressionCompile) {
				expressionCompile = fel.compile(expression, null, new Optimizer(){
	
					@Override
					public FelNode call(FelContext arg0, FelNode arg1) {
						return arg1;
					}
					
				});
				expressionCompileMap.put(expression, expressionCompile); 
			}
			Object result =  expressionCompile.eval(context);
			return result;
		} catch (Exception e) {
			return null;
		}
	}
	
	public static Object calcul(String expression,Map<String,Double> valueMap) {
		FelContext context = fel.getContext();
		for (String key : valueMap.keySet()) {
			if (null != key) {
				context.set(key, valueMap.get(key));
			}
		}
		return calcul(expression,context);
	}
	
}
