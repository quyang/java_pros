package com.xianzaishi.couponcenter.component.coupon;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.coupon.CouponService;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.coupon.query.CouponsQuery;
import com.xianzaishi.couponcenter.client.usercoupon.query.Status;
import com.xianzaishi.couponcenter.dal.coupon.dao.CouponDOMapper;
import com.xianzaishi.couponcenter.dal.coupon.dataobject.CouponDO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("couponService")
public class CouponServiceImpl implements CouponService {

  private static final Logger log = LoggerFactory.getLogger(CouponServiceImpl.class);

  private ConcurrentHashMap<Long, CouponDO> cache = new ConcurrentHashMap<Long, CouponDO>();

  @Autowired
  private CouponDOMapper couponDOMapper;

  @Override
  public Result<Long> insert(CouponDTO couponDTO) {
    if (checkCouponParemeter(couponDTO)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "参数不正确");
    }

    log.error("数据源=" + JackSonUtil.getJson(couponDTO));

    CouponDO couponDO = new CouponDO();
    copyBeanProperties(couponDTO,couponDO);
    couponDO.setCrowdId(0L);
    couponDO.setStatus(Status.VALID.getValue());

    if (couponDOMapper.insert(couponDO) == 1) {
      return Result.getSuccDataResult(1L);
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "插入数据失败");
  }


  /**
   * 校验优惠券参数是否正确
   * 
   * @param couponDTO
   * @return
   */
  private boolean checkCouponParemeter(CouponDTO couponDTO) {
    if (null == couponDTO || StringUtils.isEmpty(couponDTO.getTitle())
        || StringUtils.isEmpty(couponDTO.getRules()) || null == couponDTO.getGmtStart()
        || null == couponDTO.getGmtEnd() || null == couponDTO.getChannelType()
        || null == couponDTO.getType()) {
      return true;
    }
    return false;
  }

  @Override
  public Result<CouponDTO> getCouponById(long id) {
    CouponDO couponDO = cache.get(id);
    if (null == couponDO) {
      couponDO = couponDOMapper.selectByPrimaryKey(id);
      if (null != couponDO) {
        cache.put(couponDO.getId(), couponDO);
      }else{
        return Result.getErrDataResult(ServerResultCode.COUPON_NOT_EXIST_ERROR, "coupon is not exist with this " + id);
      }
    }
    CouponDTO couponDTO = new CouponDTO();
    BeanCopierUtils.copyProperties(couponDO, couponDTO);
    return Result.getSuccDataResult(couponDTO);
  }

//  private CouponDO get(long id) {
//    try {
//      return couponDAO.selectByPrimaryKey(id);
//    } catch (DataAccessException e) {
//      log.error("select Coupon failed.", e);
//    }
//    return null;
//  }

  @Override
  public Result<List<CouponDTO>> getCouponsByType(short type) {
//    CouponExample example = new CouponExample();
//    example.createCriteria().andTypeEqualTo(type).andStatusEqualTo((short) 1)
//        .andGmtEndGreaterThan(new Date());
//    try {
//      return couponDAO.selectByExample(example);
//    } catch (DataAccessException e) {
//      log.error("select Coupons failed.", e);
//    }
    return null;
  }

//  @Override
//  public PageList<CouponDTO> getConpons(short[] status, int curPage, int pageSize) {
//    String statusStr = "";
//    if ((null != status) && (status.length > 0)) {
//      for (short state : status) {
//        statusStr += (String.valueOf(state) + ",");
//      }
//      statusStr = StringUtils.left(statusStr, statusStr.length() - 1);
//    } else {
//      statusStr = "-1,0,1";
//    }
//    try {
//      int total = couponDAO.countByStatus(statusStr);
//      if (total > 0) {
//        Pagination pagination = Pagination.getPagination(curPage, pageSize, total);
//        return new PageList<Coupon>(pagination, couponDAO.selectByStatus(statusStr, pagination));
//      }
//    } catch (DataAccessException e) {
//      log.error("select Coupons failed.", e);
//    }
//    return null;
//  }

  @Override
  public Result<Boolean> delete(long id) {
    return update(id, Status.INVALID);
  }

  private Result<Boolean> update(long id, Status status) {
//    Result<CouponDTO> couponResult = getCoupon(id);
//    if (null == couponResult || !couponResult.getSuccess() || null == couponResult.getModule()) {
//      return Result.getErrDataResult(couponResult.getResultCode(), couponResult.getErrorMsg());
//    }
//      coupon.setStatus(status.getValue());
//      try {
//        return couponDAO.updateByPrimaryKey(coupon) > 0;
//      } catch (DataAccessException e) {
//        log.error("update Coupon failed.", e);
//      }
    return null;
  }

  private long insert(CouponDO coupon) {
    Date now = new Date();
    coupon.setGmtCreate(now);
    coupon.setGmtModify(now);
    coupon.setStatus(Status.VALID.getValue());
    if (couponDOMapper.insert(coupon) > 0) {
      return coupon.getId();
    }
    return 0l;
  }

  @Override
  public Result<List<CouponDTO>> getCouponsByCouponTypes(Collection<Short> couponTypes) {
//    CouponExample example = new CouponExample();
//
//    Criteria criteria =
//        example.createCriteria().andStatusEqualTo((short) 1).andGmtEndGreaterThan(new Date());
//    if (couponTypes != null && !couponTypes.isEmpty()) {
//      criteria.andTypeIn(Lists.newArrayList(couponTypes));
//    } else {
//      criteria.andTypeNotIn(Lists.newArrayList((short) 77));
//    }
//    try {
//      return couponDAO.selectByExample(example);
//    } catch (DataAccessException e) {
//      log.error("select Coupons failed.", e);
//    }
    return null;
  }

  public Result<List<CouponDTO>> query(CouponsQuery couponQuery) {
    if (null == couponQuery ) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数对象为空");
    }
    List<CouponDO> couponDOList = couponDOMapper.query(couponQuery);
    List<CouponDTO> list = new ArrayList<>();
    if (null != couponDOList && CollectionUtils.isNotEmpty(couponDOList)) {
      for (CouponDO coupon : couponDOList) {
        if (null == coupon) {
          continue;
        }
        CouponDTO couponDTO1 = new CouponDTO();
        BeanCopierUtils.copyProperties(coupon,couponDTO1);
        list.add(couponDTO1);
      }
    }

    if (CollectionUtils.isEmpty(list)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询优惠券结果为空");
    }

    return Result.getSuccDataResult(list);
  }

  private void getRightCouponDO(CouponsQuery couponQuery, CouponDO couponDO) {
    couponDO.setGmtStart(couponQuery.getGmtStart());
    couponDO.setGmtEnd(couponQuery.getGmtEnd());
    couponDO.setTitle(couponQuery.getTitle());
    couponDO.setId(couponQuery.getId());
    couponDO.setStatus(couponQuery.getStatus());
    couponDO.setType(couponQuery.getType());
    couponDO.setChannelType(couponQuery.getChannelType());
    couponDO.setAmount(couponQuery.getAmount());
    couponDO.setRules(couponQuery.getRules());
    couponDO.setGmtDayDuration(couponQuery.getNum());
  }


  @Override
  public Result<Boolean> update(CouponDTO couponDTO) {
    if (null == couponDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数对象异常");
    }

    log.error("数据源对象=" + JackSonUtil.getJson(couponDTO));
    CouponDO couponDO = new CouponDO();
    copyBeanProperties(couponDTO, couponDO);
    int i = couponDOMapper.updateByPrimaryKey(couponDO);
    if (1 == i) {
      return Result.getSuccDataResult(true);
    }

    return Result.getSuccDataResult(true);
  }

  public Result<Integer> queryCount(CouponsQuery couponQuery) {
    if (null == couponQuery) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "数据对象为空");
    }
    Integer integer = couponDOMapper.queryCount(couponQuery);
    if (null == integer) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询优惠券数量失败");
    }

    return Result.getSuccDataResult(integer);
  }

  @Override
  public Result<List<CouponDTO>> getCouponByIdList(List<Long> ids) {
    if(CollectionUtils.isEmpty(ids)){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "query param is error");
    }
    List<CouponDO> couponDOs = couponDOMapper.batchSelectByIds(ids);
    if(CollectionUtils.isEmpty(couponDOs)){
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_DATA_NOT_EXIST, "data not exist with this query");
    }
    log.error(JackSonUtil.getJson(couponDOs));
    List<CouponDTO> couponDTOs = Lists.newArrayList();
    //beanCopier拷贝过来的都是null，只能用set方法了
    for(CouponDO couponDO : couponDOs){
      CouponDTO couponDTO = new CouponDTO();
      couponDTO.setAmount(couponDO.getAmount());
      couponDTO.setAmountLimit(couponDO.getAmountLimit());
      couponDTO.setChannelType(couponDO.getChannelType());
      couponDTO.setCrowdId(couponDO.getCrowdId());
      couponDTO.setGmtCreate(couponDO.getGmtCreate());
      couponDTO.setGmtDayDuration(couponDO.getGmtDayDuration());
      couponDTO.setGmtEnd(couponDO.getGmtEnd());
      couponDTO.setGmtModify(couponDO.getGmtModify());
      couponDTO.setGmtStart(couponDO.getGmtStart());
      couponDTO.setId(couponDO.getId());
      couponDTO.setRules(couponDO.getRules());
      couponDTO.setSendRules(couponDO.getSendRules());
      couponDTO.setStatus(couponDO.getStatus());
      couponDTO.setTitle(couponDO.getTitle());
      couponDTO.setType(couponDO.getType());
      couponDTOs.add(couponDTO);
    }
    return Result.getSuccDataResult(couponDTOs);
  }

  /**
   * couponDTO赋值到couponDO
   * @param couponDTO
   * @param couponDO
   */
  private void copyBeanProperties(CouponDTO couponDTO, CouponDO couponDO) {

    if (null == couponDO || null == couponDTO) {
      return ;
    }
    couponDO.setAmountLimit(couponDTO.getAmountLimit());
    couponDO.setAmount(couponDTO.getAmount());
    couponDO.setId(couponDTO.getId());
    couponDO.setType(couponDTO.getType());
    couponDO.setTitle(couponDTO.getTitle());
    couponDO.setStatus(couponDTO.getStatus());
    couponDO.setGmtStart(couponDTO.getGmtStart());
    couponDO.setGmtEnd(couponDTO.getGmtEnd());
    couponDO.setCrowdId(couponDTO.getCrowdId());
    couponDO.setGmtDayDuration(couponDTO.getGmtDayDuration());
    couponDO.setGmtCreate(couponDTO.getGmtCreate());
    couponDO.setGmtModify(couponDTO.getGmtModify());
    couponDO.setRules(couponDTO.getRules());
    couponDO.setSendRules(couponDTO.getSendRules());
    couponDO.setChannelType(couponDTO.getChannelType());
  }

  /**
   * 数据从do拷贝到dto
   * @param coupon
   * @param couponDTO1
   */
  private void copyDO2DTO(CouponDO coupon, CouponDTO couponDTO1) {
    couponDTO1.setId(coupon.getId());
    couponDTO1.setTitle(coupon.getTitle());
    couponDTO1.setRules(coupon.getRules());
    couponDTO1.setSendRules(coupon.getSendRules());
    couponDTO1.setCrowdId(coupon.getCrowdId());
    couponDTO1.setType(coupon.getType());
    couponDTO1.setGmtStart(coupon.getGmtStart());
    couponDTO1.setGmtEnd(coupon.getGmtEnd());
    couponDTO1.setGmtDayDuration(coupon.getGmtDayDuration());
    couponDTO1.setGmtCreate(coupon.getGmtCreate());
    couponDTO1.setGmtModify(coupon.getGmtModify());
    couponDTO1.setStatus(coupon.getStatus());
    couponDTO1.setAmount(coupon.getAmount());
    couponDTO1.setChannelType(coupon.getChannelType());
    couponDTO1.setAmountLimit(coupon.getAmountLimit());
  }

}
