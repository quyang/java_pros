package com.xianzaishi.couponcenter.component.tmporder;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.tmporder.OrderTmpService;
import com.xianzaishi.couponcenter.dal.order.dao.OrderTmpDOMapper;
import com.xianzaishi.couponcenter.dal.order.dataobject.OrderTmpDO;

@Service("orderTmpService")
public class OrderTmpServiceImpl implements OrderTmpService{

  private static final Logger log = LoggerFactory.getLogger(OrderTmpServiceImpl.class);
  
  @Autowired
  private OrderTmpDOMapper orderTmpDOMapper;
  
  @Override
  public String getOrderInfo(Map<String, String> input) {
    if(null == input || CollectionUtils.isEmpty(input.keySet())){
      return "请求参数为空";
    }
    String start = input.get("start");
    String end = input.get("end");
    String ids = input.get("ids");
    List<Long> idList = Lists.newArrayList();
    String idArray[] = ids.split(",");
    for(String id:idArray){
      idList.add(Long.valueOf(id));
    }
    List<OrderTmpDO> result = orderTmpDOMapper.selectByQuery(start, end, idList.get(0));
    
    log.error("[OrderTmpServiceImpl]Query result is:"+(null == result));
    if(null != result){
      log.error("[OrderTmpServiceImpl]Query result size is:"+(result.size()));
    }
    String resultStr = "";
    for(OrderTmpDO obj:result){
      resultStr = resultStr + obj.toString()+"\r\n";
    }
    log.error("[OrderTmpServiceImpl]Return result is:"+resultStr);
    return resultStr;
  }

}
