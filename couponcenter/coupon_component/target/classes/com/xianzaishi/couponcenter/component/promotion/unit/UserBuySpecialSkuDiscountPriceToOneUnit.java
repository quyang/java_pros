package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.discount.enums.DiscountRefTypeEnum;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.dal.discount.dao.DiscountInfoDOMapper;
import com.xianzaishi.couponcenter.dal.discount.dao.DiscountRefDOMapper;
import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountInfoDO;
import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountRefDO;
import com.xianzaishi.couponcenter.dal.discount.query.DiscountInfoQuery;
import com.xianzaishi.couponcenter.dal.discount.query.DiscountRefQuery;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 针对用户购买独特商品，降价做独立处理
 * 
 * @author zhancang
 */
@Component(value = "userBuySpecialSkuDiscountPriceToOneUnit")
public class UserBuySpecialSkuDiscountPriceToOneUnit extends ProcessorUnit {

  @Autowired
  private DiscountRefDOMapper discountRefDOMapper;
  
  @Autowired
  private DiscountInfoDOMapper discountInfoDOMapper;

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    List<DiscountInfoDO> userDiscountInfoDO = context.getUserDiscountList();
    if (userDiscountInfoDO == null) {
      userDiscountInfoDO = Lists.newArrayList();
      context.setUserDiscountList(userDiscountInfoDO);
    }
    List<BuySkuInfoDTO> buySkuInfoDTOs = context.getBuyInfoList();
    if(CollectionUtils.isEmpty(buySkuInfoDTOs)){
      return Result.getSuccDataResult(true);
    }
    List<Long> targetList = Lists.newArrayList();
    for (BuySkuInfoDTO buySkuInfoDTO : buySkuInfoDTOs) {
      targetList.add(buySkuInfoDTO.getSkuId());
    }
    
    targetList.add(context.getUserId());
    
    List<DiscountRefDO> refList = getUserDiscount(targetList, Arrays.asList(Short.valueOf(String.valueOf(DiscountRefTypeEnum.REF_USER.getValue())), Short.valueOf(String.valueOf(DiscountRefTypeEnum.REF_SKU.getValue()))), (short)1);
    List<DiscountInfoDO> discountList = getDiscountInfoDOList(refList, context.getUserId());
    userDiscountInfoDO.addAll(discountList);
    
    return Result.getSuccDataResult(true);
  }

  /**
   * 查询所有可用关系
   * @param targetIdList
   * @param typeList
   * @param status
   * @return
   */
  private List<DiscountRefDO> getUserDiscount(List<Long> targetIdList, List<Short> typeList,Short status){
    DiscountRefQuery query = new DiscountRefQuery();
    query.setTargetIdList(targetIdList);
    query.setTypeList(typeList);
    query.setStatus(status);
    return discountRefDOMapper.select(query);
  }

  /**
   * 根据关系查优惠列表，并且过滤出符合条件的优惠列表
   * @param
   * @return
   */
  private List<DiscountInfoDO> getDiscountInfoDOList(List<DiscountRefDO> discountRefDO, Long userId){
    List<DiscountInfoDO> result = Lists.newArrayList();
    if(CollectionUtils.isEmpty(discountRefDO)){
      return result;
    }
    
    Map<Integer,DiscountRefDO> itemRef = Maps.newHashMap();
    Map<Integer,DiscountRefDO> userRef = Maps.newHashMap();
    Map<Integer,DiscountInfoDO> discountDetail = Maps.newHashMap();
    
    List<Integer> discountIdList = Lists.newArrayList();
    for(DiscountRefDO dis:discountRefDO){
      if(dis.getRefType().intValue() == (DiscountRefTypeEnum.REF_SKU.getValue())){
        itemRef.put(dis.getDiscountDetaiId(), dis);
      }else if(dis.getRefType().intValue() == (DiscountRefTypeEnum.REF_USER.getValue())){
        userRef.put(dis.getDiscountDetaiId(), dis);
      }
      if(!discountIdList.contains(dis.getDiscountDetaiId())){
        discountIdList.add(dis.getDiscountDetaiId());
      }
    }
    
    DiscountInfoQuery query = new DiscountInfoQuery();
    query.setDetailId(discountIdList);
    List<DiscountInfoDO> queryResult = discountInfoDOMapper.querySuitableByDetailIds(query);
    for(DiscountInfoDO dicount:queryResult){
      if(!dicount.getDiscountId().equals(5)){
        continue;//默认的减至X元的活动配置id
      }
      discountDetail.put(dicount.getDiscountDetailId(), dicount);
    }
    
    if(CollectionUtils.isEmpty(itemRef.keySet()) || CollectionUtils.isEmpty(userRef.keySet())
        || CollectionUtils.isEmpty(discountDetail.keySet())){
      return result;
    }
    for(Integer detailId:discountDetail.keySet()){
      DiscountInfoDO dis = discountDetail.get(detailId);
      DiscountRefDO item = itemRef.get(detailId);
      DiscountRefDO user = userRef.get(detailId);
      if(isLegalDiscount(dis,item,user)){
        result.add(dis);
      }
    }
    
    return result;
  }

  private boolean isLegalDiscount(DiscountInfoDO discount,DiscountRefDO itemRef,DiscountRefDO userRef){
    if(null == discount || null == itemRef || null == userRef){
      return false;
    }
    if(itemRef.getRefId().intValue() != discount.getThreshold()){
      return false;//skuId不同，这里后面要字段变长的
    }
    return isLegalDiscountRefDO(itemRef) && isLegalDiscountRefDO(userRef) && isLegalDiscountInfoDO(discount);
  }
}
