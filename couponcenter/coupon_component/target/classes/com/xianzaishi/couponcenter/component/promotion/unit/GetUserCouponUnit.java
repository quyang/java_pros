package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.dal.coupon.dataobject.CouponDO;
import com.xianzaishi.couponcenter.dal.coupon.dataobject.UserCouponDO;
import com.xianzaishi.couponcenter.dal.coupon.query.UserCouponQuery;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 获取用户已经存在优惠券列表
 * @author zhancang
 */
@Component(value = "getUserCouponUnit")
public class GetUserCouponUnit extends ProcessorUnit {

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getCouponList())) {
      return Result.getSuccDataResult(true);
    }

    List<Long> userCouponIdList = Lists.newArrayList();
    for(CouponDTO coupon:context.getCouponList()){
      userCouponIdList.add(coupon.getId());
    }
    
    UserCouponQuery query = new UserCouponQuery();
    query.setCouponIds(userCouponIdList);
    query.setUserId(context.getUserId());
    query.setStatus(null);
    query.setSortByAmount();
    List<UserCouponDO> userAllCoupon = userCouponDOMapper.selectByQuery(query);
    if(CollectionUtils.isNotEmpty(userAllCoupon)){
      List<UserCouponDTO> userCouponDTOList = Lists.newArrayList();
      
      for(UserCouponDO coupon:userAllCoupon){
        UserCouponDTO couponDTO = new UserCouponDTO();
        org.springframework.beans.BeanUtils.copyProperties(coupon, couponDTO);
        userCouponDTOList.add(couponDTO);
      }
      context.setUserCouponList(userCouponDTOList);
    }
    
    return Result.getSuccDataResult(true);
  }
}
