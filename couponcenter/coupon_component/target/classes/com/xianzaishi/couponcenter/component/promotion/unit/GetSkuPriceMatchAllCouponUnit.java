package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 查询用户符合条件的第一个优惠券
 * @author zhancang
 */
@Component(value = "getSkuPriceMatchAllCouponUnit")
public class GetSkuPriceMatchAllCouponUnit extends ProcessorUnit {

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getUserCouponList())) {
      return Result.getSuccDataResult(true);
    }

//    Integer tprice = context.getSkuPriceMaps().get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0");
//    BigDecimal totalPrice = new BigDecimal("0");
//    if(null!=tprice)
//      totalPrice = new BigDecimal(String.valueOf(tprice)) ;
//    Integer priceTag64 = context.getSkuPriceMaps().get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + PromotionChainContext.TOTAL_PRICE_PRE_64tag0_KEY);
//    BigDecimal totalPriceTag64 = new BigDecimal("0");
//    if(null!=priceTag64)
//      totalPriceTag64 = new BigDecimal(String.valueOf(priceTag64));
//
//    if(context.getDiscountTotalrice()>0||(totalPrice.intValue()==0&&totalPriceTag64.intValue()>0)){
//      return Result.getSuccDataResult(true);
//    }

    List<UserCouponDTO> userCouponList = Lists.newArrayList();
    for (UserCouponDTO coupon : context.getUserCouponList()) {
      if(!checkCouponAvailable(coupon,context)){
        continue;
      }
      int discountpirce = calculate(coupon, context.getSkuPriceMaps(),0);
      if(discountpirce > 0){
        userCouponList.add(coupon);
      }
    }
    context.setCanUseCouponList(userCouponList);
    
    DiscountResultDTO discountResult = context.getDiscountResult();
    if(null == discountResult){
      discountResult = new DiscountResultDTO();
    }
    discountResult.setUserCouponList(userCouponList);
    return Result.getSuccDataResult(true);
  }
}
