package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.hive.domain.client.itf.IInventory2CDomainClient;
import com.xianzaishi.wms.hive.vo.InventoryVO;

/**
 * 查询sku信息，这里查询出的sku是准备参与优惠计价的sku，真实交易的sku可能比这里的sku要多
 * 
 * @author zhancang
 */
@Component(value = "querySkuUnit")
public class QuerySkuUnit extends ProcessorUnit {
  @Autowired
  private IInventory2CDomainClient iInventory2CDomainClient;


  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    String errorInfo = "";
    List<BuySkuInfoDTO> buySkuInfoDTOList = context.getBuyInfoList();
    if (CollectionUtils.isEmpty(buySkuInfoDTOList)) {
      errorInfo = "[PromotionProcessorUnit]Input parameter skuIdList is empty,return null";
      log.error(errorInfo);
      return Result.getErrDataResult(-1, errorInfo);
    }
    if (MapUtils.isNotEmpty(context.getSkuMaps()))
      return Result.getSuccDataResult(true);
    List<Long> skuIds = Lists.newArrayList();
    for (BuySkuInfoDTO sku : buySkuInfoDTOList) {
      if(skuIds.contains(sku.getSkuId())){
        continue;
      }
      skuIds.add(sku.getSkuId());
    }

    boolean saleIsOnline = (context.getPayChannel() == 1);

    if (saleIsOnline) {//线上交易时，会扣除掉不能下单或者不能加购的商品，后续会扣除这些商品的价格。这里获取的商品，会为后面计算总价和折扣价做计算，所以即使少算了商品，只会少算折扣价，交易不使用这里的总价，只会使用折扣价格
      // 根据库存和是否可交易来获取可交易的skuIds
      Result<List<ItemSkuTagsDTO>> itemListResult = skuService.querySkuTagsDetail(skuIds);
      if (null == itemListResult || !itemListResult.getSuccess()
          || null == itemListResult.getModule() || itemListResult.getModule().size() != skuIds.size()) {
        log.error("[PromotionProcessorUnit]query sku Tag return null,skuIds is:" + skuIds);
        return null;
      }
      List<ItemSkuTagsDTO> itemSkuTagsDTOList = itemListResult.getModule();
      final Map<Long, ItemSkuTagsDTO> skuTagM = Maps.newHashMap();
      for (ItemSkuTagsDTO itemSkuTagsDTO : itemSkuTagsDTOList) {
        skuTagM.put(itemSkuTagsDTO.getSkuId(), itemSkuTagsDTO);
      }
      
      CollectionUtils.filter(skuIds, new Predicate() {
        @Override
        public boolean evaluate(Object input) {
          Long skuId = (Long) input;
          ItemSkuTagsDTO itemSkuTagsDTO = skuTagM.get(skuId);
          if (!itemSkuTagsDTO.getMayPlus() || !itemSkuTagsDTO.getMayOrder())
            return false;
          return true;
        }
      });
    }

    Result<List<ItemCommoditySkuDTO>> skuResultList =
        skuService.queryItemSkuList(SkuQuery.querySkuBySkuIds(skuIds));
    if (null == skuResultList || !skuResultList.getSuccess()
        || skuResultList.getModule().size() != skuIds.size()) {
      log.error("[PromotionProcessorUnit]query sku return null,skuIds is:" + skuIds);
      return null;
    }
    final List<ItemCommoditySkuDTO> itemCommoditySkuDTOs = skuResultList.getModule();

    if (saleIsOnline) {
      SimpleResultVO<List<InventoryVO>> storeResult = null;
      try {
        // 根据库存来判断
        storeResult = iInventory2CDomainClient.batchGetInventoryByGovAndSKU(1L, skuIds);
      } catch (Exception e) {
        log.error("查询库存异常" + e.getMessage());
      }
      if (null == storeResult || !storeResult.isSuccess() || null == storeResult.getTarget()) {
        log.error("[PromotionProcessorUnit]query sku Inventory return null,skuIds is:" + skuIds);
        return null;
      }
      List<InventoryVO> inventoryVOList = storeResult.getTarget();
      final Map<Long, InventoryVO> skuInventoryM = Maps.newHashMap();
      for (InventoryVO inventoryVO : inventoryVOList) {
        skuInventoryM.put(inventoryVO.getSkuId(), inventoryVO);
      }

      CollectionUtils.filter(itemCommoditySkuDTOs, new Predicate() {
        @Override
        public boolean evaluate(Object input) {
          ItemCommoditySkuDTO itemCommoditySkuDTO = (ItemCommoditySkuDTO) input;
          InventoryVO inventoryVO = skuInventoryM.get(itemCommoditySkuDTO.getSkuId());
          if (!isInventoryEnough(inventoryVO))
            return false;
          return true;
        }
      });
    }

    final Map<Long, ItemCommoditySkuDTO> skuM = Maps.newHashMap();
    for (ItemCommoditySkuDTO sku : itemCommoditySkuDTOs) {
      skuM.put(sku.getSkuId(), sku);
    }

    CollectionUtils.filter(buySkuInfoDTOList, new Predicate() {
      @Override
      public boolean evaluate(Object input) {
        BuySkuInfoDTO buySkuInfoDTO = (BuySkuInfoDTO) input;
        ItemCommoditySkuDTO temCommoditySkuDTO = skuM.get(buySkuInfoDTO.getSkuId());
        if (null == temCommoditySkuDTO)
          return false;
        return true;
      }
    });
    context.setSkuMaps(skuM);
    return Result.getSuccDataResult(true);
  }

  /**
   * 判断库存是否足够
   * 
   * @return
   */
  private boolean isInventoryEnough(InventoryVO inventoryVO) {
    if (null == inventoryVO || inventoryVO.getNumber() == 0)
      return false;
    return true;
  }

}
