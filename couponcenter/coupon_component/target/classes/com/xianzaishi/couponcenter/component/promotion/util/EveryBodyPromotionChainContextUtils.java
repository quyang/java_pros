package com.xianzaishi.couponcenter.component.promotion.util;


import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * 获取每个用户对应得PromotionContext
 *
 * @author jianmo
 * @create 2017-01-02 上午 10:41
 **/
public class EveryBodyPromotionChainContextUtils {

    private static final ConcurrentMap<Long,PromotionChainContext> contextMap = new ConcurrentHashMap<>();

    public static PromotionChainContext initPromotionChainContext(Long uuid,PromotionChainContext context){
        if(uuid==null||context==null) return null;
        contextMap.put(uuid,context);
        return context;
    }

    public static PromotionChainContext getPromotionChainContext(Long uuid){
        return contextMap.get(uuid);
    }

    /**
     * 用户改变skuid数的时候清空，即在购物车中操作sku数时都要执行此动作
     * @param userId
     */
    public static void removePromotionChainContext(Long userId){
        if(userId == null) return ;
        PromotionChainContext context1 = getPromotionChainContext(userId);
        if(context1==null)return ;
        contextMap.remove(userId);
    }



}
