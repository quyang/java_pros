package com.xianzaishi.couponcenter.component.promotion.unit;

import com.xianzaishi.couponcenter.client.discount.dto.DiscountCalResultDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountInfoDTO;
import com.xianzaishi.couponcenter.client.discount.enums.DiscountTypeEnum;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.dal.discount.dao.DiscountInfoDOMapper;
import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountInfoDO;
import com.xianzaishi.couponcenter.dal.discount.query.DiscountInfoQuery;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 满额减
 *
 * @author jianmo
 * @create 2016-12-27 下午 5:13
 **/
@Component(value = "fullReduceDiscountUnit")
public class FullReduceDiscountUnit extends ProcessorUnit {

    @Autowired
    private DiscountInfoDOMapper discountInfoDOMapper;

    @Override
    public Result<Boolean> process(PromotionChainContext context) {
//        context = this.getChainContext(context);
        Map<String,DiscountCalResultDTO> ffullNUseMDiscount = context.getFullNUseMDiscount();
        if(ffullNUseMDiscount==null){
            ffullNUseMDiscount = new HashMap<>();
        }
        //获取现在的总金额
       Integer tprice = context.getSkuPriceMaps().get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0");
        BigDecimal totalPrice = new BigDecimal("0");
        if(null!=tprice){
          totalPrice = new BigDecimal(String.valueOf(tprice)) ;
        }
       Integer priceTag64 = context.getSkuPriceMaps().get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + PromotionChainContext.TOTAL_PRICE_PRE_64tag0_KEY);
        BigDecimal totalPriceTag64 = new BigDecimal("0");
       if(null!=priceTag64)
           totalPriceTag64 = new BigDecimal(String.valueOf(priceTag64));
//        double totalPrice = Arith.div(Double.valueOf(String.valueOf(totalPriceR)),Double.valueOf("100"),2);
        //在获取参与活动的优惠总金额
        BigDecimal dicountTotalPrice = new BigDecimal(String.valueOf(context.getDiscountTotalrice()));
        //参与活动后的金额
        BigDecimal currPrice = totalPrice.subtract(dicountTotalPrice);

        DiscountCalResultDTO discountCalResultDTO =null;
        //以当前金额获取匹配的活动规则
        DiscountInfoDO discountInfoDO = null;
        if(currPrice.intValue() > 0){
          discountInfoDO = getDiscountMatcher(currPrice.intValue());
        }
        if(null!=discountInfoDO) {
            //计算优惠信息
            discountCalResultDTO = calNUseMDiscount(discountInfoDO,currPrice.intValue(),totalPrice,dicountTotalPrice,totalPriceTag64);
            if(discountCalResultDTO.isGetDiscount()){
                dicountTotalPrice= dicountTotalPrice.add(new BigDecimal(String.valueOf(discountCalResultDTO.getDiscountPrice())));
            }
        }else{
            discountInfoDO = getUnDiscountMatcher(currPrice.intValue());
            if(discountInfoDO!=null)
            //计算差价信息
            discountCalResultDTO = calNUseMUnDiscount(discountInfoDO, currPrice,dicountTotalPrice,totalPriceTag64);
            else{
                discountCalResultDTO=calNUseMDiscount(currPrice,totalPrice,dicountTotalPrice,totalPriceTag64);
            }
        }
        ffullNUseMDiscount.put(DiscountTypeEnum.FULL_REDUCE.getCode()+DiscountTypeEnum.FULL_REDUCE.getValue(),discountCalResultDTO);
        context.setFullNUseMDiscount(ffullNUseMDiscount);
        context.setDiscountTotalrice(dicountTotalPrice.intValue());
//        this.setChainContext(context);
        return Result.getSuccDataResult(true);
    }

    /**
     * 获取可享受的最近优惠-即可享受的最高优惠
     * @param
     * @return
     */
    private DiscountInfoDO getDiscountMatcher(int currPrice){
        DiscountInfoQuery query = new DiscountInfoQuery();
        query.setType(DiscountTypeEnum.FULL_REDUCE.getValue());
        query.setItemCount(currPrice);
        DiscountInfoDO  discountInfoDOs = discountInfoDOMapper.querySuitableFirstByType(query);
        return discountInfoDOs;
    }

    /**
     * 获取不可享受的最近优惠-即不可享受的最低优惠
     * @param currPrice
     * @return
     */
    private DiscountInfoDO getUnDiscountMatcher(int currPrice){
        DiscountInfoQuery query = new DiscountInfoQuery();
        query.setType(DiscountTypeEnum.FULL_REDUCE.getValue());
        query.setItemCount(currPrice);
        DiscountInfoDO  discountInfoDOs = discountInfoDOMapper.queryUNSuitableFirstByType(query);
        return discountInfoDOs;
    }


    /**
     * 计算可享受到的优惠信息
     * @param discountInfoDO
     * @param currPrice 优惠后的金额
     * @param totalPrice 总金额
     * @param dicountTotalPrice 以优惠金额
     * @return
     */
    private DiscountCalResultDTO calNUseMDiscount(DiscountInfoDO discountInfoDO ,int currPrice, BigDecimal totalPrice, BigDecimal dicountTotalPrice, BigDecimal totalPriceTag64 ){
        DiscountCalResultDTO resultDTO = new DiscountCalResultDTO();
        //计算公式：优惠后的价格；原价格；优惠价格原价
        BigDecimal discountValue =new BigDecimal(String.valueOf(discountInfoDO.getDiscountValue())) ;
        BigDecimal totalDiscountPrice = dicountTotalPrice.add(discountValue);
        //        转换数据
        DiscountInfoDTO discountInfoDTO = new DiscountInfoDTO();
        BeanCopierUtils.copyProperties(discountInfoDO,discountInfoDTO);
        resultDTO.setDiscountInfoDTO(discountInfoDTO);
        resultDTO.setGetDiscount(true);
        resultDTO.setItemCount(String.valueOf(currPrice));
        resultDTO.setOriginPrice(totalPrice.add(totalPriceTag64).intValue());
        resultDTO.setDiscountPrice(totalDiscountPrice.intValue());
        resultDTO.setSingleDiscountPrice(discountValue.intValue());
        resultDTO.setTotalDiscountPrice(totalDiscountPrice.intValue());
        resultDTO.setCurrPrice(totalPrice.add(totalPriceTag64).subtract(totalDiscountPrice).intValue());
        return resultDTO;

    }

    private DiscountCalResultDTO calNUseMDiscount(BigDecimal currPrice, BigDecimal totalPrice, BigDecimal dicountTotalPrice, BigDecimal totalPriceTag64){
        DiscountCalResultDTO resultDTO = new DiscountCalResultDTO();
        if(dicountTotalPrice.intValue()!=0)
        resultDTO.setGetDiscount(true);
        resultDTO.setOriginPrice(totalPrice.add(totalPriceTag64).intValue());
        resultDTO.setDiscountPrice(dicountTotalPrice.intValue());
        resultDTO.setCurrPrice(currPrice.add(totalPriceTag64).intValue());
        resultDTO.setTotalDiscountPrice(dicountTotalPrice.intValue());
        return resultDTO;
    }


    /**
     * 计算不可享受优惠时，还差多少可享受优惠
     * @return
     */
    private DiscountCalResultDTO calNUseMUnDiscount(DiscountInfoDO discountInfoDO,BigDecimal currPrice, BigDecimal dicountTotalPrice, BigDecimal totalPriceTag64){
        DiscountCalResultDTO resultDTO = new DiscountCalResultDTO();
        //计算公式：初始价格；还差价；享受优惠价；
//        double discountValue = discountInfoDO.getDiscountValue();
        //可享受优惠的金额
        BigDecimal threshold =  new BigDecimal(String.valueOf(discountInfoDO.getThreshold()));
        BigDecimal currPriceB = new BigDecimal(String.valueOf(currPrice));

        //还差多少钱
        BigDecimal differenceNum = threshold.subtract(currPriceB);

        //  转换数据
        DiscountInfoDTO discountInfoDTO = new DiscountInfoDTO();
        BeanCopierUtils.copyProperties(discountInfoDO,discountInfoDTO);
        resultDTO.setDiscountInfoDTO(discountInfoDTO);
        resultDTO.setGetDiscount(false);
        resultDTO.setDiscountPrice(differenceNum.intValue());
        resultDTO.setItemCount(String.valueOf(currPrice));
        resultDTO.setCurrPrice(currPrice.add(totalPriceTag64).intValue());
        resultDTO.setOriginPrice(currPrice.add(totalPriceTag64).intValue());
        resultDTO.setTotalDiscountPrice(dicountTotalPrice.intValue());
        return resultDTO;
    }

}
