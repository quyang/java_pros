package com.xianzaishi.couponcenter.component.common;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;

/**
 * Results
 * @author siyuan.leejh
 *
 */
public abstract class Results {
	
	public static final Result<Boolean> BOOLEAN_SUCCESS;
	public static final Result<Boolean> BOOLEAN_FAILURE;
	
	static {
		BOOLEAN_SUCCESS = booleanResult(true);
		BOOLEAN_FAILURE = booleanResult(false);
	}
	
	private static Result<Boolean> booleanResult(boolean bool) {
		return getSuccessResult(bool);
	}
	
	private static <T> Result<T> getSuccessResult(T data) {
		Result<T> result = new Result<T>();
	    result.setModule(data);
	    result.setSuccess(true);
	    result.setResultCode(ServerResultCode.SUCCESS_CODE);
	    return result;
	}
}
