package com.xianzaishi.couponcenter.component.promotion.unit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountDetailDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.component.common.Results;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.dal.coupon.dataobject.UserCouponDO;
import com.xianzaishi.itemcenter.client.itemsku.constants.SkuConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO.SkuTagDefinitionConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.SkuTagDetail;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 单品特价限购
 * 
 * @author siyuan
 */
@Component(value = "specialSkuDiscountUnit")
public class SpecialSkuDiscountUnit extends ProcessorUnit {

	// TODO: Move to base class -> ProcessorUnit as a CONSTANT
	public static final String DISCOUNT_TOTAL_PRICE_KEY = PromotionChainContext.TOTAL_PRICE_PRE_KEY
			+ "0";

	// TODO: Move to base class -> ProcessorUnit as a CONSTANT
	public static final String NO_DISCOUNT_TOTAL_PRICE_KEY = PromotionChainContext.TOTAL_PRICE_PRE_KEY
			+ PromotionChainContext.TOTAL_PRICE_PRE_64tag0_KEY;

	private static final int SKU_TYPE = CouponDTO.SPECIAL_SKU_DISCOUNT_COUPON_TYPE;
	/**
	 * 优惠名称
	 */
	private static final String DISCOUNT_NAME = "单品特价限购";

	@Override
	public Result<Boolean> process(PromotionChainContext context) {
		Map<Long, ItemCommoditySkuDTO> skuM = context.getSkuMaps();
		int originPriceTotal = 0; // 所有特价限购单品折扣前的总价
		int discountedPriceTotal = 0; // 所有特价限购单品折扣后的总价
		
		boolean exist = false;
		// 单品特价限购的sku集合
		Set<Long> skuIds = new HashSet<Long>();
		for (BuySkuInfoDTO skuInfo : context.getBuyInfoList()) {
			ItemCommoditySkuDTO sku = skuM.get(skuInfo.getSkuId());
			if (sku == null) {
				continue;
			}

			String skuTagContents = sku.queryTagContent();
			@SuppressWarnings("unchecked")
			List<SkuTagDetail> skuTagDetails = JackSonUtil.jsonToList(
					skuTagContents, SkuTagDetail.class);
			if (CollectionUtils.isEmpty(skuTagDetails)) {
				continue;
			}
			SkuTagDetail tagDetail = null;
			for (SkuTagDetail skuTagDetail : skuTagDetails) {
				if (skuTagDetail.getTagId().equals(
						SkuTagDefinitionConstants.PROMOTION_TAG)) {
					tagDetail = skuTagDetail;
					break;
				}
			}
			if (null == tagDetail) {
				continue;
			}
			String tagRule = tagDetail.getTagRule();
			if (StringUtils.isNotBlank(tagRule)) {
				List<String> couponIds = Arrays.asList(tagRule.split(","));
				if (CollectionUtils.isEmpty(couponIds)) {
					continue;
				}
				List<Long> ids = tranform(couponIds);
				List<UserCouponDO> couponList = super
						.getUserCouponByIdList(ids);
				if (couponList.isEmpty()) {
					continue;
				}

				UserCouponDO coupon = getMatchUserCouponByType(couponList);
				if (coupon == null) {
					continue;
				}
				
//				// 校验渠道
//				if ((coupon.getChannelType() != 0 && 
//						!Objects.equal(coupon.getChannelType(), context.getPayChannel()))) {
//					continue;
//				}
				if(!super.checkCouponAvailable(coupon, context,false)){
				  continue;
				}
				
				exist = true;
				skuIds.add(skuInfo.getSkuId());
				int couponReduction = calcCouponReduction(coupon, sku, context);
				// 计入优惠总价，根据优惠总价判断“总价满减”资格
				accumulateToQuotaSum(coupon, sku, context, couponReduction);

				int couponVal = sku.getDiscountPrice() - couponReduction;
				int originlPrice = getOrinalPrice(sku, skuInfo.getBuyCount(),
						context);
				// 记录一笔特价限购单品优惠明细
				
				int disPrice = couponVal*getSkuCount(
						sku, context.getPayChannel(), skuInfo.getBuyCount());
				recordDiscountDetail(disPrice, originlPrice,
						skuInfo.getSkuId(), context, ids);

				// 因为tagRule中记录的是user_coupon.id，而不是user_coupon.coupon_id
				long couponId = coupon.getCouponId();
				// 计入指定优惠券，当前积累的额度（满M减N的优惠券）
				accumulateToQuotaPerCoupon(
						couponId,
						couponVal,
						getSkuCount(sku, context.getPayChannel(),
								skuInfo.getBuyCount()), context);

				originPriceTotal += originlPrice;
				discountedPriceTotal += getDiscountedPrice(couponVal, sku,
						skuInfo.getBuyCount(), context);
			}
		}
		if (exist) {
			// 记录一笔特价限购单品总类优惠
			recordDiscountTotalDetail(originPriceTotal, discountedPriceTotal,
					context);
			// 设置最终要付的钱
			setDiscountedTotal(skuIds, context);
		}
		
		return Results.BOOLEAN_SUCCESS;
	}

	public static void main(String args[]){
	  Short i = 1;
	  int j = 1;
	  System.out.println(Objects.equal(i, j));
	}
	private void recordDiscountTotalDetail(int originPriceTotal,
			int discountedPriceTotal, PromotionChainContext context) {
		DiscountDetailDTO totalDetail = new DiscountDetailDTO();
		totalDetail.setOriginPrice(originPriceTotal);
		totalDetail.setDiscountPrice(discountedPriceTotal);
		totalDetail.setDiscountName(DISCOUNT_NAME);
		totalDetail.setType((short) SKU_TYPE);

		DiscountResultDTO discountResult = context.getDiscountResult();
		if (null == discountResult) {
			discountResult = new DiscountResultDTO();
		}
		List<DiscountDetailDTO> disList = discountResult.getTotalDiscount();
		if (CollectionUtils.isEmpty(disList)) {
			disList = Lists.newArrayList();
		}
		disList.add(totalDetail);// 总优惠中记录一条
		discountResult.setTotalDiscount(disList);// 记录总优惠，合并一个类型的优惠
		
		context.setDiscountResult(discountResult);
	}

	private void recordDiscountDetail(int disPrice, int originalPrice,
			long skuId, PromotionChainContext context, List<Long> discountIds) {
		DiscountDetailDTO disDetail = buildDiscountDetail(disPrice,
				originalPrice, skuId, context, discountIds);

		DiscountResultDTO discountResult = context.getDiscountResult();
		if (discountResult == null) {
			discountResult = new DiscountResultDTO();
		}
		Map<Long, List<DiscountDetailDTO>> discount = discountResult
				.getDiscountDetailMap();
		if (null == discount) {
			discount = Maps.newHashMap();
		}
		List<DiscountDetailDTO> disList = discount.get(disDetail.getSkuId());
		if (disList == null) {
			disList = Lists.newArrayList();
		}

		disList.add(disDetail);
		discount.put(disDetail.getSkuId(), disList);
		discountResult.setDiscountDetailMap(discount);
	}
	
	/*
	 * 计算单品特价限购优惠了多少钱。单位是分
	 */
	private void setDiscountedTotal(Set<Long> skuIds,
			PromotionChainContext context) {
		DiscountResultDTO discountResult = context.getDiscountResult();
		Map<Long, List<DiscountDetailDTO>> discount = discountResult
				.getDiscountDetailMap();
		if (null == discount) {
			return;
		}

		Integer totalPay = discountResult.getDiscountPrice();
		boolean totalPayIsNull = (totalPay == null);
		
		int reductionTotal = 0;
		for (long skuId : skuIds) {
			List<DiscountDetailDTO> details = discount.get(skuId);
			if (details != null) {
				int specialSkuPrice = 0; // 单品特价价格
				int skuOriginPrice = 0; // 原价
				int minPrice = 0; // 一个单品可以享受多个优惠价格，此处取最优惠的价格
				for (DiscountDetailDTO detail : details) {
					if (detail.getType() != SKU_TYPE) {
						if (minPrice == 0) {
							minPrice = detail.getDiscountPrice();
						} else {
							if (minPrice > detail.getDiscountPrice()) {
								minPrice = detail.getDiscountPrice();
							}
						}
					} else {
						specialSkuPrice = detail.getDiscountPrice();
						skuOriginPrice = detail.getOriginPrice();
					}
				}

				if (specialSkuPrice == 0) {
					continue;
				}
				int reduction = 0;
				if (minPrice == 0) {
					reduction = (specialSkuPrice - skuOriginPrice);
					if (reduction > 0) {
						reduction = 0;
					}
				} else {
					reduction = (specialSkuPrice - minPrice);
					if (reduction > 0) {
						reduction = 0;
					}
				}
				// 总优惠价
				reductionTotal += reduction;
				
			}
		}
		if (totalPayIsNull) {
			discountResult.setDiscountPrice(discountResult.getOrinalPrice() + reductionTotal);
		} else {
			discountResult.setDiscountPrice(discountResult.getDiscountPrice() + reductionTotal);
		}
		
	}

	private UserCouponDO getMatchUserCouponByType(List<UserCouponDO> coupons) {
		UserCouponDO couponRet = null;
		for (UserCouponDO coupon : coupons) {
			int channelType = coupon.getCouponType();
			if (SKU_TYPE == channelType) {
				if (couponRet != null) {
					log.error(
							"[GetSpecialSkuDiscountUnit] 不能为一个商品配置多个’单品特价限购’优惠；当前的coupon.coupon_id为：{}",
							coupon.getCouponId());
					continue;
				}
				couponRet = coupon;
			}
		}
		return couponRet;
	}

	private int getSkuCount(ItemCommoditySkuDTO sku, int payChannel,
			String buyCountStr) {
		// 称重类型的，线下传来的是称重重量
		if (sku.getIsSteelyardSku() && payChannel == 2) {
			return 1;
		}
		return new BigDecimal(buyCountStr).intValue();
	}

	private int getDiscountedPrice(int couponVal, ItemCommoditySkuDTO sku,
			String buyCountStr, PromotionChainContext context) {
		return couponVal
				* getSkuCount(sku, context.getPayChannel(), buyCountStr);
	}

	private int getOrinalPrice(ItemCommoditySkuDTO sku, String buyCountStr,
			PromotionChainContext context) {
		Integer skuPrice = sku.getDiscountPrice();
		Integer skuUnitPrice = sku.getDiscountUnitPrice();
		Integer skuTotalPrice = 0;

		// 称重类型的，线下传来的是称重重量
		if (sku.getIsSteelyardSku() && context.getPayChannel() == 2) {
			skuTotalPrice = new BigDecimal(buyCountStr).multiply(
					new BigDecimal(skuUnitPrice)).intValue();
		} else {
			skuTotalPrice = new BigDecimal(buyCountStr).multiply(
					new BigDecimal(skuPrice)).intValue();
		}
		return skuTotalPrice;
	}

	// 构建优惠明细项
	private DiscountDetailDTO buildDiscountDetail(int disPrice,
			int orinalPrice, long skuId, PromotionChainContext context,
			List<Long> discountIdList) {
		DiscountDetailDTO disDetail = new DiscountDetailDTO();
		disDetail.setChannelType((short) context.getPayChannel());
		disDetail.setDiscountId(0L);
		disDetail.setDiscountName(DISCOUNT_NAME);
		disDetail.setDiscountPrice(disPrice);
		disDetail.setIsGetDiscount(true);
		disDetail.setItemId(0L);
		disDetail.setNextDiscountDetail("");
		disDetail.setOriginPrice(orinalPrice);
		disDetail.setIgnorePromotionPrice(orinalPrice);
		disDetail.setSkuId(skuId);
		disDetail.setThreshold(orinalPrice);
		disDetail.setType((short) SKU_TYPE);
		if (CollectionUtils.isNotEmpty(discountIdList)) {
			disDetail.setDiscountIds(discountIdList);
		}
		return disDetail;
	}

	private void accumulateToQuotaSum(UserCouponDO coupon, ItemCommoditySkuDTO sku,
			PromotionChainContext context, int couponReduction) {
		Map<String, Integer> skuPrices = context.getSkuPriceMaps();

		// 是否记录优惠
		boolean countToDiscountTotalPrice = !sku
				.skuContainTag(SkuConstants.SKU_IGNORE_PROMOTION);
		/*
		 * 该单品会计入优惠总价， 注意前面已经将该单品折扣后的价格计入优惠总价（FIXME:
		 * 怎么判断chain的pre已经对优惠进行扣减，防止重复扣减）， 所以，此处需要再次扣除商品优惠额：
		 */
		if (countToDiscountTotalPrice) {
			addPrice((-couponReduction), DISCOUNT_TOTAL_PRICE_KEY, skuPrices);

		} else {
			addPrice((-couponReduction), NO_DISCOUNT_TOTAL_PRICE_KEY, skuPrices);
		}
	}

	private void accumulateToQuotaPerCoupon(long couponId, int couponValYuan, int skuCount,
			PromotionChainContext context) {
		Map<String, Integer> skuPriceM = context.getSkuPriceMaps();
		if (skuPriceM == null) {
			skuPriceM = Maps.newHashMap();
		}

		String key = PromotionChainContext.TOTAL_PRICE_PRE_ID_KEY + couponId;
		int skuTotalPrice = couponValYuan * skuCount;

		Integer addPrice = skuPriceM.get(key);
		if (null == addPrice || addPrice == 0) {
			addPrice = skuTotalPrice;
		} else {
			addPrice = addPrice + skuTotalPrice;
		}
		skuPriceM.put(key, addPrice);
		if (context.getSkuPriceMaps() == null) {
			context.setSkuPriceMaps(skuPriceM);
		}

	}

	// 返回优惠了多少
	private int calcCouponReduction(UserCouponDO coupon,
			ItemCommoditySkuDTO sku, PromotionChainContext context) {
		// 线下称重商品不参加单品特价优惠（不可能为每个称重商品单独设置tag吧...)
		if (isOfflineSteelyard(sku, context.getPayChannel())) {
			return 0;
		}

		// 进来先检查对应优惠是否可用
		if (!couponAvaliable(coupon, context)) {
			return 0;
		}

		// 注意这里的优惠券的面值其实时单品特价的折扣后的真实售价,单位为元
		double couponVal = coupon.getAmount();
		// 单品单价，单位分
		int couponCentVal = tranformToCent(couponVal);
		// 单品售价(商品系统传过来的值，设置商品时设置的促销价格）， 此处以该值作为用户需要支付的起始值
		int currPrice = sku.getDiscountPrice();
		// 优惠了多少分
		int currCouponedPrice = (currPrice - couponCentVal);
		if (currCouponedPrice < 0) { // 当前价格比特惠价格便宜，保持当前价格不变
			currCouponedPrice = 0;
		}
		return currCouponedPrice;
	}

	private boolean couponChannelMatch(UserCouponDTO coupon,
			PromotionChainContext context) {
		if ((coupon.getChannelType() == 2 && context.getPayChannel() == 1)
				|| (coupon.getChannelType() == 1 && context.getPayChannel() == 2)) {
			return false;
		}
		return true;
	}

	/*
	 * TODO Move to base class -> ProcessUnit Cause of
	 * ProcessUnit#checkCouponAvailable method is a general method that match
	 * coupon to user. To split one method to many specific method to make
	 * multiplexing. In this scene, we need not to match coupon to user. cause
	 * everyone can enjoy the promotion.
	 */
	private boolean couponAvaliable(UserCouponDTO coupon,
			PromotionChainContext context) {
		if (couponChannelMatch(coupon, context)) {
			return super.checkCouponStatusAvailable(coupon);
		}
		return false;
	}

	// 是否线下称重商品
	private boolean isOfflineSteelyard(ItemCommoditySkuDTO sku, int payChannel) {
		return sku.getIsSteelyardSku() && payChannel == 2;
	}

	private List<Long> tranform(List<String> ids) {
		Assert.notEmpty(ids);
		List<Long> longIds = new ArrayList<Long>(ids.size());
		try {
			for (String id : ids) {
				longIds.add(Long.parseLong(id.trim()));
			}
		} catch (RuntimeException e) {
			return Collections.emptyList();
		}
		return longIds;
	}

	// 元转为分
	private int tranformToCent(double yuan) {
		return new BigDecimal(yuan*100).setScale(0, BigDecimal.ROUND_HALF_UP).intValue();
	}
}
