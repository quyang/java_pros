package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 
 * 统计并记录用户优惠数量,用于后面发送优惠券数量计算
 * 
 * @author zhancang
 */
@Component(value = "getCouponCount")
public class GetCouponCount extends ProcessorUnit {

  private static final int COUPON_COUNT_FLAG = 0;

  private static final int VALID_COUPON_COUNT_FLAG = 1;

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getUserCouponList())) {
      return Result.getSuccDataResult(true);
    }

    Map<String, Integer> countMap = context.getCouponCount();
    if(null == countMap){
      countMap = Maps.newHashMap();
    }
    for (UserCouponDTO coupon : context.getUserCouponList()) {
      if (checkCouponStatusAvailable(coupon)) {
        addUserCouponCount(countMap, coupon.getCouponId(), VALID_COUPON_COUNT_FLAG);
      }
      addUserCouponCount(countMap, coupon.getCouponId(), COUPON_COUNT_FLAG);
    }
    context.setCouponCount(countMap);
    return Result.getSuccDataResult(true);
  }

  private void addUserCouponCount(Map<String, Integer> countMap, Long couponId, int countType) {
    if (null == countMap) {
      countMap = Maps.newHashMap();
    }
    if (null == couponId) {
      return;
    }
    if (countType == VALID_COUPON_COUNT_FLAG) {
      String key = new StringBuilder(couponId.toString()).append(VALID_COUPON_COUNT_KEY).toString();
      addCount(key, countMap);
    } else {
      String key = new StringBuilder(couponId.toString()).append(COUPON_COUNT_KEY).toString();
      addCount(key, countMap);
    }
  }

  private void addCount(String key, Map<String, Integer> countMap) {
    if (null == countMap) {
      return;
    }
    Integer oldCount = countMap.get(key);
    if (null == oldCount) {
      oldCount = 0;
    }
    countMap.put(key, ++oldCount);
  }
}
