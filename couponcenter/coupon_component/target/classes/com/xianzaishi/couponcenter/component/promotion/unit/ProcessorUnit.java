package com.xianzaishi.couponcenter.component.promotion.unit;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.component.promotion.util.EveryBodyPromotionChainContextUtils;
import com.xianzaishi.couponcenter.dal.coupon.dao.UserCouponDOMapper;
import com.xianzaishi.couponcenter.dal.coupon.dataobject.UserCouponDO;
import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountInfoDO;
import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountRefDO;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;

public abstract class ProcessorUnit {

  protected static final Logger log = LoggerFactory.getLogger(ProcessorUnit.class);

  @Autowired
  protected SkuService skuService;

  @Autowired
  protected UserCouponDOMapper userCouponDOMapper;

  /**
   * 计算用户所有优惠券时，传值使用的key
   */
  protected static final String VALID_COUPON_COUNT_KEY = "_valid_coupon";

  /**
   * 计算用户所有优惠券时，传值使用的key
   */
  protected static final String COUPON_COUNT_KEY = "_coupon";

  public abstract Result<Boolean> process(PromotionChainContext context);

  /**
   * 添加价格
   * 
   * @param price
   * @param key
   * @param skuPriceMaps
   */
  protected void addPrice(int price, String key, Map<String, Integer> skuPriceMaps) {
    Integer addPrice = skuPriceMaps.get(key);
    if (null == addPrice || addPrice == 0) {
      addPrice = price;
    } else {
      addPrice = addPrice + price;
    }
    skuPriceMaps.put(key, addPrice);
  }

  /**
   * 规则校验入口
   * 
   * @param rule
   * @param parameterM
   * @return
   */
  public synchronized String calculate(String rule, Map<String, Double> parameterM) {
    if (null == parameterM) {
      return "0";
    }
    Object result = ExpressionCalculatorUtil.calcul(rule, parameterM);
//    log.error(JackSonUtil.getJson(result));
    if (null != result) {
      return result.toString();
    }
    return "0";
  }

  /**
   * 规则校验入口
   * 
   * @param rule
   * @param parameterM
   * @return
   */
  public synchronized String calculateLong(String rule, Map<String, Long> parameterM) {
    if (null == parameterM) {
      return "0";
    }
    Map<String, Double> parameter = Maps.newHashMap();
    for (String key : parameterM.keySet()) {
      Long value = parameterM.get(key);
      if (null != value) {
        parameter.put(key, Double.valueOf(String.valueOf(value)));
      }
    }
    return calculate(rule, parameter);
  }

  public static void main(String args[]) {
    // String rule = "count<=2?(num*price):(num*price*2)";
    String rule =
        "num<=2?(num*price):(num<=3?(num*0.8*price):(num<=4?(num*0.6*price):(num*0.4*price)))";

    String rule2 =
        "(id==11068||id==11069)?1:((id==11070||id==11071)?2:((id==11072||id==11073)?3:0))";

    Map<String, Double> parameterM = Maps.newHashMap();
    parameterM.put("id", (double) 11170);
    // parameterM.put("price", (double) 1);
    long time = System.currentTimeMillis();
    Object result = ExpressionCalculatorUtil.calcul(rule2, parameterM);
    long end = System.currentTimeMillis();
    if (null != result) {
      System.out.println(result);
    } else {
      System.out.println(0);
    }
    System.out.println(end - time);
  }

  /**
   * 根据价格参数和优惠价计算抵扣值，分为单位
   * 
   * @param coupon
   * @param priceM
   * @return
   */
  /**
   * 根据价格参数和优惠价计算抵扣值，分为单位
   * 
   * @param coupon
   * @param priceM
   * @return
   */
  protected int calculate(UserCouponDTO coupon, Map<String, Integer> priceM, int discountBasePrice) {
    String rule = coupon.getCouponRules();
    if (StringUtils.isEmpty(rule)) {
      return 0;
    }
    int amount = priceM.get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0");
    String keyId = "金额";
    Map<String, Double> parameterM = Maps.newHashMap();
    if (coupon.getCouponType() == 12) {
      Long couponId = coupon.getCouponId();
      if (couponId > 0) {
        keyId = "金额_id_" + couponId;
        Integer tagAmount = priceM.get(PromotionChainContext.TOTAL_PRICE_PRE_ID_KEY + couponId);
        if (null != tagAmount) {
          amount = tagAmount;
        } else {
          amount = 0;
        }
      } else {
        return 0;
      }
    }
    
    double cmpAmount = amount;//分为单位
    cmpAmount =
          new BigDecimal(amount).divide(new BigDecimal(100), 2, BigDecimal.ROUND_DOWN)
              .doubleValue();
    
    parameterM.put(keyId, cmpAmount);
    
    String discountpirce = calculate(rule, parameterM);
    log.error("Set rule is:"+rule+",para is："+JackSonUtil.getJson(parameterM)+",result is:"+discountpirce);
    int discount = 0;
    if (!coupon.isFenPriceTypeUserCoupon()) {
      discount = new BigDecimal(discountpirce).multiply(new BigDecimal(100)).intValue();
    } else {
      discount = new BigDecimal(discountpirce).intValue();
    }
    return discount;
  }

  /**
   * 校验优惠券是否可用
   * 
   * @param coupon
   * @return
   */
  protected boolean checkCouponAvailable(UserCouponDTO coupon, PromotionChainContext context) {
    return checkCouponAvailable(coupon,context,true);
  }

  /**
   * 校验优惠券是否可用
   * 
   * @param coupon
   * @return
   */
  protected boolean checkCouponAvailable(UserCouponDTO coupon, PromotionChainContext context, boolean checkUser) {
    if ((coupon.getChannelType() == 2 && context.getPayChannel() == 1)
        || (coupon.getChannelType() == 1 && context.getPayChannel() == 2)) {
      return false;
    }
    if (checkUser && !coupon.getUserId().equals(context.getUserId())) {
      return false;
    }
    return checkCouponStatusAvailable(coupon);
  }
  
  /**
   * 是否是一个有效优惠券
   * 
   * @return
   */
  protected boolean checkCouponStatusAvailable(UserCouponDTO coupon) {
    Date now = new Date();
    if (coupon.getCouponStartTime().after(now) || coupon.getCouponEndTime().before(now)) {
      return false;
    }
    if (coupon.getStatus() == 0) {
      return false;
    }

    return true;
  }

  /**
   * 是否还有效
   * 
   * @param discount
   * @return
   */
  protected boolean isLegalDiscountInfoDO(DiscountInfoDO discount) {
    if (null == discount) {
      return false;
    }
    Date now = new Date();
    if (now.before(discount.getGmtStart()) || now.after(discount.getGmtEnd())) {
      return false;
    }
    if (discount.getSubStatus().intValue() == 1) {
      return true;
    }
    return false;
  }

  /**
   * 是否还有效
   * 
   * @param discount
   * @return
   */
  protected boolean isLegalDiscountRefDO(DiscountRefDO discount) {
    if (null == discount) {
      return false;
    }
    if (discount.getRefStatus().intValue() == 1) {
      return true;
    }
    return false;
  }

  public static class CompratorByMoney implements Comparator<UserCouponDTO> {
    public int compare(UserCouponDTO f1, UserCouponDTO f2) {
      Double amont1 = f1.getAmount() == null ? 0 : f1.getAmount();
      Double amont2 = f2.getAmount() == null ? 0 : f2.getAmount();
      if (!f1.isFenPriceTypeUserCoupon()) {
        amont1 = new BigDecimal(amont1.toString()).multiply(new BigDecimal(100)).doubleValue();
      }
      if (!f2.isFenPriceTypeUserCoupon()) {
        amont2 = new BigDecimal(amont2.toString()).multiply(new BigDecimal(100)).doubleValue();
      }
      if (amont1 > amont2) {
        return -1;
      } else {
        return 1;
      }
    }
  }

  private int getCouponTag(UserCouponDTO coupon) {
    String rule = coupon.getCouponRules();
    String tmpKey = rule.substring(0, rule.indexOf(">"));
    tmpKey = tmpKey.replace("金额_tag_", "");
    if (StringUtils.isNumeric(tmpKey)) {
      return Integer.valueOf(tmpKey);
    }
    return 0;
  }

  /**
   * 根据主键查询
   * @param id
   * @return
   */
  protected UserCouponDO getUserCouponById(Long id){
    return userCouponDOMapper.selectByPrimaryKey(id);
  }
  
  /**
   * 根据主键查询
   * @param id
   * @return
   */
  protected List<UserCouponDO> getUserCouponByIdList(List<Long> idList){
    return userCouponDOMapper.selectByIdList(idList);
  }
  
  /**
   * 获取context
   * 
   * @param context
   * @return
   */
  public static PromotionChainContext getChainContext(PromotionChainContext context) {
    if (context == null)
      return null;
    PromotionChainContext context1 =
        EveryBodyPromotionChainContextUtils.getPromotionChainContext(context.getUserId());


    if (context1 != null) {
      context1.setPayChannel(context.getPayChannel());
      if (CollectionUtils.isNotEmpty(context.getUserCouponList()))
        //设置用户所有优惠券
        context1.setUserCouponList(context.getUserCouponList());
      if (CollectionUtils.isNotEmpty(context.getUserSelectedCouponList()))
        //
        context1.setUserSelectedCouponList(context.getUserSelectedCouponList());
      if (MapUtils.isNotEmpty(context.getCouponCount()))
        context1.setCouponCount(context.getCouponCount());
      if (CollectionUtils.isNotEmpty(context.getCouponList()))
        context1.setCouponList(context.getCouponList());
      return context1;
    }
    EveryBodyPromotionChainContextUtils.initPromotionChainContext(context.getUserId(), context);
    return context;
  }
}
