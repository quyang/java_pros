package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 通过优惠券规则和用户优惠价数据计算给用户赠送的优惠券
 * 
 * @author zhancang
 */
@Component(value = "getSendUserCouponNumUnit")
public class GetSendUserCouponNumUnit extends ProcessorUnit {

  protected static final String VALID_COUPON_RULE_KEY = "valid_coupon";
  
  protected static final String ALL_COUPON_RULE_KEY = "coupon";
  
  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getCouponList())) {
      return Result.getSuccDataResult(true);
    }

    Map<String, Integer> countMap = context.getCouponCount();
    if(null == countMap){
      countMap = Maps.newHashMap();
    }
    for (CouponDTO coupon : context.getCouponList()) {
      String rule = coupon.getSendRules();
      String allKey = new StringBuilder().append(String.valueOf(coupon.getId())).append(
          COUPON_COUNT_KEY).toString();
      Integer allCouponCount =
          countMap.get(allKey);
      String validKey = new StringBuilder().append(String.valueOf(coupon.getId())).append(
          VALID_COUPON_COUNT_KEY).toString();
      Integer validCouponCount =
          countMap.get(validKey);
      allCouponCount = allCouponCount == null ? 0 : allCouponCount;
      validCouponCount = validCouponCount == null ? 0 : validCouponCount;
      if(StringUtils.isEmpty(rule)){
        if(allCouponCount == 0){
          countMap.put(coupon.getId().toString(), 1);
        }else{
          countMap.put(coupon.getId().toString(), 0);
        }
      }else{
        Map<String, Double> parameterM = Maps.newHashMap();
        parameterM.put(VALID_COUPON_RULE_KEY, Double.valueOf(validCouponCount.toString()));
        parameterM.put(ALL_COUPON_RULE_KEY, Double.valueOf(allCouponCount.toString()));
        String result = super.calculate(rule, parameterM);
        if(StringUtils.isEmpty(result) || !StringUtils.isNumeric(result)){
          countMap.put(coupon.getId().toString(), 0);
        }else{
          countMap.put(coupon.getId().toString(), Integer.valueOf(result));
        }
      }
    }
    
    context.setCouponCount(countMap);

    return Result.getSuccDataResult(true);
  }
}
