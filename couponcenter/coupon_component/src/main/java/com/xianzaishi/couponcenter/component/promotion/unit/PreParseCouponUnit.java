package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 优先处理掉错误的优惠券，并更新优惠券使用时间到可直接使用
 * @author zhancang
 */
@Component(value = "preParseCouponUnit")
public class PreParseCouponUnit extends ProcessorUnit {

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getCouponList())) {
      return Result.getSuccDataResult(true);
    }

    List<CouponDTO> couponList = Lists.newArrayList();
    for(CouponDTO coupon:context.getCouponList()){
      if(null == coupon){
        continue;
      }
      
      Date today = getToday();
      if(null == coupon.getGmtStart()){
        continue;
      }
      if(null == coupon.getGmtEnd() && null == coupon.getGmtDayDuration()){
        continue;
      }
      //优惠券未设置结束时间，则比较当前时间和优惠券开始时间，加上优惠券延续时间
      if(null == coupon.getGmtEnd() && null != coupon.getGmtDayDuration()){
        if(today.before(coupon.getGmtStart())){
          Date couponEnd = DateUtils.addDays(coupon.getGmtStart(), coupon.getGmtDayDuration());
          couponEnd = DateUtils.addSeconds(couponEnd, -1);
          coupon.setGmtEnd(couponEnd);
        }else{
          Date couponEnd = DateUtils.addDays(today, coupon.getGmtDayDuration());
          couponEnd = DateUtils.addSeconds(couponEnd, -1);
          coupon.setGmtStart(today);
          coupon.setGmtEnd(couponEnd);
        }
        if(coupon.getGmtEnd().after((coupon.getGmtStart()))){
          couponList.add(coupon);
        }
        continue;
      }
      
      //设置过开始时间，结束时间和持续时间
      if(null != coupon.getGmtStart() && null != coupon.getGmtEnd() && null != coupon.getGmtDayDuration()){
        if(coupon.getGmtStart().after(coupon.getGmtEnd())){
          continue;
        }
        
        Date couponEnd = null;
        if(today.before(coupon.getGmtStart())){
          couponEnd = DateUtils.addDays(coupon.getGmtStart(), coupon.getGmtDayDuration());
          couponEnd = DateUtils.addSeconds(couponEnd, -1);
        }else{
          couponEnd = DateUtils.addDays(today, coupon.getGmtDayDuration());
          couponEnd = DateUtils.addSeconds(couponEnd, -1);
          coupon.setGmtStart(today);
        }
        
        Date initEnd = coupon.getGmtEnd();
        if(initEnd.before(couponEnd)){
          couponEnd = initEnd;
        }
        coupon.setGmtEnd(couponEnd);
        couponList.add(coupon);
      }
      
      //设置过开始时间，结束时间和持续时间
      if(null != coupon.getGmtStart() && null != coupon.getGmtEnd() && null == coupon.getGmtDayDuration()){
        if(coupon.getGmtStart().after(coupon.getGmtEnd())){
          continue;
        }
        
        if(today.after(coupon.getGmtEnd())){
          continue;
        }
        
        if(today.before(coupon.getGmtStart())){
        }else{
          coupon.setGmtStart(today);
        }
        couponList.add(coupon);
      }
    }
    context.setCouponList(couponList);
    
    return Result.getSuccDataResult(true);
  }
  
  /**
   * 获取0点
   * @param
   * @return
   */
  private Date getToday(){
    Date date = new Date();
    date = DateUtils.setHours(date, 0);
    date = DateUtils.setMinutes(date, 0);
    date = DateUtils.setSeconds(date, 0);
    date = DateUtils.setMilliseconds(date, 0);
    return date;
  }
}
