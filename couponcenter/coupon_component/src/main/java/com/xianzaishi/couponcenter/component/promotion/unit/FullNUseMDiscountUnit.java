package com.xianzaishi.couponcenter.component.promotion.unit;

import com.xianzaishi.couponcenter.client.discount.discountUtil.DiscountUtils;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountCalResultDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountInfoDTO;
import com.xianzaishi.couponcenter.client.discount.enums.DiscountRefTypeEnum;
import com.xianzaishi.couponcenter.client.discount.enums.DiscountTypeEnum;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.dal.discount.dao.DiscountInfoDOMapper;
import com.xianzaishi.couponcenter.dal.discount.dao.DiscountRefDOMapper;
import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountInfoDO;
import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountRefDO;
import com.xianzaishi.couponcenter.dal.discount.query.DiscountInfoQuery;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import org.apache.commons.collections.Closure;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * N件M折优惠处理
 *
 * @author jianmo
 * @create 2016-12-27 下午 5:10
 **/
@Component(value = "fullNUseMDiscountUnit")
public class FullNUseMDiscountUnit extends ProcessorUnit {

    @Autowired
    private DiscountInfoDOMapper discountInfoDOMapper;

    @Autowired
    private DiscountRefDOMapper discountRefDOMapper;

    @Override
    public Result<Boolean> process(PromotionChainContext context) {
      if(context.getPayChannel() == 2){//线下不支持
        return Result.getSuccDataResult(true);
      }
//        context = this.getChainContext(context);
        List<BuySkuInfoDTO> buySkuInfoDTOs = context.getBuyInfoList();
        Map<String,DiscountCalResultDTO> ffullNUseMDiscount = context.getFullNUseMDiscount();
        if(ffullNUseMDiscount==null){
            ffullNUseMDiscount = new HashMap<>();
        }
        //获取所有参与满件减活动的商品的skuId:先获取满件减活动的id，在获取商品的skuId
        List<DiscountInfoDO> discountInfoDOs = getDiscountByType();
        if(CollectionUtils.isEmpty(discountInfoDOs)) return Result.getSuccDataResult(true);
        final List<Integer> discountDetailIds = new ArrayList<>();
        CollectionUtils.forAllDo(discountInfoDOs,new Closure(){
            @Override
            public void execute(Object input) {
                DiscountInfoDO discountInfoDO = (DiscountInfoDO)input;
                discountDetailIds.add(discountInfoDO.getDiscountDetailId());
            }
        });

        //批量获取状态可用的商品skuIds
        List<DiscountRefDO> discountRefDOs = discountRefDOMapper.batchQuery(discountDetailIds);
        if(CollectionUtils.isEmpty(discountRefDOs)) return Result.getSuccDataResult(true);
        final List<Long> skuIds = new ArrayList<>();
        CollectionUtils.forAllDo(discountRefDOs,new Closure(){
            @Override
            public void execute(Object input) {
                DiscountRefDO discountRefDO = (DiscountRefDO)input;
                //TODO - 状态枚举化
                if(DiscountRefTypeEnum.REF_SKU.getValue()==discountRefDO.getRefType()&&1==discountRefDO.getRefStatus()){
                    skuIds.add(discountRefDO.getRefId());
                }
            }
        });
        //商品去重
        Set<Long> skuIdsSet = new HashSet<>(skuIds);
        BigDecimal dicountTotalPrice = new BigDecimal(String.valueOf(context.getDiscountTotalrice()));
        for(BuySkuInfoDTO buySkuInfoDTO : buySkuInfoDTOs){
            if(!skuIdsSet.contains(buySkuInfoDTO.getSkuId())){
                continue;
            }
            ItemCommoditySkuDTO itemCommoditySkuDTO = context.getSkuMaps().get(buySkuInfoDTO.getSkuId());
            //如果是称重商品，则继续
            if(itemCommoditySkuDTO.getIsSteelyardSku()) continue;
            DiscountCalResultDTO discountCalResultDTO =null;
            //1、获取适合该中sku的N件N折的规则
            DiscountInfoDO discountInfoDO = getDiscountMatcher(buySkuInfoDTO.getBuyCount());
            if(null!=discountInfoDO) {
                //2、计算其优惠信息
                discountCalResultDTO = calNUseMDiscount(discountInfoDO, itemCommoditySkuDTO,buySkuInfoDTO.getBuyCount());
                if(discountCalResultDTO.isGetDiscount()) {
                    dicountTotalPrice= dicountTotalPrice.add(new BigDecimal(String.valueOf(discountCalResultDTO.getDiscountPrice())));
                }
            }else{
                discountInfoDO = getUnDiscountMatcher(buySkuInfoDTO.getBuyCount());
                if(discountInfoDO!=null)
                //计算差价信息
                discountCalResultDTO = calNUseMUnDiscount(discountInfoDO, itemCommoditySkuDTO, buySkuInfoDTO.getBuyCount());
            }

            ffullNUseMDiscount.put(DiscountTypeEnum.FULL_N_USE_M.getCode()+DiscountTypeEnum.FULL_N_USE_M.getValue()+"_"+buySkuInfoDTO.getSkuId(),discountCalResultDTO);
        }
        context.setFullNUseMDiscount(ffullNUseMDiscount);
        context.setDiscountTotalrice(dicountTotalPrice.intValue());
        return Result.getSuccDataResult(true);
    }

    /**
     * 根据类型获取其优惠信息
     * @return
     */
    private List<DiscountInfoDO> getDiscountByType(){
        DiscountInfoQuery query = new DiscountInfoQuery();
        query.setType(DiscountTypeEnum.FULL_N_USE_M.getValue());
        return discountInfoDOMapper.selectByType(query);
    }

    /**
     * 获取可享受的最近优惠-即可享受的最高优惠
     * @param itemCount
     * @return
     */
    private DiscountInfoDO getDiscountMatcher(String itemCount){
        DiscountInfoQuery query = new DiscountInfoQuery();
        query.setType(DiscountTypeEnum.FULL_N_USE_M.getValue());
        query.setItemCount(DiscountUtils.buyCountTo100(itemCount));
        DiscountInfoDO  discountInfoDOs = discountInfoDOMapper.querySuitableFirstByType(query);
        //在返回件数之前，直接转换
        if(discountInfoDOs!=null)
        discountInfoDOs.setThreshold(new BigDecimal(String.valueOf(discountInfoDOs.getThreshold())).divide(new BigDecimal("100")).intValue());
        return discountInfoDOs;
    }

    /**
     * 获取不可享受的最近优惠-即不可享受的最低优惠
     * @param itemCount
     * @return
     */
    private DiscountInfoDO getUnDiscountMatcher(String itemCount){
        DiscountInfoQuery query = new DiscountInfoQuery();
        query.setType(DiscountTypeEnum.FULL_N_USE_M.getValue());
        query.setItemCount(DiscountUtils.buyCountTo100(itemCount));
        DiscountInfoDO  discountInfoDOs = discountInfoDOMapper.queryUNSuitableFirstByType(query);
        //在返回件数之前，直接转换
        if(discountInfoDOs!=null)
        discountInfoDOs.setThreshold(new BigDecimal(String.valueOf(discountInfoDOs.getThreshold())).divide(new BigDecimal("100")).intValue());
        return discountInfoDOs;
    }

    /**
     * 计算可享受到的优惠信息
     * @param discountInfoDO
     * @param itemCommoditySkuDTO
     * @param itemCount
     * @return
     */
    private DiscountCalResultDTO calNUseMDiscount(DiscountInfoDO discountInfoDO,ItemCommoditySkuDTO itemCommoditySkuDTO ,String itemCount){
        DiscountCalResultDTO resultDTO = new DiscountCalResultDTO();
        //计算公式：优惠后的价格 = 折扣*件数*单价；原价格=单价*件数；优惠价格原价-优惠后的价格
        BigDecimal itemPrice =new BigDecimal(String.valueOf(itemCommoditySkuDTO.getDiscountPrice()));
        BigDecimal discountValue = new BigDecimal(String.valueOf( discountInfoDO.getDiscountValue())).divide(new BigDecimal("100"));
        BigDecimal itemCountB = new BigDecimal(itemCount);
        BigDecimal discountSingleCount = new BigDecimal(String.valueOf(discountInfoDO.getThreshold()));
        BigDecimal tempCount = itemCountB.divideAndRemainder(discountSingleCount)[1];
        BigDecimal discountCount = itemCountB.subtract(tempCount);
        BigDecimal currTempPrice = discountValue.multiply(itemPrice).multiply(discountCount);
        BigDecimal currPrice = currTempPrice.add(tempCount.multiply(itemPrice));
        BigDecimal originPrice = itemPrice.multiply(itemCountB);
        BigDecimal discountPrice = originPrice.subtract(currPrice);
        resultDTO.setSkuId(itemCommoditySkuDTO.getSkuId());
        resultDTO.setItemId(itemCommoditySkuDTO.getItemId());
//        转换数据
        DiscountInfoDTO discountInfoDTO = new DiscountInfoDTO();
        BeanCopierUtils.copyProperties(discountInfoDO,discountInfoDTO);
        resultDTO.setDiscountInfoDTO(discountInfoDTO);
        resultDTO.setGetDiscount(true);
        resultDTO.setItemCount(itemCount);
        resultDTO.setCurrPrice(currPrice.intValue());
        resultDTO.setOriginPrice(originPrice.intValue());
        resultDTO.setDiscountPrice(discountPrice.intValue());
        resultDTO.setSingleDiscountPrice(discountPrice.intValue());
        return resultDTO;
    }

    /**
     * 计算不可享受优惠时，还差多少可享受优惠
     * @return
     */
    private DiscountCalResultDTO calNUseMUnDiscount(DiscountInfoDO discountInfoDO,ItemCommoditySkuDTO itemCommoditySkuDTO ,String itemCount){
        DiscountCalResultDTO resultDTO = new DiscountCalResultDTO();
        //计算公式：初始价格；还差价；享受优惠价；
        //可享受优惠的件数
        BigDecimal threshold = new BigDecimal(String.valueOf(discountInfoDO.getThreshold()));
        BigDecimal itemCountB = new BigDecimal(itemCount);
        BigDecimal itemPrice = new BigDecimal(String.valueOf(itemCommoditySkuDTO.getDiscountPrice()));
        //还差多少件
        BigDecimal differenceNum = threshold.subtract(itemCountB).multiply(new BigDecimal("100"));
        BigDecimal originPrice = itemPrice.multiply(itemCountB);

        resultDTO.setSkuId(itemCommoditySkuDTO.getSkuId());
        resultDTO.setItemId(itemCommoditySkuDTO.getItemId());
        //  转换数据
        DiscountInfoDTO discountInfoDTO = new DiscountInfoDTO();
        BeanCopierUtils.copyProperties(discountInfoDO,discountInfoDTO);
        resultDTO.setDiscountInfoDTO(discountInfoDTO);
        resultDTO.setGetDiscount(false);
        resultDTO.setDiscountPrice(differenceNum.intValue());
        resultDTO.setItemCount(itemCount);
        resultDTO.setCurrPrice(originPrice.intValue());
        resultDTO.setOriginPrice(originPrice.intValue());
        return resultDTO;
    }

}
