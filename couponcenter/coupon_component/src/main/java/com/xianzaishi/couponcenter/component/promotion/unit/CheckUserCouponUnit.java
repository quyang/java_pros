package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 检查过滤用户所有优惠券，并且按照优惠价格倒序排序
 * @author zhancang
 */
@Component(value = "checkUserCouponUnit")
public class CheckUserCouponUnit extends ProcessorUnit {

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getUserCouponList())) {
      return Result.getSuccDataResult(true);
    }

    List<UserCouponDTO> userCouponList = context.getUserCouponList();
    List<UserCouponDTO> cleanCouponList = Lists.newArrayList();
    for (UserCouponDTO coupon : userCouponList) {
      if(checkCouponAvailable(coupon, context)){
        cleanCouponList.add(coupon);
      }
    }
    Collections.sort(cleanCouponList, new CompratorByMoney());
    context.setUserCouponList(cleanCouponList);
    return Result.getSuccDataResult(true);
  }
}
