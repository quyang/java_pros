package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountDetailDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 临期商品计入优惠
 * @author zhancang
 */
@Component(value = "nearShelfLifeSkuProcessUnit")
public class NearShelfLifeSkuProcessUnit extends ProcessorUnit{

  private static final String DISCOUNT_NAME = "店铺A类促销优惠";
  
  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    Map<Long, DiscountDetailDTO> priceM = context.getNearShelfLifePrice();
    if(null == priceM || CollectionUtils.isEmpty(priceM.keySet())){
      return Result.getSuccDataResult(true);
    }
    
    DiscountResultDTO discountResult = context.getDiscountResult();
    if(null == discountResult){
      discountResult = new DiscountResultDTO();
    }
    
    int discount = 0;//有优惠的折扣后付款总价格
    int originPrice = 0;
    for(Long skuId:priceM.keySet()){
      DiscountDetailDTO disDetail = priceM.get(skuId);
      discount = discount + disDetail.getDiscountPrice();
      originPrice = originPrice + disDetail.getOriginPrice();
      setDiscountDetail(disDetail,discountResult);//记录优惠明细，sku为单位
    }
    
    DiscountDetailDTO totalDetail = new DiscountDetailDTO();
    totalDetail.setOriginPrice(originPrice);
    totalDetail.setDiscountPrice(discount);
    totalDetail.setDiscountName(DISCOUNT_NAME);
    totalDetail.setType((short)3);
    List<DiscountDetailDTO> disList = discountResult.getTotalDiscount();
    if(CollectionUtils.isEmpty(disList)){
      disList = Lists.newArrayList();
    }
    disList.add(totalDetail);//总优惠中记录一条，合并所有临期优惠到一起
    discountResult.setTotalDiscount(disList);//记录总优惠，合并一个类型的优惠
    
    Integer totalDiscount = discountResult.getDiscountPrice();
    if(null == totalDiscount){
      totalDiscount = 0;
    }
    discountResult.setDiscountPrice(discountResult.getOrinalPrice()-(totalDiscount+(originPrice-discount)));//记录总优惠价格，合并所有优惠
    
    context.setDiscountResult(discountResult);
    return Result.getSuccDataResult(true);
  }

  /**
   * 更新到优惠结果中,记录到优惠明细中，以sku为单位
   */
  private void setDiscountDetail(DiscountDetailDTO disDetail,DiscountResultDTO discountResult){
    if(null == disDetail || null == discountResult){
      return;
    }
    Map<Long, List<DiscountDetailDTO>> discount = discountResult.getDiscountDetailMap();
    if(null == discount){
      discount = Maps.newHashMap();
    }
    List<DiscountDetailDTO> disList = discount.get(disDetail.getSkuId());
    if(disList == null){
      disList = Lists.newArrayList();
    }
    disList.add(disDetail);
    discount.put(disDetail.getSkuId(), disList);
    discountResult.setDiscountDetailMap(discount);
  }
}
