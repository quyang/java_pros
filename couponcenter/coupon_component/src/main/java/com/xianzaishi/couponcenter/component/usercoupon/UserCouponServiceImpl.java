package com.xianzaishi.couponcenter.component.usercoupon;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.usercoupon.UserCouponService;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDiscontResultDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.QueryParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.QueryParaDTO.QueryTypeConstants;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendItemCouponParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO.SendTypeConstants;
import com.xianzaishi.couponcenter.client.usercoupon.query.Status;
import com.xianzaishi.couponcenter.client.usercoupon.query.UserCouponDiscontQueryDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.component.promotion.PromotionProcessor;
import com.xianzaishi.couponcenter.dal.coupon.dao.CouponDOMapper;
import com.xianzaishi.couponcenter.dal.coupon.dao.UserCouponDOMapper;
import com.xianzaishi.couponcenter.dal.coupon.dataobject.CouponDO;
import com.xianzaishi.couponcenter.dal.coupon.dataobject.UserCouponDO;
import com.xianzaishi.couponcenter.dal.coupon.query.CouponQuery;
import com.xianzaishi.couponcenter.dal.coupon.query.UserCouponQuery;
import com.xianzaishi.itemcenter.client.itemsku.SkuService;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.query.SkuQuery;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.usercenter.client.user.UserService;
import com.xianzaishi.usercenter.client.user.dto.BaseUserDTO;

/**
 * 用户优惠券实现类 重写by@author dongpo
 * 
 */
@Service("userCouponService")
public class UserCouponServiceImpl implements UserCouponService {

  @Autowired
  private UserService userService;

  @Autowired
  protected SkuService skuService;

  @Autowired
  private UserCouponDOMapper userCouponDOMapper;

  @Autowired
  private CouponDOMapper couponDOMapper;

  @Autowired
  private PromotionProcessor promotionProcessor;

  @Autowired
  private SystemPropertyService systemPropertyService;

  private Logger LOGGER = LoggerFactory.getLogger(UserCouponServiceImpl.class);

  private static final long DEFAULT_USER_COUPON_TIME = 30 * 24 * 3600 * 1000l;

  private ConcurrentHashMap<Long, CouponDO> cache = new ConcurrentHashMap<Long, CouponDO>();

  private ConcurrentHashMap<Long, List<Object>> timecache =
      new ConcurrentHashMap<Long, List<Object>>();


  @Override
  public Result<List<UserCouponDTO>> getUserCoupon(QueryParaDTO para) {
    if (null == para || (null != para.getQueryType() && !(isLegalQueryPara(para.getQueryType())))
        || null == para.getUserId()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Query para is error");
    }

    com.xianzaishi.itemcenter.common.result.Result<? extends BaseUserDTO> userResult =
        userService.queryUserByUserId(para.getUserId(), false);
    if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "user is not exist");
    }
    BaseUserDTO user = userResult.getModule();
    if(user.getUserId() == null || user.getUserId() <= 0){
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "user is not exist");
    }
    
    List<Long> idList = null;
    if (QueryTypeConstants.BY_COUPON_LIST.equals(para.getQueryType())) {
      if (CollectionUtils.isEmpty(para.getCouponIds())) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
            "Query para coupon id is null");
      }
      idList = para.getCouponIds();
    }

    if (QueryTypeConstants.BY_COUPON_PACKAGE.equals(para.getQueryType())) {
      if (null == para.getUserId() || para.getPackageId() == null || para.getPackageId() <= 0) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
            "Query para package id is error");
      }
      idList = getCouponConfigPackage(para.getPackageId());
    }

    List<UserCouponDO> couponList = null;
    if (null == para.isIncludeAllCoupon() || !para.isIncludeAllCoupon()) {
      couponList =
          this.getUserCoupons(user.getUserId(), null, idList, Status.VALID.getValue(), null, true,0);
    } else {
      couponList = this.getUserCoupons(user.getUserId(), null, idList, null, null, false,0);
    }
    if (CollectionUtils.isEmpty(couponList)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Query user coupon return null");
    }
    List<UserCouponDTO> result = getCouponDTOByCouponDO(couponList);

    return Result.getSuccDataResult(result);
  }
  
  @Override
  public Result<Integer> countUserCoupon(QueryParaDTO para) {
    if (null == para || (null != para.getQueryType() && !(isLegalQueryPara(para.getQueryType())))
        || null == para.getUserId()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Query para is error");
    }

    com.xianzaishi.itemcenter.common.result.Result<? extends BaseUserDTO> userResult =
        userService.queryUserByUserId(para.getUserId(), false);
    if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "user is not exist");
    }
    BaseUserDTO user = userResult.getModule();
    if(user.getUserId() == null || user.getUserId() <= 0){
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "user is not exist");
    }
    
    List<Long> idList = null;
    if (QueryTypeConstants.BY_COUPON_LIST.equals(para.getQueryType())) {
      if (CollectionUtils.isEmpty(para.getCouponIds())) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
            "Query para coupon id is null");
      }
      idList = para.getCouponIds();
    }

    if (QueryTypeConstants.BY_COUPON_PACKAGE.equals(para.getQueryType())) {
      if (null == para.getUserId() || para.getPackageId() == null || para.getPackageId() <= 0) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
            "Query para package id is error");
      }
      idList = getCouponConfigPackage(para.getPackageId());
    }

    Integer count = null;
    if (null == para.isIncludeAllCoupon() || !para.isIncludeAllCoupon()) {
      count =
          this.count(user.getUserId(), null, idList, Status.VALID.getValue(), null, true,0);
    } else {
      count = this.count(user.getUserId(), null, idList, null, null, false,0);
    }
    return Result.getSuccDataResult(count);
  }


  @Override
  public Result<List<UserCouponDTO>> getItemCoupon(QueryParaDTO para) {
    if (null == para || null == para.getSkuId()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Query para is error");
    }    
    boolean include =false;
    if(null != para.isIncludeAllCoupon()){
      include =para.isIncludeAllCoupon();
    }
    List<UserCouponDO> couponList =
          this.getUserCoupons(para.getSkuId(), null, null, null, null, !include,1);
    if (CollectionUtils.isEmpty(couponList)) {
      return Result.getSuccDataResult(Collections.<UserCouponDTO>emptyList());
    }
    List<UserCouponDTO> result = getCouponDTOByCouponDO(couponList);

    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<UserCouponDTO> getUserCouponByOrderId(Long userId, Long orderId) {
    if (null == userId || userId <= 0 || null == orderId || orderId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parameter is error");
    }
    List<UserCouponDO> couponList = this.getUserCoupons(userId, null, null, null, orderId, false,0);
    if (CollectionUtils.isNotEmpty(couponList)) {
      UserCouponDTO result = new UserCouponDTO();
      BeanCopierUtils.copyProperties(couponList.get(0), result);
      return Result.getSuccDataResult(result);
    } else {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter query not fount,userId:" + userId + ",orderId:" + orderId);
    }
  }

  @Override
  public Result<UserCouponDTO> getUserCouponById(Long id) {
    if (null == id || id <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parameter is error");
    }
    UserCouponDO userCoupon = userCouponDOMapper.selectByPrimaryKey(id);
    if (userCoupon != null) {
      UserCouponDTO wrapper = new UserCouponDTO();
      try {
        BeanUtils.copyProperties(userCoupon,wrapper);
        return Result.getSuccDataResult(wrapper);
      } catch (Exception e) {
        LOGGER.error("Fail to copy properties from userCoupon -> " + id, userCoupon);
      }
    }

    return Result.getErrDataResult(ServerResultCode.SERVER_ERROR,
        "coupon is not exist with this query： " + id);
  }

  @Override
  public Result<Boolean> sendCoupon(SendParaDTO para) {
    if (null == para || null == para.getSendType() || !(isLegalSendPara(para.getSendType()))
        || null == para.getUserId()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Query para is error");
    }

    com.xianzaishi.itemcenter.common.result.Result<? extends BaseUserDTO> userResult =
        userService.queryUserByUserId(para.getUserId(), false);
    if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "user is not exist");
    }
    BaseUserDTO user = userResult.getModule();

    List<CouponDO> couponDOLists = null;
    if (SendTypeConstants.BY_COUPON_LIST.equals(para.getSendType())) {
      if (CollectionUtils.isEmpty(para.getCouponIds())) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
            "Query para coupon id is null");
      }
      if (para.getCouponIds().size() == 1) {
        couponDOLists = Arrays.asList(getCouponById(para.getCouponIds().get(0)));
      } else {
        couponDOLists = getCouponByIdList(para.getCouponIds());
      }
    }

    if (SendTypeConstants.BY_COUPON_PACKAGE.equals(para.getSendType())) {
      if (null == para.getUserId() || para.getPackageId() == null || para.getPackageId() <= 0) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
            "Query para package id is error");
      }
      List<Long> idList = getCouponConfigPackage(para.getPackageId());
      if (CollectionUtils.isNotEmpty(idList)) {
        couponDOLists = getCouponByIdList(idList);
      }
    }

    if (CollectionUtils.isEmpty(couponDOLists)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Query para get coupon list is empty");
    }

    List<CouponDTO> couponList = Lists.newArrayList();
    for (CouponDO coupon : couponDOLists) {
      CouponDTO couponDTO = new CouponDTO();
      org.springframework.beans.BeanUtils.copyProperties(coupon, couponDTO);
      couponList.add(couponDTO);
    }
    PromotionChainContext context = new PromotionChainContext();
    context.setUserId(user.getUserId());
    context.setCouponList(couponList);

    PromotionChainContext contextResult = promotionProcessor.processSendCouponRule(context);

    if (null == contextResult || CollectionUtils.isEmpty(contextResult.getCouponList())
        || null == contextResult.getCouponCount()
        || CollectionUtils.isEmpty(contextResult.getCouponCount().keySet())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Get coupon by paremeter return is empty");
    }

    Map<String, Integer> sendCouponCount = contextResult.getCouponCount();
    List<CouponDTO> sendList = Lists.newArrayList();
    for (CouponDTO coupon : couponList) {
      Long couponId = coupon.getId();
      Integer sendNum = sendCouponCount.get(couponId.toString());
      if (null != sendNum && sendNum > 0) {
        for (int i = 0; i < sendNum; i++) {
          sendList.add(coupon);
        }
      }
    }

    if (CollectionUtils.isEmpty(sendList)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Get coupon count by paremeter return is empty");
    }

    return this.batchInsertCouponDTO(user.getUserId(), sendList);
  }

  @Override
  public Result<List<Long>> addItemCoupon(SendItemCouponParaDTO para) {
    if (null == para || null == para.getSkuId() || null == para.getLimitCount()
        || null == para.getBuyPrice() || para.getLimitCount() <= 0 || para.getLimitCount() <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Additem coupon para is error");
    }

    Result<ItemCommoditySkuDTO> skuResult =
        skuService.queryItemSku(SkuQuery.querySkuById(para.getSkuId()));
    if (null == skuResult || !skuResult.getSuccess() || null == skuResult.getModule()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Additem coupon query item return null");
    }

    CouponDO coupon = getCouponById(para.getCouponId());
    if(null == coupon){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Additem coupon query coupon return null");
    }
    
    UserCouponDO itemCouponDO = getItemCoupon(para.getSkuId(),para.getBuyPrice(),para.getCouponDayTime(),coupon);
    List<Long> idList = Lists.newArrayList();
    for(int i=0;i<para.getLimitCount();i++){
      if (userCouponDOMapper.insert(itemCouponDO) > 0) {
        idList.add(itemCouponDO.getId());
        itemCouponDO.setId(null);
      }
    }
    return Result.getSuccDataResult(idList);
  }

  @Override
  public Result<Boolean> updateCouponStatus(Long id, Short status) {
    int updateCount = userCouponDOMapper.updateStatus(id, status);
    if (updateCount == 1) {
      return Result.getSuccDataResult(true);
    } else {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Update usercoupon status failed,id:" + id);
    }
  }

  @Override
  public Result<Boolean> updateCouponOrder(Long id, Long orderId) {
    int updateCount = userCouponDOMapper.updateOrder(id, orderId);
    if (updateCount == 1) {
      return Result.getSuccDataResult(true);
    } else {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Update usercoupon status failed,id:" + id);
    }
  }

  private List<UserCouponDO> getUserCoupons(long userId, List<Short> couponTypes,
      List<Long> couponIds, Short couponStatus, Long orderId, boolean filterExpiredCoupon,int sortBy) {

    List<UserCouponDO> results = new ArrayList<>();

    try {
      UserCouponQuery query = new UserCouponQuery();
      query.setUserId(userId);
      if (CollectionUtils.isNotEmpty(couponTypes)) {
        query.setCouponTypes(couponTypes);
      }
      if (CollectionUtils.isNotEmpty(couponIds)) {
        query.setCouponIds(couponIds);
      }
      if (null != orderId && orderId > 0) {
        query.setOid(orderId);
      }
      query.setStatus(couponStatus);
      if (filterExpiredCoupon) {
        query.setCouponEndTimeRangeEnd(new Date());
      }
      if(sortBy == 0){
        query.setSortByAmount();
      }else{
        query.setSortById();
      }
      List<UserCouponDO> db = userCouponDOMapper.selectByQuery(query);
      if (CollectionUtils.isNotEmpty(db)) {
        results.addAll(db);
      }
    } catch (Exception e) {
      LOGGER.error("Fail to query user coupons by couponTypes", e);
    }
    LOGGER.info(JackSonUtil.getJson(results));
    return results;
  }

  private Integer count(long userId, List<Short> couponTypes,
      List<Long> couponIds, Short couponStatus, Long orderId, boolean filterExpiredCoupon,int sortBy) {

    Integer count = null;

    try {
      UserCouponQuery query = new UserCouponQuery();
      query.setUserId(userId);
      if (CollectionUtils.isNotEmpty(couponTypes)) {
        query.setCouponTypes(couponTypes);
      }
      if (CollectionUtils.isNotEmpty(couponIds)) {
        query.setCouponIds(couponIds);
      }
      if (null != orderId && orderId > 0) {
        query.setOid(orderId);
      }
      query.setStatus(couponStatus);
      if (filterExpiredCoupon) {
        query.setCouponEndTimeRangeEnd(new Date());
      }
      if(sortBy == 0){
        query.setSortByAmount();
      }else{
        query.setSortById();
      }
      count = userCouponDOMapper.count(query);
      if (null == count) {
        count = 0;
      }
    } catch (Exception e) {
      LOGGER.error("Fail to count user coupons by couponTypes", e);
    }
    LOGGER.info(JackSonUtil.getJson(count));
    return count;
  }

  
  /**
   * 批量插入用户优惠券
   * 
   * @param userId
   * @param coupons
   * @return
   */
  private Result<Boolean> batchInsertCouponDTO(long userId, List<CouponDTO> coupons) {
    if (userId <= 0 || CollectionUtils.isEmpty(coupons)) {
      LOGGER.error("parameter is error");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parameter is error");
    }
    List<UserCouponDO> userCouponDOs = Lists.newArrayList();
    Date now = new Date();
    for (CouponDTO coupon : coupons) {
      UserCouponDO userCoupon = new UserCouponDO();
      userCoupon.setUserId(userId);
      userCoupon.setCouponId(coupon.getId());
      userCoupon.setCouponTitle(coupon.getTitle());
      userCoupon.setCouponStartTime(coupon.getGmtStart());
      userCoupon.setCouponEndTime(getEndTimeOfDay(coupon.getGmtEnd()));
      userCoupon.setOrderId(0L);
      userCoupon.setCouponRules(coupon.getRules());
      userCoupon.setSendRules(coupon.getSendRules());
      userCoupon.setGmtCreate(now);
      userCoupon.setGmtModify(now);
      userCoupon.setStatus(Status.VALID.getValue());
      userCoupon.setAmount(coupon.getAmount());
      userCoupon.setCouponType(coupon.getType());
      userCoupon.setChannelType(coupon.getChannelType());
      userCoupon.setAmountLimit(coupon.getAmountLimit());
      userCouponDOs.add(userCoupon);
    }
    int insertNumber = userCouponDOMapper.batchInsert(userCouponDOs);
    if (insertNumber > 0) {
      LOGGER.info("用户id：" + userId + "，本次应插入用户优惠券数量：" + userCouponDOs.size() + "，本次实际插入用户优惠券数量："
          + insertNumber);
      return Result.getSuccDataResult(true);
    }
    return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "server db error");
  }

  /**
   * 根据id获取优惠券
   * 
   * @param id
   * @return
   */
  private CouponDO getCouponById(long id) {
    CouponDO couponDO = null;
    List<Object> timeObje = timecache.get(id);
    if (CollectionUtils.isEmpty(timeObje) || timeObje.size() < 2) {
      couponDO = couponDOMapper.selectByPrimaryKey(id);
      List<Object> result = Lists.newArrayList();
      result.add(couponDO);
      result.add(new Date().getTime());
      timecache.put(id, result);
      return couponDO;
    } else {
      List<Object> result = timeObje;
      couponDO = (CouponDO) timeObje.get(0);
      Long time = (Long) timeObje.get(1);
      long now = System.currentTimeMillis();
      if (null != time && now - time <= 300000) {// 5分钟
        return couponDO;
      } else {
        couponDO = couponDOMapper.selectByPrimaryKey(id);
        result.add(0, couponDO);
        result.add(1, now);
        timecache.put(id, result);
        return couponDO;
      }
    }
  }

  // 按照服务端配置的优惠券包发放，实际还是调用多个优惠券发放
  private List<Long> getCouponConfigPackage(int configId) {
    Result<SystemConfigDTO> configResult = systemPropertyService.getSystemConfig(configId);
    if (null != configResult && configResult.getSuccess() && null != configResult.getModule()) {
      String configStr = configResult.getModule().getConfigInfo();
      String configIdArray[] = configStr.split(",");
      List<Long> idList = Lists.newArrayList();
      for (String id : configIdArray) {
        if (StringUtils.isNotEmpty(id) && StringUtils.isNumeric(id)) {
          idList.add(Long.valueOf(id));
        }
      }
      if (CollectionUtils.isNotEmpty(idList)) {
        return idList;
      }
    }
    return Collections.emptyList();
  }

  @Override
  public Result<UserCouponDiscontResultDTO> selectMathCoupon(UserCouponDiscontQueryDTO query) {
    PromotionChainContext context = new PromotionChainContext();
    context.setBuyInfoList(query.getBuySkuDetail());
    context.setPayChannel(query.getPayType());
    context.setUserId(query.getUserId());
    // 获取用户优惠券
    List<UserCouponDO> userCouponDOs =
        getUserCoupons(query.getUserId(), null, null, (short) 1, null, false,0);
    if (CollectionUtils.isNotEmpty(userCouponDOs)) {
      List<UserCouponDTO> tmpResult = getCouponDTOByCouponDO(userCouponDOs);
      context.setUserCouponList(tmpResult);
    }

    PromotionChainContext contextResult = null;
    if (null != query.getQueryType() && query.getQueryType() == 1) {
      // 获取所有可用优惠券
      contextResult = promotionProcessor.selectMathAllCoupon(context);
    } else {
      contextResult = promotionProcessor.selectMathFirstCoupon(context);
    }
    if (null != contextResult) {
      UserCouponDiscontResultDTO discountResult = new UserCouponDiscontResultDTO();
      // 设置商品无优惠时的总价
      discountResult.setOriginalPrice(contextResult.getSkuPriceMaps().get(
          PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0"));
      // 设置商品优惠金额
      discountResult.setDiscountPrice(contextResult.getPrice());
      // 设置商品实际价格
      discountResult.setActualPayPrice(discountResult.getOriginalPrice()
          - discountResult.getDiscountPrice());

      if (CollectionUtils.isNotEmpty(contextResult.getCanUseCouponList())) {
        discountResult.setUsedCouponList(contextResult.getCanUseCouponList());
      }
      return Result.getSuccDataResult(discountResult);
    }
    return Result.getErrDataResult(-1, "Query usercoupon not found");
  }

  @Override
  public Result<UserCouponDiscontResultDTO> calculateDiscountPriceWithCoupon(
      UserCouponDiscontQueryDTO query) {
    PromotionChainContext context = new PromotionChainContext();
    context.setBuyInfoList(query.getBuySkuDetail());
    context.setPayChannel(query.getPayType());
    context.setUserId(query.getUserId());
    if (null != query.getCouponId() && query.getCouponId() > 0) {
      UserCouponDTO inputCoupon = getUserCouponDTO(query.getCouponId());
      if (null != inputCoupon) {
        context.setUserSelectedCouponList(Arrays.asList(inputCoupon));
      }
    }

    PromotionChainContext contextResult =
        promotionProcessor.calculateDiscountPriceWithCoupon(context);
    if (null != contextResult) {
      UserCouponDiscontResultDTO discountResult = new UserCouponDiscontResultDTO();
      discountResult.setOriginalPrice(contextResult.getSkuPriceMaps().get(
          PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0"));

      discountResult.setDiscountPrice(contextResult.getPrice());
      if (discountResult.getOriginalPrice() >= discountResult.getDiscountPrice()) {
        discountResult.setActualPayPrice(discountResult.getOriginalPrice()
            - discountResult.getDiscountPrice());
      } else {
        discountResult.setActualPayPrice(0);
      }

      if (CollectionUtils.isNotEmpty(contextResult.getCanUseCouponList())) {
        discountResult.setUsedCouponList(contextResult.getCanUseCouponList());
        if (query.isCalculateAndUse()) {
        }
      }
      return Result.getSuccDataResult(discountResult);
    }
    return Result.getErrDataResult(-1, "Calculate usercoupon failed");
  }

  private boolean isLegalQueryPara(Integer input) {
    return (QueryTypeConstants.BY_COUPON_LIST.equals(input)
        || QueryTypeConstants.BY_COUPON_PACKAGE.equals(input) || QueryTypeConstants.BY_COUPON_ORDER
          .equals(input));
  }

  private boolean isLegalSendPara(Integer input) {
    return (SendTypeConstants.BY_COUPON_LIST.equals(input) || SendTypeConstants.BY_COUPON_PACKAGE
        .equals(input));
  }

  @Override
  public Result<List<CouponDTO>> getCouponByTypes(Collection<Short> couponTypes) {

    List<CouponDO> coupons = getCouponsByCouponTypes(couponTypes);

    List<CouponDTO> wrappers = new ArrayList<>();
    if (coupons != null && !coupons.isEmpty()) {
      for (CouponDO coupon : coupons) {
        CouponDTO wrapper = new CouponDTO();
        try {
          BeanUtils.copyProperties(wrapper, coupon);
          wrappers.add(wrapper);
        } catch (Exception e) {
          LOGGER.error("Fail to copy properties from coupon -> " + coupon.getId(), coupon);
        }
      }
    }

    return Result.getSuccDataResult(wrappers);
  }

  @Override
  public Result<List<UserCouponDTO>> getUserCouponByTypes(long userId, Collection<Short> couponTypes) {
    try {
      List<UserCouponDO> userCoupons =
          getUserCoupons(userId, couponTypes != null ? Lists.newArrayList(couponTypes) : null,
              null, (short) 1, null, false,0);
      List<UserCouponDTO> wrappers = new ArrayList<>();
      if (userCoupons != null && !userCoupons.isEmpty()) {
        for (UserCouponDO userCoupon : userCoupons) {
          UserCouponDTO wrapper = new UserCouponDTO();
          BeanUtils.copyProperties(wrapper, userCoupon);
          wrappers.add(wrapper);
        }
      }
      return Result.getSuccDataResult(wrappers);
    } catch (Exception e) {
      LOGGER.error("Fail to query UserCoupon for userId ->" + userId, e);
    }

    return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "server db error");
  }



  /**
   * 批量插入用户优惠券
   * 
   * @param userId
   * @param coupons
   * @return
   */
  private Result<Boolean> batchInsert(long userId, List<CouponDO> coupons) {
    if (userId <= 0 || CollectionUtils.isEmpty(coupons)) {
      LOGGER.error("parameter is error");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parameter is error");
    }
    List<UserCouponDO> userCouponDOs = Lists.newArrayList();
    for (CouponDO couponDO : coupons) {
      UserCouponDO userCoupon = new UserCouponDO();
      Date now = new Date();
      userCoupon.setGmtCreate(now);
      userCoupon.setGmtModify(now);
      userCoupon.setStatus(Status.VALID.getValue());
      userCoupon.setCouponEndTime(getEndTimeOfDay(couponDO.getGmtEnd()));
      userCoupon.setCouponId(couponDO.getId());
      userCoupon.setCouponRules(couponDO.getRules());
      userCoupon.setCouponStartTime(couponDO.getGmtStart());
      userCoupon.setCouponTitle(couponDO.getTitle());
      userCoupon.setAmount(couponDO.getAmount());
      userCoupon.setCouponType(couponDO.getType());
      userCoupon.setOrderId(0L);
      userCoupon.setUserId(userId);
      userCoupon.setChannelType(couponDO.getChannelType());
      userCoupon.setAmountLimit(couponDO.getAmountLimit());
      userCoupon.setStatus(Status.VALID.getValue());
      userCouponDOs.add(userCoupon);
    }
    int insertNumber = userCouponDOMapper.batchInsert(userCouponDOs);
    if (insertNumber > 0) {
      LOGGER.info("用户id：" + userId + "，本次应插入用户优惠券数量：" + userCouponDOs.size() + "，本次实际插入用户优惠券数量："
          + insertNumber);
      return Result.getSuccDataResult(true);
    }
    return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "server db error");
  }

  private long insert(UserCouponDO userCoupon) {
    Date now = new Date();
    userCoupon.setGmtCreate(now);
    userCoupon.setGmtModify(now);
    userCoupon.setStatus(Status.VALID.getValue());
    if (userCouponDOMapper.insert(userCoupon) > 0) {
      return userCoupon.getId();
    }
    return 0L;
  }

  /**
   * 根据优惠券类型获取优惠券
   * 
   * @param type 优惠券类型
   * @return
   */
  private List<CouponDO> getCouponsByCouponTypes(Collection<Short> couponTypes) {
    CouponQuery query = new CouponQuery();
    query.setType(couponTypes);
    query.setStatus((short) 1);
    return couponDOMapper.selectByQuery(query);
  }

  /**
   * 根据id获取优惠券
   * 
   * @param id
   * @return
   */
  private List<CouponDO> getCouponByIdList(List<Long> idList) {
    CouponQuery query = new CouponQuery();
    query.setCouponIds(idList);
    List<CouponDO> result = couponDOMapper.selectByQuery(query);
    if (CollectionUtils.isEmpty(result)) {
      return Lists.newArrayList();
    }
    return result;
  }

  /**
   * 取一天的最后时刻
   * 
   * @param date
   * @return
   */
  private Date getStartTimeOfDay(long  millis) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTimeInMillis(millis);
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    return calendar.getTime();
  }
  
  /**
   * 取一天的最后时刻
   * 
   * @param date
   * @return
   */
  private Date getEndTimeOfDay(Date date) {
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(date);
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    calendar.set(Calendar.MINUTE, 59);
    calendar.set(Calendar.SECOND, 58);
    return calendar.getTime();
  }

  @Override
  public Result<Boolean> sendCouponPackageByIds(long userId, Collection<Long> couponIds,
      long duration) {
    if (userId <= 0 || CollectionUtils.isEmpty(couponIds)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parameter is error");
    }

    Result<? extends BaseUserDTO> userResult = userService.queryUserByUserId(userId, false);
    if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "user is not exist");
    }
    BaseUserDTO user = userResult.getModule();

    List<CouponDO> couponDOs = getCouponsByCouponIds(couponIds);// 获取优惠券
    if (CollectionUtils.isEmpty(couponDOs)) {
      return Result.getErrDataResult(ServerResultCode.COUPON_NOT_EXIST_ERROR,
          "coupon is not exist with those ids");
    }
    List<UserCouponDO> userCouponDOs =
        getUserCoupons(user.getUserId(), null, null != couponIds ? Lists.newArrayList(couponIds)
            : null, null, null, false,0);// 获取该用户当前类型的优惠券
    Set<Long> exist = new HashSet<>();
    for (UserCouponDO userCouponDO : userCouponDOs) {
      exist.add(userCouponDO.getCouponId());
    }
    List<CouponDO> couponsInsert = Lists.newArrayList();
    for (CouponDO couponDO : couponDOs) {
      if (!exist.contains(couponDO.getId())) {// 如果当前用户没有这种类型的优惠券，则发放
        Date now = new Date();
        if (null != couponDO.getId()
            && ((couponDO.getId() == 32 || couponDO.getId() == 33 || couponDO.getId() == 34))) {
        } else {
          couponDO.setGmtStart(now);
        }

        if (couponDO.getGmtEnd().after(now)) {
          // 优先逻辑，优先使用优惠券本身有效期
        } else if (null != couponDO.getGmtDayDuration() && couponDO.getGmtDayDuration() > 0) {
          couponDO.setGmtEnd(new Date(now.getTime()
              + (couponDO.getGmtDayDuration() * 24l * 3600 * 1000)));
        } else if (duration > 0) {
          couponDO.setGmtEnd(new Date(now.getTime() + duration));
        } else {
          couponDO.setGmtEnd(new Date(now.getTime() + DEFAULT_USER_COUPON_TIME));
        }
        couponsInsert.add(couponDO);
      }
    }
    if (CollectionUtils.isNotEmpty(couponsInsert)) {
      return batchInsert(user.getUserId(), couponsInsert);// 批量插入数据库
    } else {// 用户已经拥有该优惠券了
      return Result.getSuccDataResult(true);
    }
  }

  @Override
  public Result<List<UserCouponDTO>> getUserAllCouponByUserId(long userId) {
      try {
        List<UserCouponDO> userCoupons =
            getUserCoupons(userId, null, null, null, null, false,0);
        List<UserCouponDTO> wrappers = new ArrayList<>();
        if (userCoupons != null && !userCoupons.isEmpty()) {
          for (UserCouponDO userCoupon : userCoupons) {
            UserCouponDTO wrapper = new UserCouponDTO();
            BeanUtils.copyProperties(wrapper, userCoupon);
            wrappers.add(wrapper);
          }
        }
        return Result.getSuccDataResult(wrappers);
      } catch (Exception e) {
        LOGGER.error("Fail to query UserCoupon for userId ->" + userId, e);
      }

      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "server db error");

  }

  private List<CouponDO> getCouponsByCouponIds(Collection<Long> couponIds) {
    CouponQuery query = new CouponQuery();
    query.setCouponIds(couponIds);
    query.setStatus((short) 1);
    return couponDOMapper.selectByQuery(query);
  }



  private UserCouponDTO getUserCouponDTO(long couponId) {
    UserCouponDO userCoupon = userCouponDOMapper.selectByPrimaryKey(couponId);
    if (userCoupon != null) {
      UserCouponDTO wrapper = new UserCouponDTO();
      try {
        BeanUtils.copyProperties(wrapper, userCoupon);
        return wrapper;
      } catch (Exception e) {
        LOGGER.error("Fail to copy properties from userCoupon -> " + couponId, userCoupon);
      }
    }
    return null;
  }

  private long insertCoupon(long userId, CouponDO couponDO) {

    if (userId <= 0 || null == couponDO) {
      LOGGER.error("parameter is error");
      throw new IllegalArgumentException("parameter is error");
    }
    UserCouponDO userCoupon = getUserCoupon(userId, couponDO);
    return insert(userCoupon);
  }

  private UserCouponDO getUserCoupon(long userId, CouponDO couponDO) {
    UserCouponDO userCoupon = new UserCouponDO();
    userCoupon.setCouponEndTime(getEndTimeOfDay(couponDO.getGmtEnd()));
    userCoupon.setCouponId(couponDO.getId());
    userCoupon.setCouponRules(couponDO.getRules());
    userCoupon.setCouponStartTime(couponDO.getGmtStart());
    userCoupon.setCouponTitle(couponDO.getTitle());
    userCoupon.setAmount(couponDO.getAmount());
    userCoupon.setCouponType(couponDO.getType());
    userCoupon.setOrderId(0L);
    userCoupon.setUserId(userId);
    userCoupon.setChannelType(couponDO.getChannelType());
    userCoupon.setAmountLimit(couponDO.getAmountLimit());
    userCoupon.setStatus(Status.VALID.getValue());
    return userCoupon;
  }

  private UserCouponDO getItemCoupon(Long skuId,Integer price,Integer dayTime, CouponDO couponDO) {
    UserCouponDO userCoupon = new UserCouponDO();
    Date today = getStartTimeOfDay(System.currentTimeMillis());
    if(null == dayTime || dayTime <= 0){
      userCoupon.setCouponEndTime(getEndTimeOfDay(couponDO.getGmtEnd()));
    }else{
      userCoupon.setCouponEndTime(getEndTimeOfDay(DateUtils.addDays(today, dayTime)));
    }
    userCoupon.setCouponStartTime(today);
    userCoupon.setCouponId(couponDO.getId());
    userCoupon.setCouponTitle(couponDO.getTitle());
    userCoupon.setAmount(Double.valueOf(new BigDecimal(price).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN).toString()));
    userCoupon.setCouponType(couponDO.getType());
    userCoupon.setOrderId(0L);
    userCoupon.setUserId(skuId*10000);
    userCoupon.setChannelType(couponDO.getChannelType());
    userCoupon.setAmountLimit(couponDO.getAmountLimit());
    userCoupon.setStatus(Status.VALID.getValue());
    userCoupon.setCouponRules("");//字段不能为空
    return userCoupon;
  }

  // 按照优惠券类型发放
  @Override
  public Result<Boolean> sendCouponPackage(long userId, short couponType, long duration) {
    if (userId <= 0 || couponType <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parameter is error");
    }

    Result<? extends BaseUserDTO> userResult = userService.queryUserByUserId(userId, false);
    if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "user is not exist");
    }
    BaseUserDTO user = userResult.getModule();

    List<CouponDO> couponDOs = getCouponsByCouponTypes(Arrays.asList(couponType));// 获取优惠券
    if (CollectionUtils.isEmpty(couponDOs)) {
      return Result.getErrDataResult(ServerResultCode.COUPON_NOT_EXIST_ERROR,
          "coupon is not exist with this type");
    }
    List<UserCouponDO> userCouponDOs =
        getUserCoupons(user.getUserId(), Lists.newArrayList(couponType), null, (short) 1, null,
            false,0);// 获取该用户当前类型的优惠券
    Set<Long> exist = new HashSet<>();
    for (UserCouponDO userCouponDO : userCouponDOs) {
      exist.add(userCouponDO.getCouponId());
    }
    List<CouponDO> couponsInsert = Lists.newArrayList();
    for (CouponDO couponDO : couponDOs) {
      if (!exist.contains(couponDO.getId())) {// 如果当前用户没有这种类型的优惠券，则发放
        Date now = new Date();
        couponDO.setGmtStart(now);
        if (couponDO.getGmtEnd().after(now)) {
          // 优先逻辑，优先使用优惠券本身有效期
        } else if (null != couponDO.getGmtDayDuration() && couponDO.getGmtDayDuration() > 0) {
          couponDO.setGmtEnd(new Date(now.getTime()
              + (couponDO.getGmtDayDuration() * 24l * 3600 * 1000)));
        } else if (duration > 0) {
          couponDO.setGmtEnd(new Date(now.getTime() + duration));
        } else {
          couponDO.setGmtEnd(new Date(now.getTime() + DEFAULT_USER_COUPON_TIME));
        }
        couponsInsert.add(couponDO);
      }
    }
    if (CollectionUtils.isNotEmpty(couponsInsert)) {
      return batchInsert(user.getUserId(), couponsInsert);// 批量插入数据库
    } else {// 用户已经拥有该优惠券了
      return Result.getSuccDataResult(true);
    }
  }

  @Override
  public Result<Boolean> sendCouponByCouponId(long userId, long couponId, long duration) {
    if (userId <= 0 || couponId <= 0 || duration <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parameter is error");
    }

    com.xianzaishi.itemcenter.common.result.Result<? extends BaseUserDTO> userResult =
        userService.queryUserByUserId(userId, false);
    if (userResult == null || !userResult.getSuccess() || userResult.getModule() == null) {
      return Result.getErrDataResult(ServerResultCode.USER_UNEXIST, "user is not exist");
    }
    BaseUserDTO user = userResult.getModule();

    CouponDO couponDO = getCouponById(couponId);

    if (null == couponDO) {
      return Result.getErrDataResult(ServerResultCode.COUPON_NOT_EXIST_ERROR,
          "coupon is not exist with this type");
    }

    Date now = new Date();
    if (null != couponDO.getId()
        && ((couponDO.getId() == 32 || couponDO.getId() == 33 || couponDO.getId() == 34))) {
    } else {
      couponDO.setGmtStart(now);
    }
    if (couponDO.getGmtEnd().after(now)) {
      // 优先逻辑，优先使用优惠券本身有效期
    } else if (null != couponDO.getGmtDayDuration() && couponDO.getGmtDayDuration() > 0) {
      couponDO
          .setGmtEnd(new Date(now.getTime() + (couponDO.getGmtDayDuration() * 24 * 3600 * 1000)));
    } else if (duration > 0) {
      couponDO.setGmtEnd(new Date(now.getTime() + duration));
    } else {
      couponDO.setGmtEnd(new Date(now.getTime() + DEFAULT_USER_COUPON_TIME));
    }

    if (insertCoupon(user.getUserId(), couponDO) > 0) {// 赠送优惠券成功
      return Result.getSuccDataResult(true);
    }
    return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "server db error");
  }

  /**
   * 翻译优惠券
   * 
   * @param input
   * @return
   */
  private List<UserCouponDTO> getCouponDTOByCouponDO(List<UserCouponDO> input) {
    List<UserCouponDTO> result = Lists.newArrayList();
    for (UserCouponDO coupon : input) {
      UserCouponDTO tmp = new UserCouponDTO();
      org.springframework.beans.BeanUtils.copyProperties(coupon, tmp);
      result.add(tmp);
    }
    return result;
  }
}
