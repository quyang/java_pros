package com.xianzaishi.couponcenter.component.promotion;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountCalResultDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountDetailDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountInfoDO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;

public class PromotionChainContext {

  // 付款sku列表
  private Map<Long, ItemCommoditySkuDTO> skuMaps;
  
  // 过滤出的临期商品列表
  private Map<Long,List<Long>> nearShelfLifeSku;
  
  // 临期商品优惠价格
  private Map<Long,DiscountDetailDTO> nearShelfLifePrice;
  
  // skuPriceMaps 记录总价的key，其余的价格先根据商品标为key.以p_0开始的代表除去不享受优惠的总价格，p_64tag0代表不享受优惠总价格，p_total代表全部价格
  public static final String TOTAL_PRICE_PRE_KEY = "p_";
  
  // 记录参与优惠商品总价格，在计算所有商品总价格的unit中使用，通过p_id_couponId的格式方式记录针对不同优惠券，商品当前可参与价格达到多少
  public static final String TOTAL_PRICE_PRE_ID_KEY = "p_id_";
  
 //记录不参与任何优惠时的商品价格，在计算所有商品总价格的unit中使用，通过p_sku_skuId的格式方式记录针对不同优惠券
 public static final String TOTAL_PRICE_IGNOREDISCOUNT_ID_KEY = "p_sku_";

  // 记录不参与优惠商品总价格，在计算所有商品总价格的unit中使用
  public  static final String TOTAL_PRICE_PRE_64tag0_KEY = "64tag0";

  // sku价格分别计算
  private Map<String, Integer> skuPriceMaps;

  // 优惠券数量统计
  private Map<String, Integer> couponCount;

  /**
   * 购买情况
   */
  private List<BuySkuInfoDTO> buyInfoList;

  // 付款渠道
  private int payChannel;

  /**
   * 用户id
   */
  private Long userId;

  // 优惠券类型集合
  private List<CouponDTO> CouponList;

  // 用户优惠券可用列表
  private List<UserCouponDTO> userCouponList;

  // 用户选择使用优惠列表
  private List<UserCouponDTO> userSelectedCouponList;

  // 符合使用优惠列表
  private List<UserCouponDTO> canUseCouponList;

  // 使用用户选择优惠券userSelectedCouponList后，减去多少钱
  private int price;

  // 可以赠送的优惠券集合
  private Set<CouponDTO> canSendCouponList;

  /**
   * 按N件M折规则计算之后的优惠信息
   * key的规则：类型的code+类型的value[+skuid]，中括号为可选（当针对商品时，需要中括号中内容；若是全局的则不需要）
   */
  private Map<String,DiscountCalResultDTO> fullNUseMDiscount;

  /**
   * 优惠总金额
   */
  private int discountTotalrice;

  /**
   * 参与优惠计价基础金额，不参与优惠选择，只参与优惠券计价时使用
   */
  private int discountBasicPrice;
  /**
   * 按满减规则计算之后的优惠信息
   */
//  private Map<String,DiscountCalResultDTO> fullReduceDiscount;

  /**
   * 承载用户所有的优惠数据
   */
  private List<DiscountInfoDO> userDiscountList;

  /**
   * 承载计算结果，合并优惠券和默认优惠统一到一起
   */
  private DiscountResultDTO discountResult;
  
  public Map<Long, ItemCommoditySkuDTO> getSkuMaps() {
    return skuMaps;
  }

  public void setSkuMaps(Map<Long, ItemCommoditySkuDTO> skuMaps) {
    this.skuMaps = skuMaps;
  }

  public Map<String, Integer> getCouponCount() {
    return couponCount;
  }

  public void setCouponCount(Map<String, Integer> couponCount) {
    this.couponCount = couponCount;
  }

  public Map<String, Integer> getSkuPriceMaps() {
    return skuPriceMaps;
  }

  public void setSkuPriceMaps(Map<String, Integer> skuPriceMaps) {
    this.skuPriceMaps = skuPriceMaps;
  }

  public int getPayChannel() {
    return payChannel;
  }

  public void setPayChannel(int payChannel) {
    this.payChannel = payChannel;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public List<BuySkuInfoDTO> getBuyInfoList() {
    return buyInfoList;
  }

  public void setBuyInfoList(List<BuySkuInfoDTO> buyInfoList) {
    this.buyInfoList = buyInfoList;
  }

  public List<CouponDTO> getCouponList() {
    return CouponList;
  }

  public void setCouponList(List<CouponDTO> couponList) {
    CouponList = couponList;
  }

  public List<UserCouponDTO> getUserCouponList() {
    return userCouponList;
  }

  public void setUserCouponList(List<UserCouponDTO> userCouponList) {
    this.userCouponList = userCouponList;
  }

  public List<UserCouponDTO> getUserSelectedCouponList() {
    return userSelectedCouponList;
  }

  public void setUserSelectedCouponList(List<UserCouponDTO> userSelectedCouponList) {
    this.userSelectedCouponList = userSelectedCouponList;
  }

  public List<UserCouponDTO> getCanUseCouponList() {
    return canUseCouponList;
  }

  public void setCanUseCouponList(List<UserCouponDTO> canUseCouponList) {
    this.canUseCouponList = canUseCouponList;
  }

  public Set<CouponDTO> getCanSendCouponList() {
    return canSendCouponList;
  }

  public void setCanSendCouponList(Set<CouponDTO> canSendCouponList) {
    this.canSendCouponList = canSendCouponList;
  }

  public Map<String, DiscountCalResultDTO> getFullNUseMDiscount() {
    return fullNUseMDiscount;
  }

  public void setFullNUseMDiscount(Map<String, DiscountCalResultDTO> fullNUseMDiscount) {
    this.fullNUseMDiscount = fullNUseMDiscount;
  }

  public int getDiscountTotalrice() {
    return discountTotalrice;
  }

  public void setDiscountTotalrice(int discountTotalrice) {
    this.discountTotalrice = discountTotalrice;
  }

  public List<DiscountInfoDO> getUserDiscountList() {
    return userDiscountList;
  }

  public void setUserDiscountList(List<DiscountInfoDO> userDiscountList) {
    this.userDiscountList = userDiscountList;
  }

  public DiscountResultDTO getDiscountResult() {
    return discountResult;
  }

  public void setDiscountResult(DiscountResultDTO discountResult) {
    this.discountResult = discountResult;
  }

  public Map<Long, List<Long>> getNearShelfLifeSku() {
    return nearShelfLifeSku;
  }

  public void setNearShelfLifeSku(Map<Long, List<Long>> nearShelfLifeSku) {
    this.nearShelfLifeSku = nearShelfLifeSku;
  }

  public Map<Long, DiscountDetailDTO> getNearShelfLifePrice() {
    return nearShelfLifePrice;
  }

  public void setNearShelfLifePrice(Map<Long, DiscountDetailDTO> nearShelfLifePrice) {
    this.nearShelfLifePrice = nearShelfLifePrice;
  }

  public int getDiscountBasicPrice() {
    return discountBasicPrice;
  }

  public void setDiscountBasicPrice(int discountBasicPrice) {
    this.discountBasicPrice = discountBasicPrice;
  }
  
  //  public Map<String, DiscountCalResultDTO> getFullReduceDiscount() {
//    return fullReduceDiscount;
//  }
//
//  public void setFullReduceDiscount(Map<String, DiscountCalResultDTO> fullReduceDiscount) {
//    this.fullReduceDiscount = fullReduceDiscount;
//  }
}
