package com.xianzaishi.couponcenter.component.promotion.unit;

import java.math.BigDecimal;
import java.util.Arrays;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 根据用户选择的优惠券给用户打折价格
 * @author zhancang
 */
@Component(value = "getSkuCouponDiscountPriceUnit")
public class GetSkuCouponDiscountPriceUnit extends ProcessorUnit {

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    if (CollectionUtils.isEmpty(context.getUserSelectedCouponList())) {
      return Result.getSuccDataResult(true);
    }

    Integer tprice = context.getSkuPriceMaps().get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0");
    BigDecimal totalPrice = new BigDecimal("0");
    if(null!=tprice)
      totalPrice = new BigDecimal(String.valueOf(tprice)) ;
    Integer priceTag64 = context.getSkuPriceMaps().get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + PromotionChainContext.TOTAL_PRICE_PRE_64tag0_KEY);
    BigDecimal totalPriceTag64 = new BigDecimal("0");
    if(null!=priceTag64)
      totalPriceTag64 = new BigDecimal(String.valueOf(priceTag64));

//    if(context.getDiscountTotalrice()>0||(totalPrice.intValue()==0&&totalPriceTag64.intValue()>0)){
//      return Result.getSuccDataResult(true);
//    }

    for (UserCouponDTO coupon : context.getUserSelectedCouponList()) {
      int discountpirce = calculate(coupon, context.getSkuPriceMaps(),0);
      if(discountpirce > 0){
        context.setPrice(discountpirce);
        context.setCanUseCouponList(Arrays.asList(coupon));
        return Result.getSuccDataResult(true);
      }
    }
    return Result.getSuccDataResult(true);
  }
}
