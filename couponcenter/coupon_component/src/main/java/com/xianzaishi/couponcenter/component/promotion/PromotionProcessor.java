package com.xianzaishi.couponcenter.component.promotion;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.xianzaishi.couponcenter.component.promotion.unit.ProcessorUnit;
import com.xianzaishi.itemcenter.common.result.Result;

public class PromotionProcessor {

  protected static final Logger log = LoggerFactory.getLogger(PromotionProcessor.class);

  /**
   * 通过优惠券降价列表--结算时使用此处来计算优惠了多少价格
   */
  private List<ProcessorUnit> processCalculateDiscountPrice;

  /**
   * 选择用户匹配优惠券列表-默认计算出来的优惠券，在下单时时默认选择
   */
  private List<ProcessorUnit> processMathFirstCoupon;

  /**
   * 选择用户匹配优惠券列表-展示所有优惠券，在下单时可以手动选择
   */
  private List<ProcessorUnit> processMathAllCoupon;

  /**
   * 通过规则计算给用户送多少优惠券-送优惠券
   */
  private List<ProcessorUnit> processSendCoupon;

  /**
   * 通过规则计算满N件N折后的优惠信息
   */
  private List<ProcessorUnit> processorFullNUseMDiscount;

  /**
   * 通过购买信息和优惠券计算用户可享受优惠详细计算优惠价格
   */
  private List<ProcessorUnit> processDiscount;
  
  /**
   * 通过购买信息计算用户可享受优惠券列表
   */
  private List<ProcessorUnit> processCoupon;
  
  /**
   * 通过规则计算满额减后的优惠信息
   */
  // private List<ProcessorUnit> processorFullReduceDiscount;

  /**
   * 计算打折价格
   * 
   * @param context
   * @return
   */
  public PromotionChainContext calculateDiscountPriceWithCoupon(PromotionChainContext context) {
    return getResultNoCache(context, processCalculateDiscountPrice);
  }

  /**
   * 查询符合条件第一个优惠券
   * 
   * @param context
   * @return
   */
  public PromotionChainContext selectMathFirstCoupon(PromotionChainContext context) {
    return getResultNoCache(context, processMathFirstCoupon);
  }

  /**
   * 查询符合条件所有优惠券
   * 
   * @param context
   * @return
   */
  public PromotionChainContext selectMathAllCoupon(PromotionChainContext context) {
    return getResultCacheContext(context, processMathAllCoupon);
  }

  /**
   * 查询符合条件所有优惠券
   * 
   * @param context
   * @return
   */
  public PromotionChainContext processSendCouponRule(PromotionChainContext context) {
    return getResultNoCache(context, processSendCoupon);
  }

  /**
   * 获取按照N件M折规则计算后的优惠
   * 
   * @param context
   * @return
   */
  public PromotionChainContext processorFullNUseMDiscount(PromotionChainContext context) {
    return getResultCacheContext(context, processorFullNUseMDiscount);
  }

  /**
   * 计算所有优惠信息
   * 
   * @param context
   * @return
   */
  public PromotionChainContext calDiscount(PromotionChainContext context) {
    return getResultNoCache(context, processDiscount);
  }
  
  /**
   * 计算所有优惠信息
   * 
   * @param context
   * @return
   */
  public PromotionChainContext calCouponList(PromotionChainContext context) {
    return getResultNoCache(context, processCoupon);
  }
  
  /**
   * 配置发送时的独立逻辑
   * 
   * @param context
   * @param process
   * @return
   */
  private PromotionChainContext getResultNoCache(PromotionChainContext context,
      List<ProcessorUnit> process) {
    if (null == context) {
      log.error("[PromotionProcessorUnit]Input parameter context is empty,return null");
      return null;
    }
    for (ProcessorUnit processUnit : process) {
      Result<Boolean> processResult = processUnit.process(context);
      if (null == processResult || !processResult.getSuccess()) {
        return null;
      }
    }
    return context;
  }


  private PromotionChainContext getResultCacheContext(PromotionChainContext context,
      List<ProcessorUnit> process) {
    if (null == context) {
      log.error("[PromotionProcessorUnit]Input parameter context is empty,return null");
      return null;
    }
    String errorInfo = "";
    if (CollectionUtils.isEmpty(context.getBuyInfoList())) {
      errorInfo = "[PromotionProcessorUnit]Input parameter skuIdList is empty,return null";
      log.error(errorInfo);
      return null;
    }
    context = ProcessorUnit.getChainContext(context);
    for (ProcessorUnit processUnit : process) {
      Result<Boolean> processResult = processUnit.process(context);
      if (null == processResult || !processResult.getSuccess()) {
        return null;
      }
    }
    return context;
  }

  public List<ProcessorUnit> getProcessCalculateDiscountPrice() {
    return processCalculateDiscountPrice;
  }

  public void setProcessCalculateDiscountPrice(List<ProcessorUnit> processCalculateDiscountPrice) {
    this.processCalculateDiscountPrice = processCalculateDiscountPrice;
  }

  public List<ProcessorUnit> getProcessMathFirstCoupon() {
    return processMathFirstCoupon;
  }

  public void setProcessMathFirstCoupon(List<ProcessorUnit> processMathFirstCoupon) {
    this.processMathFirstCoupon = processMathFirstCoupon;
  }

  public List<ProcessorUnit> getProcessMathAllCoupon() {
    return processMathAllCoupon;
  }

  public void setProcessMathAllCoupon(List<ProcessorUnit> processMathAllCoupon) {
    this.processMathAllCoupon = processMathAllCoupon;
  }

  public List<ProcessorUnit> getProcessSendCoupon() {
    return processSendCoupon;
  }

  public void setProcessSendCoupon(List<ProcessorUnit> processSendCoupon) {
    this.processSendCoupon = processSendCoupon;
  }

  public List<ProcessorUnit> getProcessorFullNUseMDiscount() {
    return processorFullNUseMDiscount;
  }

  public void setProcessorFullNUseMDiscount(List<ProcessorUnit> processorFullNUseMDiscount) {
    this.processorFullNUseMDiscount = processorFullNUseMDiscount;
  }

  public List<ProcessorUnit> getProcessDiscount() {
    return processDiscount;
  }

  public void setProcessDiscount(List<ProcessorUnit> processDiscount) {
    this.processDiscount = processDiscount;
  }

  public List<ProcessorUnit> getProcessCoupon() {
    return processCoupon;
  }

  public void setProcessCoupon(List<ProcessorUnit> processCoupon) {
    this.processCoupon = processCoupon;
  }

  // public List<ProcessorUnit> getProcessorFullReduceDiscount() {
  // return processorFullReduceDiscount;
  // }
  //
  // public void setProcessorFullReduceDiscount(List<ProcessorUnit> processorFullReduceDiscount) {
  // this.processorFullReduceDiscount = processorFullReduceDiscount;
  // }
}
