package com.xianzaishi.couponcenter.component.discount;/**
 * Created by Administrator on 2016/12/27 0027.
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.discount.DiscountInfoService;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountCalResultDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountRefDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.discount.query.DiscountQueryDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.component.promotion.PromotionProcessor;
import com.xianzaishi.couponcenter.component.promotion.util.EveryBodyPromotionChainContextUtils;
import com.xianzaishi.couponcenter.component.usercoupon.UserCouponServiceImpl;
import com.xianzaishi.couponcenter.dal.coupon.dao.UserCouponDOMapper;
import com.xianzaishi.couponcenter.dal.coupon.dataobject.UserCouponDO;
import com.xianzaishi.couponcenter.dal.coupon.query.UserCouponQuery;
import com.xianzaishi.couponcenter.dal.discount.dao.DiscountRefDOMapper;
import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountRefDO;
import com.xianzaishi.couponcenter.dal.discount.query.DiscountRefQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;

/**
 * 优惠活动服务实现
 *
 * @author jianmo
 * @create 2016-12-27 下午 4:46
 **/
@Service("discountInfoService")
public class DiscountInfoServiceImpl implements DiscountInfoService {

    @Autowired
    private PromotionProcessor promotionProcessor;
    
    @Autowired
    private DiscountRefDOMapper discountRefDOMapper;

    @Autowired
    private UserCouponDOMapper userCouponDOMapper;
    
    private Logger LOGGER = LoggerFactory.getLogger(UserCouponServiceImpl.class);
    
    @Override
    public Result<DiscountResultDTO> calculate(DiscountQueryDTO query) {
      PromotionChainContext context = new PromotionChainContext();
      context.setBuyInfoList(query.getBuySkuDetail());
      context.setUserId(query.getUserId());
      context.setPayChannel(query.getPayType());
      if(null != query.getDiscountBasicPrice() && query.getDiscountBasicPrice() > 0){
        context.setDiscountBasicPrice(query.getDiscountBasicPrice());
      }
      PromotionChainContext contextResult = null;
      if (null != query.getCouponId() && query.getCouponId() > 0) {
        UserCouponDTO inputCoupon = getUserCouponDTO(query.getCouponId());
        if (null != inputCoupon) {
          context.setUserSelectedCouponList(Arrays.asList(inputCoupon));
        }
        contextResult = promotionProcessor.calDiscount(context);
      }else{
        // 获取用户优惠券
        List<UserCouponDO> userCouponDOs =
            getUserCoupons(query.getUserId(), null, null, (short) 1, null, true);
        if (CollectionUtils.isNotEmpty(userCouponDOs)) {
          List<UserCouponDTO> tmpResult = getCouponDTOByCouponDO(userCouponDOs);
          context.setUserCouponList(tmpResult);
        }
        contextResult = promotionProcessor.calCouponList(context);
      }
      
      
      if(null != contextResult){
        DiscountResultDTO discountResultDTO = contextResult.getDiscountResult();
          return Result.getSuccDataResult(discountResultDTO);
      }
      return Result.getErrDataResult(-1, "Calculation FullNUseM not found");
    }

    /**
     * 优惠计算的入口
     * @param query
     * @return
     */
    @Override
    public Result<Map<String,DiscountCalResultDTO>> cal(DiscountQueryDTO query) {
        PromotionChainContext context = new PromotionChainContext();
        context.setBuyInfoList(query.getBuySkuDetail());
        context.setUserId(query.getUserId());
        PromotionChainContext contextResult = promotionProcessor.processorFullNUseMDiscount(context);
        if(null != contextResult){
            Map<String, DiscountCalResultDTO> discountCalResultDTOMap = contextResult.getFullNUseMDiscount();
            return Result.getSuccDataResult(discountCalResultDTOMap);
        }
        return Result.getErrDataResult(-1, "Calculation FullNUseM not found");
    }

    @Override
    public Result<Boolean> update(DiscountRefDTO disocuntRel) {
      if(null == disocuntRel){
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "Parameter input is null");
      }
      
      DiscountRefDO ref = new DiscountRefDO();
      BeanUtils.copyProperties(disocuntRel, ref);
      boolean udateResult = discountRefDOMapper.updateByPrimaryKeySelective(ref) == 1;
      if(udateResult){
        return Result.getSuccDataResult(true);
      }else{
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "updata failed,db updata failed");
      }
    }
    
    @Override
    public Result<Boolean> updateByUserList(List<Integer> detailId, Long userId, Short status) {
      if(CollectionUtils.isEmpty(detailId) || null == userId || null == status){
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "Input para detailid,userId or status is null");
      }
      discountRefDOMapper.updateByUserList(detailId, userId, status);
      return Result.getSuccDataResult(true);
    }

    @Override
    public Result<Boolean> addOrUpdateRelation(List<DiscountRefDTO> disocuntRelList) {
      if(CollectionUtils.isEmpty(disocuntRelList)){
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "Parameter input is null");
      }
      List<Long> targetIdList = Lists.newArrayList();
      for(DiscountRefDTO relation:disocuntRelList){
        targetIdList.add(relation.getRefId());
      }
      
      DiscountRefQuery query = new DiscountRefQuery();
      query.setTargetIdList(targetIdList);
      query.setDiscountId(disocuntRelList.get(0).getDiscountDetaiId());
      
      List<DiscountRefDO> queryResult = discountRefDOMapper.select(query);
      
      Map<String,DiscountRefDO> resultM = Maps.newHashMap();
      for(DiscountRefDO ref:queryResult){
        String key = new StringBuilder().append(ref.getDiscountDetaiId())
            .append("_").append(ref.getRefId())
            .append("_").append(ref.getRefType()).toString();
        resultM.put(key, ref);
      }
      List<DiscountRefDO> insertList = Lists.newArrayList();
      List<DiscountRefDO> updateList = Lists.newArrayList();
      for(DiscountRefDTO ref:disocuntRelList){
        String key = new StringBuilder().append(ref.getDiscountDetaiId())
            .append("_").append(ref.getRefId())
            .append("_").append(ref.getRefType()).toString();
        DiscountRefDTO value = resultM.get(key);
        if(null == value){
          DiscountRefDO refDO = new DiscountRefDO();
          BeanUtils.copyProperties(ref, refDO);
          insertList.add(refDO);
        }else if(!value.getRefStatus().equals(ref.getRefStatus())){
          DiscountRefDO refDO = new DiscountRefDO();
          BeanUtils.copyProperties(ref, refDO);
          refDO.setId(value.getId());
          updateList.add(refDO);
        }
      }
      
      int insertCount = 0;
      int updateCount = 0;
      if(CollectionUtils.isNotEmpty(insertList)){
        insertCount = discountRefDOMapper.batchinsert(insertList);
        if(insertCount != insertList.size()){
          return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "Insert relation failed");
        }
      }
      if(CollectionUtils.isNotEmpty(updateList)){
        for(DiscountRefDO refDO:updateList){
          updateCount = discountRefDOMapper.updateByPrimaryKeySelective(refDO);
          if(1!= updateCount){
            return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "Update relation failed");
          }
        }
      }
      return Result.getSuccDataResult(true);
    }
    public void removePromotionChainContext(Long uuid) {
        EveryBodyPromotionChainContextUtils.removePromotionChainContext(uuid);
    }
    
    private UserCouponDTO getUserCouponDTO(long couponId) {
      UserCouponDO userCoupon = userCouponDOMapper.selectByPrimaryKey(couponId);
      if (userCoupon != null) {
        UserCouponDTO wrapper = new UserCouponDTO();
        try {
          BeanUtils.copyProperties(userCoupon,wrapper);
          return wrapper;
        } catch (Exception e) {
          LOGGER.error("Fail to copy properties from userCoupon -> " + couponId, userCoupon);
        }
      }
      return null;
    }
    
    private List<UserCouponDO> getUserCoupons(long userId, List<Short> couponTypes,
        List<Long> couponIds, Short couponStatus, Long orderId, boolean filterExpiredCoupon) {

      List<UserCouponDO> results = new ArrayList<>();

      try {
        UserCouponQuery query = new UserCouponQuery();
        query.setUserId(userId);
        if (CollectionUtils.isNotEmpty(couponTypes)) {
          query.setCouponTypes(couponTypes);
        }
        if (CollectionUtils.isNotEmpty(couponIds)) {
          query.setCouponIds(couponIds);
        }
        if (null != orderId && orderId > 0) {
          query.setOid(orderId);
        }
        query.setStatus(couponStatus);
        if (filterExpiredCoupon) {
          query.setCouponEndTimeRangeEnd(new Date());
        }
        query.setSortByAmount();
        List<UserCouponDO> db = userCouponDOMapper.selectByQuery(query);
        if (CollectionUtils.isNotEmpty(db)) {
          results.addAll(db);
        }
      } catch (Exception e) {
        LOGGER.error("Fail to query user coupons by couponTypes", e);
      }
      LOGGER.info(JackSonUtil.getJson(results));
      return results;
    }
    
    /**
     * 翻译优惠券
     * 
     * @param input
     * @return
     */
    private List<UserCouponDTO> getCouponDTOByCouponDO(List<UserCouponDO> input) {
      List<UserCouponDTO> result = Lists.newArrayList();
      for (UserCouponDO coupon : input) {
        UserCouponDTO tmp = new UserCouponDTO();
        org.springframework.beans.BeanUtils.copyProperties(coupon, tmp);
        result.add(tmp);
      }
      return result;
    }
}
