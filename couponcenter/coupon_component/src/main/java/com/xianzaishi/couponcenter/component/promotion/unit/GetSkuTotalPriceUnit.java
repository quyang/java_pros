package com.xianzaishi.couponcenter.component.promotion.unit;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountDetailDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.dal.coupon.dataobject.UserCouponDO;
import com.xianzaishi.itemcenter.client.itemsku.constants.SkuConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemSkuTagsDTO.SkuTagDefinitionConstants;
import com.xianzaishi.itemcenter.client.itemsku.dto.SkuTagDetail;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 计算sku所有的价格，包括各种优惠价格
 * 
 * @author zhancang
 */
@Component(value = "getSkuTotalPriceUnit")
public class GetSkuTotalPriceUnit extends ProcessorUnit {

  private static final String DISCOUNT_NAME = "店铺A类促销优惠";
  
  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    String errorInfo = "";
    if (CollectionUtils.isEmpty(context.getSkuMaps().keySet())
        || CollectionUtils.isEmpty(context.getBuyInfoList())) {
      errorInfo = "[PromotionProcessorUnit]Input parameter skuIdList is empty,return null";
      log.error(errorInfo);
      return Result.getErrDataResult(-1, errorInfo);
    }
    // context = this.getChainContext(context);
    if (MapUtils.isNotEmpty(context.getSkuPriceMaps())){
      return Result.getSuccDataResult(true);
    }
    Map<String, Integer> skuPriceMaps = Maps.newHashMap();
    Map<Long, ItemCommoditySkuDTO> skuMaps = context.getSkuMaps();

    Map<Long, List<Long>> nearShelfLifeSku = context.getNearShelfLifeSku();
    Map<Long, DiscountDetailDTO> calNearShelfLifeSku = Maps.newHashMap();//真实计算过程折扣价格
    
    //初始化总价格为0，出现异常时后续不至于出错
    addPrice(0, PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0", skuPriceMaps);

    int totalPrice = 0;//所有不包含优惠时的总价格
    for (BuySkuInfoDTO skuInfo : context.getBuyInfoList()) {
      String buyCountStr = skuInfo.getBuyCount();
      long skuId = skuInfo.getSkuId();
      ItemCommoditySkuDTO sku = skuMaps.get(skuId);
      if(sku==null){
        continue;
      }
      
      //过滤部分标签，主要过滤掉打标不参与优惠，又使用了临期标签，忽略这种标签，依然使用原价
      cleanNearShelfLifeSku(sku, context, nearShelfLifeSku);
      Integer orinalPrice = getOrinalPrice(sku, buyCountStr, context);//计算不包含优惠时的价格
      addPrice(orinalPrice, PromotionChainContext.TOTAL_PRICE_PRE_KEY + "total", skuPriceMaps);
      addPrice(orinalPrice, PromotionChainContext.TOTAL_PRICE_IGNOREDISCOUNT_ID_KEY + sku.getSkuId(), skuPriceMaps);
      List<Long> discountIdList = Lists.newArrayList();
      Integer discount = getDiscount(sku, buyCountStr, context, nearShelfLifeSku,discountIdList);//计算临期降价多少，总价格，记录使用的优惠id
      Integer skuTotalPrice = orinalPrice;//除去临期优惠后的价格，可以继续参与优惠价计价
      totalPrice = totalPrice + skuTotalPrice;
      if(discount > 0){
        //获取优惠明细，一个sku一个记录，即使买多个数量
        DiscountDetailDTO disDetail = getDisDetail((orinalPrice-discount), orinalPrice, skuId, context,discountIdList);
        calNearShelfLifeSku.put(sku.getSkuId(), disDetail);
        skuTotalPrice = skuTotalPrice - discount;//扣除优惠后的价格才参与后续优惠券计算
      }
      
      // 增加打不支持优惠标商品时，不会计入总价
      if (sku.skuContainTag(SkuConstants.SKU_IGNORE_PROMOTION)) {
        addPrice(skuTotalPrice, PromotionChainContext.TOTAL_PRICE_PRE_KEY
            + PromotionChainContext.TOTAL_PRICE_PRE_64tag0_KEY, skuPriceMaps);
        continue;
      }
      //该key记录所有默认价格，即默认的未优惠前的价格,扣除不能享受优惠商品和临期是商品价格后的总价格
      addPrice(skuTotalPrice, PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0", skuPriceMaps);
      //记录不同标的优惠  老版本满减计价规律，删除不用，已经废弃
//      if (null != sku.getTags() && sku.getTags() > 0) {
//        int tag = sku.getTags();
//        for (int i = 1; i <= tag; i = i << 1) {
//          if ((i & tag) == i) {
//            addPrice(skuTotalPrice, PromotionChainContext.TOTAL_PRICE_PRE_KEY + i, skuPriceMaps);
//          }
//        }
//      }
      
      String skuTagContents = sku.queryTagContent();
      List<SkuTagDetail> skuTagDetails = JackSonUtil.jsonToList(skuTagContents, SkuTagDetail.class);
      if(CollectionUtils.isEmpty(skuTagDetails)){
        continue;
      }
      SkuTagDetail tagDetail = null;
      for(SkuTagDetail skuTagDetail : skuTagDetails){
        if(skuTagDetail.getTagId().equals(SkuTagDefinitionConstants.PROMOTION_TAG)){
          tagDetail = skuTagDetail;
          break;
        }
      }
      if(null == tagDetail){
        continue;
      }
      String tagRule = tagDetail.getTagRule();
      if(StringUtils.isNotBlank(tagRule)){
        List<String> promotionIds = Arrays.asList(tagRule.split(","));
        for(String promotionId : promotionIds){
          if(StringUtils.isBlank(promotionId) || !StringUtils.isNumeric(promotionId)){
            continue;
          }
          addPrice(skuTotalPrice, PromotionChainContext.TOTAL_PRICE_PRE_ID_KEY + promotionId, skuPriceMaps);
        }
      }
    }
    context.setSkuPriceMaps(skuPriceMaps);
    context.setNearShelfLifePrice(calNearShelfLifeSku);
    
    DiscountResultDTO discountResult = context.getDiscountResult();
    if(null == discountResult){
      discountResult = new DiscountResultDTO();
    }
    discountResult.setOrinalPrice(totalPrice);
    context.setDiscountResult(discountResult);
    return Result.getSuccDataResult(true);
  }
  
  /**
   * 过滤部分临期商品，标识为不参与优惠或者称重类目，不能支持临期标签
   * @param sku
   * @param buyCountStr
   * @param context
   * @param nearShelfLifeSku
   * @return
   */
  private void cleanNearShelfLifeSku(ItemCommoditySkuDTO sku, PromotionChainContext context, Map<Long, List<Long>> nearShelfLifeSku){
    if(null == nearShelfLifeSku){
      return;
    }
    if(sku.getIsSteelyardSku() && context.getPayChannel() == 2) {// 称重类型的，线下传来的是称重重量
      if(nearShelfLifeSku.keySet().contains(sku.getSkuId())){
        nearShelfLifeSku.remove(sku.getSkuId());
      }
    }
    if (sku.skuContainTag(SkuConstants.SKU_IGNORE_PROMOTION)) {
      if(nearShelfLifeSku.keySet().contains(sku.getSkuId())){
        nearShelfLifeSku.remove(sku.getSkuId());
      }
    }
    return;
  }
  
  /**
   * 计算sku价格
   */
  private Integer getOrinalPrice(ItemCommoditySkuDTO sku, String buyCountStr,
      PromotionChainContext context) {
    Integer skuPrice = sku.getDiscountPrice();
    Integer skuUnitPrice = sku.getDiscountUnitPrice();
    Integer skuTotalPrice = 0;

    if (sku.getIsSteelyardSku() && context.getPayChannel() == 2) {// 称重类型的，线下传来的是称重重量
      skuTotalPrice = new BigDecimal(buyCountStr).multiply(new BigDecimal(skuUnitPrice)).intValue();
    } else {
      skuTotalPrice = new BigDecimal(buyCountStr).multiply(new BigDecimal(skuPrice)).intValue();
    }
    return skuTotalPrice;
  }
  
  /**
   * 计算临期商品时，折扣多少钱
   */
  private Integer getDiscount(ItemCommoditySkuDTO sku, String buyCountStr,
      PromotionChainContext context, Map<Long, List<Long>> nearShelfLifeSku,List<Long> discountIdList) {

    if (sku.getIsSteelyardSku() && context.getPayChannel() == 2) {// 称重类型的，线下传来的是称重重量
      return 0;
    } else {
      if (null != nearShelfLifeSku && nearShelfLifeSku.containsKey(sku.getSkuId())) {
        List<Long> codList = nearShelfLifeSku.get(sku.getSkuId());
        if(CollectionUtils.isEmpty(codList)){
          return 0;
        }
        Integer skuPrice = sku.getDiscountPrice();
        List<UserCouponDO> couponList = super.getUserCouponByIdList(codList);
        int promotionPrice = 0;// 总优惠价格
        for (UserCouponDO coupon : couponList) {
          int price =
              new BigDecimal(coupon.getAmount().toString()).multiply(new BigDecimal(100))
                  .intValue();
          promotionPrice = promotionPrice + (skuPrice-price);
        }
        discountIdList.addAll(codList);
        return promotionPrice;
      }else{
        return 0;
      }
    }
  }
  /**
   * 计算sku价格
   */
//  private Integer getPrice(ItemCommoditySkuDTO sku, String buyCountStr,
//      PromotionChainContext context, Map<Long, List<Long>> nearShelfLifeSku,
//      Map<Long, DiscountDetailDTO> calNearShelfLifeSku) {
//    Integer skuPrice = sku.getDiscountPrice();
//    Integer skuUnitPrice = sku.getDiscountUnitPrice();
//    Integer skuTotalPrice = 0;
//
//    if (sku.getIsSteelyardSku() && context.getPayChannel() == 2) {// 称重类型的，线下传来的是称重重量
//      skuTotalPrice = new BigDecimal(buyCountStr).multiply(new BigDecimal(skuUnitPrice)).intValue();
//    } else {
//      skuTotalPrice = new BigDecimal(buyCountStr).multiply(new BigDecimal(skuPrice)).intValue();
//
//      if (null != nearShelfLifeSku && nearShelfLifeSku.containsKey(sku.getSkuId())) {
//        List<Long> codList = nearShelfLifeSku.get(sku.getSkuId());
//        List<UserCouponDO> couponList = super.getUserCouponByIdList(codList);
//        int skuCount = 0;// 使用优惠价格的商品数量
//        int promotionPrice = 0;// 使用优惠价格的优惠总价
//        for (UserCouponDO coupon : couponList) {
//          int price =
//              new BigDecimal(coupon.getAmount().toString()).multiply(new BigDecimal(100))
//                  .intValue();
//          promotionPrice = promotionPrice + price;
//          skuCount++;
//          if (skuCount >= new BigDecimal(buyCountStr).intValue()) {
//            break;
//          }
//        }
//
//        int orinalPriceCount = new BigDecimal(buyCountStr).intValue() - skuCount;// 剩余多少商品使用无优惠计算价格
//        int addPrice =
//            new BigDecimal(orinalPriceCount).multiply(new BigDecimal(skuPrice)).intValue();
//        int newPrice = promotionPrice + addPrice;// 真实使用价格
//
//        if(skuTotalPrice <= newPrice){
//          return skuTotalPrice;
//        }
//        DiscountDetailDTO disDetail = getDisDetail((skuTotalPrice-newPrice), skuTotalPrice, sku.getSkuId(), context);
//        calNearShelfLifeSku.put(sku.getSkuId(), disDetail);// 记录优惠金额
//        skuTotalPrice = newPrice;
//      }
//    }
//    return skuTotalPrice;
//  }
  
  private DiscountDetailDTO getDisDetail(int disPrice,int orinalPrice, long skuId, PromotionChainContext context,List<Long> discountIdList){
    DiscountDetailDTO disDetail = new DiscountDetailDTO();
    disDetail.setChannelType((short)context.getPayChannel());
    disDetail.setDiscountId(0L);
    disDetail.setDiscountName(DISCOUNT_NAME);
    disDetail.setDiscountPrice(disPrice);
    disDetail.setIsGetDiscount(true);
    disDetail.setItemId(0L);
    disDetail.setNextDiscountDetail("");
    disDetail.setOriginPrice(orinalPrice);
    disDetail.setIgnorePromotionPrice(orinalPrice);
    disDetail.setSkuId(skuId);
    disDetail.setThreshold(orinalPrice);
    disDetail.setType((short)13);
    if(CollectionUtils.isNotEmpty(discountIdList)){
      disDetail.setDiscountIds(discountIdList);
    }
    return disDetail;
  }
}
