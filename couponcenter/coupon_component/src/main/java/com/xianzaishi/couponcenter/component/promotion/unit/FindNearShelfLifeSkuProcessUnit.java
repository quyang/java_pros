package com.xianzaishi.couponcenter.component.promotion.unit;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.dal.coupon.dataobject.UserCouponDO;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 辨识出临期商品
 * 
 * @author zhancang
 */
@Component(value = "findNearShelfLifeSkuProcessUnit")
public class FindNearShelfLifeSkuProcessUnit extends ProcessorUnit {

  /**
   * 临期商品条码标志
   */
  private static final String NearShelfLifeSkuBarFlag = "027001";
  
  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    List<BuySkuInfoDTO> buySkuInfoDTOList = context.getBuyInfoList();
    String errorInfo = "";
    if (CollectionUtils.isEmpty(buySkuInfoDTOList)) {
      errorInfo = "[PromotionProcessorUnit]Input parameter skuIdList is empty,return null";
      log.error(errorInfo);
      return Result.getErrDataResult(-1, errorInfo);
    }

    boolean saleIsOnline = (context.getPayChannel() == 1);
    if (saleIsOnline) {// 临期商品处理逻辑只支持线下购买行为
      Result.getSuccDataResult(true);
    }
    UserCouponDO queryCouponResult = null;
    Map<Long, List<Long>> nearShelfLifeSku = Maps.newHashMap();
    for (BuySkuInfoDTO skuInfo : buySkuInfoDTOList) {
      String skuBarCodeInfo = skuInfo.getSkuBarCode();
      List<String> skuBarCodeList = Lists.newArrayList();
      if(StringUtils.isNotEmpty(skuBarCodeInfo)){
        String bararray[] = skuBarCodeInfo.split(",");
        for(String tmp:bararray){
          if(StringUtils.isNotEmpty(tmp)){
            skuBarCodeList.add(tmp);
          }
        }
      }
      if(CollectionUtils.isEmpty(skuBarCodeList)){
        continue;
      }
      for(String skuBarCode:skuBarCodeList){
        Long skuId = skuInfo.getSkuId();
        Long code = getItemDiscountFlag(skuBarCode, skuId);
        if (null == code || code <= 0) {
          continue;
        }
        queryCouponResult = getUserCouponById(code);
        // 非法的优惠券
        if (!isValidUserCoupon(queryCouponResult, skuId, context)) {
          continue;
        }

        addSku(skuId, code, nearShelfLifeSku);
      }
    }
    context.setNearShelfLifeSku(nearShelfLifeSku);
    return Result.getSuccDataResult(true);
  }

  /**
   * 获取临期商品id，027001+skuId+code
   * 
   * @param skuBarCode
   * @return
   */
  private Long getItemDiscountFlag(String skuBarCode, Long skuId) {
    if (StringUtils.isEmpty(skuBarCode) || skuBarCode.length() <= 14 || !skuBarCode.startsWith(NearShelfLifeSkuBarFlag) || null == skuId) {
      return null;
    }
    String code = skuBarCode.replaceFirst(NearShelfLifeSkuBarFlag, "");
    code = code.replaceFirst(skuId.toString(), "");
    Long id = Long.parseLong(code);
    if (null == id || id <= 0) {
      return null;
    }

    return id;
  }

  /**
   * 是否有效的优惠
   * 
   * @return
   */
  private boolean isValidUserCoupon(UserCouponDO userCoupon, Long skuId,
      PromotionChainContext context) {
    if (null == userCoupon) {
      return false;
    }
    Long couponSkuId = userCoupon.getUserId();
    Long cmpSkuId = skuId * 10000;// 系统存储的skuId乘以10000，防止和用户id重复
    if (couponSkuId.compareTo(cmpSkuId) != 0) {
      return false;// skuId不相同
    }

    if ((userCoupon.getChannelType() == 2 && context.getPayChannel() == 1)
        || (userCoupon.getChannelType() == 1 && context.getPayChannel() == 2)) {
      return false;
    }
    
    return super.checkCouponStatusAvailable(userCoupon);
  }

  private void addSku(Long skuId, Long code, Map<Long, List<Long>> nearShelfLifeSku) {
    if (null == nearShelfLifeSku) {
      return;
    }

    List<Long> codeList = nearShelfLifeSku.get(skuId);
    if (null == codeList) {
      codeList = Lists.newArrayList();
    }
    codeList.add(code);
    nearShelfLifeSku.put(skuId, codeList);
  }
}
