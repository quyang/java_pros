package com.xianzaishi.couponcenter.component.promotion.unit;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountCalResultDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountInfoDTO;
import com.xianzaishi.couponcenter.client.discount.enums.DiscountTypeEnum;
import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.couponcenter.dal.discount.dao.DiscountInfoDOMapper;
import com.xianzaishi.couponcenter.dal.discount.dataobject.DiscountInfoDO;
import com.xianzaishi.itemcenter.client.itemsku.dto.ItemCommoditySkuDTO;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 获取用户和活动对应关系，后续流水直接根据用户和活动关系降价
 * 
 * @author zhancang
 */
@Component(value = "getUserBuySpecialSkuDiscounRuleUnit")
public class GetUserBuySpecialSkuDiscounRuleUnit extends ProcessorUnit {

  @Autowired
  private DiscountInfoDOMapper discountInfoDOMapper;

  @Override
  public Result<Boolean> process(PromotionChainContext context) {
    Map<String, DiscountCalResultDTO> ffullNUseMDiscount = context.getFullNUseMDiscount();
    if (ffullNUseMDiscount == null) {
      ffullNUseMDiscount = new HashMap<>();
      context.setFullNUseMDiscount(ffullNUseMDiscount);
    }
    
    List<DiscountInfoDO> userDiscountInfoDO = context.getUserDiscountList();
    if (CollectionUtils.isEmpty(userDiscountInfoDO) || context.getSkuPriceMaps()== null || context.getSkuMaps() == null ) {
      Result.getSuccDataResult(true);
    }
    
    //获取现在的总金额
    BigDecimal totalPrice = new BigDecimal(String.valueOf(context.getSkuPriceMaps().get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0"))) ;
    Map<Long, ItemCommoditySkuDTO> skuM = context.getSkuMaps();
    
    for(DiscountInfoDO discountResult:userDiscountInfoDO){
      String rules = discountResult.getDicountRules();
      int threadhole = discountResult.getThreshold();//skuId;
      Long skuId = Long.valueOf(threadhole);
       
      ItemCommoditySkuDTO sku = skuM.get(skuId);
      if(null == sku){
        continue;
      }
      
      Map<String, Double> parameterM = Maps.newHashMap();
      parameterM.put("id", sku.getSkuId().doubleValue());
      String calResult = calculate(rules, parameterM);
      if(StringUtils.isEmpty(calResult) || !StringUtils.isNumeric(calResult.trim())){
        continue;
      }
      int discountCmpPirce = Integer.valueOf(calResult);
      if(discountCmpPirce == 0){//不会参与减到X元活动
        continue;
      }
      Integer oldPrice = sku.getDiscountUnitPrice();
      if(null == oldPrice){
        oldPrice = sku.getUnitPrice();
        if(oldPrice < 1){
          return Result.getSuccDataResult(true);
        }
      }
      Integer discountPrice = new BigDecimal(oldPrice).subtract(new BigDecimal(discountCmpPirce)).intValue();
      totalPrice = totalPrice.subtract(new BigDecimal(discountPrice));
      Map<Long, String> skuCount = getBuyCount(context);
      
      context.getSkuPriceMaps().put(PromotionChainContext.TOTAL_PRICE_PRE_KEY + "0", totalPrice.intValue());
      
      DiscountCalResultDTO result = calNUseMUnDiscount(discountResult,sku,skuCount.get(skuId),discountPrice);
      ffullNUseMDiscount.put(DiscountTypeEnum.SKU_DISCOUNT.getCode()+DiscountTypeEnum.SKU_DISCOUNT.getValue()+"_"+skuId,result);
    }
    return Result.getSuccDataResult(true);
  }

  /**
   * 输出优惠信息
   * @param discountInfoDO
   * @param itemCommoditySkuDTO
   * @param itemCount
   * @return
   */
  private DiscountCalResultDTO calNUseMUnDiscount(DiscountInfoDO discountInfoDO,
      ItemCommoditySkuDTO itemCommoditySkuDTO, String itemCount, int discount) {
    
    BigDecimal itemCountB = new BigDecimal(itemCount);
    BigDecimal itemPrice = new BigDecimal(String.valueOf(itemCommoditySkuDTO.getDiscountPrice()));
    // 还差多少件
    BigDecimal originPrice = itemPrice.multiply(itemCountB);

    DiscountCalResultDTO resultDTO = new DiscountCalResultDTO();
    resultDTO.setSkuId(itemCommoditySkuDTO.getSkuId());
    resultDTO.setItemId(itemCommoditySkuDTO.getItemId());
    // 转换数据
    DiscountInfoDTO discountInfoDTO = new DiscountInfoDTO();
    BeanCopierUtils.copyProperties(discountInfoDO, discountInfoDTO);
    resultDTO.setDiscountInfoDTO(discountInfoDTO);
    resultDTO.setGetDiscount(true);
    resultDTO.setDiscountPrice(discount);
    resultDTO.setItemCount(itemCount);
    resultDTO.setCurrPrice(originPrice.subtract(new BigDecimal(discount)).intValue());
    resultDTO.setOriginPrice(originPrice.intValue());
    
    return resultDTO;
  }
  
  private Map<Long, String> getBuyCount(PromotionChainContext context){
    Map<Long, String> skuCount = Maps.newHashMap();
    List<BuySkuInfoDTO> buySkuInfoDTOs = context.getBuyInfoList();
    for (BuySkuInfoDTO buySkuInfoDTO : buySkuInfoDTOs) {
      skuCount.put(buySkuInfoDTO.getSkuId(), buySkuInfoDTO.getBuyCount());
    }
    return skuCount;
  }
}
