package com.xianzaishi.couponcenter.component.promotion.unit;

import java.math.BigDecimal;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountDetailDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.component.promotion.PromotionChainContext;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 根据用户选择的优惠券给用户打折价格
 * 
 * @author zhancang
 */
@Component(value = "addCouponDiscountPriceUnit")
public class AddCouponDiscountPriceUnit extends ProcessorUnit {

  private static final String DISCOUNT_NAME = "满减优惠券";

  @Override
  public Result<Boolean> process(PromotionChainContext context) {

    Integer allPrice = null;
    Integer currentPrice = null;
    DiscountResultDTO discountResult = context.getDiscountResult();
    if (null == discountResult) {
      discountResult = new DiscountResultDTO();
      allPrice = context.getSkuPriceMaps().get(PromotionChainContext.TOTAL_PRICE_PRE_KEY + "total");
      currentPrice = allPrice;
    } else {
      allPrice = discountResult.getOrinalPrice();
      currentPrice = discountResult.getDiscountPrice();
      if (null == currentPrice) {
        currentPrice = allPrice;
      }
    }

    int totalDis = 0;// 所有优惠券折扣价格
    if (CollectionUtils.isNotEmpty(context.getUserSelectedCouponList())) {
      UserCouponDTO coupon = context.getUserSelectedCouponList().get(0);
      int discountpirce = calculate(coupon, context.getSkuPriceMaps(), 0);// 这里返回的是优惠多少钱，不是优惠后多少钱
      if (discountpirce > 0) {
        totalDis = totalDis + discountpirce;
      }
    }

    if (totalDis > 0) {
      // 设置一个总优惠明细
      DiscountDetailDTO disDetail = getDisDetail((currentPrice - totalDis), currentPrice);// 这里记录的是计算优惠的起始值，不是所有价格，会除掉已经使用过的优惠金额
      List<DiscountDetailDTO> discountList = discountResult.getTotalDiscount();
      if (null == discountList) {
        discountList = Lists.newArrayList();
      }
      discountList.add(disDetail);
      discountResult.setTotalDiscount(discountList);

      // 设置一个总优惠价格
      currentPrice = currentPrice - totalDis;
      discountResult.setDiscountPrice(currentPrice);// 记录总优惠价格，合并所有优惠
      discountResult.setOrinalPrice(discountResult.getOrinalPrice());
    }else{
      discountResult.setDiscountPrice(currentPrice);// 记录总优惠价格，合并所有优惠
      discountResult.setOrinalPrice(discountResult.getOrinalPrice());
    }
    context.setDiscountResult(discountResult);
    return Result.getSuccDataResult(true);
  }

  private DiscountDetailDTO getDisDetail(int disPrice, int orinalPrice) {
    DiscountDetailDTO disDetail = new DiscountDetailDTO();
    disDetail.setDiscountName(DISCOUNT_NAME);
    if (disPrice >= 0) {
      disDetail.setDiscountPrice(disPrice);
    } else {
      disDetail.setDiscountPrice(0);
    }
    disDetail.setDiscountName("优惠券优惠");
    disDetail.setIsGetDiscount(true);
    disDetail.setItemId(0L);
    disDetail.setNextDiscountDetail("");
    disDetail.setOriginPrice(orinalPrice);
    disDetail.setType((short) 1);
    return disDetail;
  }
}
