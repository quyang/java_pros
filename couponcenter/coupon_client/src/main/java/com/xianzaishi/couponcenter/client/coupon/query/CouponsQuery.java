package com.xianzaishi.couponcenter.client.coupon.query;

import com.xianzaishi.itemcenter.common.query.BaseQuery;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by quyang on 2017/5/5.
 */
public class CouponsQuery extends BaseQuery implements Serializable {


  private static final long serialVersionUID = 4846858054640801558L;
  /**
   * 优惠券id
   */
  private Long id;

  /**
   * 优惠券名称
   */
  private String title;

  /**
   * 开始时间
   */
  private Date gmtStart;

  /**
   * 截止时间
   */
  private Date gmtEnd;

  private Short type;

  /**
   * 渠道
   */
  private Short channelType;

  /**
   * 使用规则
   */
  private String rules;

  /**
   * 失效时长
   */
  private Integer gmtDayDuration;

  /**
   * INVALID("无效",(short)-1),
   INIT("初始化",(short)0),
   VALID("有效",(short)1);
   */
  private Short status;


  private Double amount;



  public Integer getGmtDayDuration() {
    return gmtDayDuration;
  }

  public void setGmtDayDuration(Integer gmtDayDuration) {
    this.gmtDayDuration = gmtDayDuration;
  }

  public String getRules() {
    return rules;
  }

  public void setRules(String rules) {
    this.rules = rules;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public Date getGmtStart() {
    return gmtStart;
  }

  public void setGmtStart(Date gmtStart) {
    this.gmtStart = gmtStart;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public static CouponsQuery queryById(Long couponId) {
    CouponsQuery query = new CouponsQuery();
    query.setId(couponId);
    return query;
  }

  public static CouponsQuery queryByTitle(String title) {
    CouponsQuery query = new CouponsQuery();
    query.setTitle(title);
    return query;
  }

  public static CouponsQuery queryByTitleAndId(Long couponId,String title) {
    CouponsQuery query = new CouponsQuery();
    query.setTitle(title);
    query.setId(couponId);
    return query;
  }

  public static CouponsQuery queryByStatus(Short status) {
    CouponsQuery query = new CouponsQuery();
    query.setStatus(status);
    return query;
  }
}
