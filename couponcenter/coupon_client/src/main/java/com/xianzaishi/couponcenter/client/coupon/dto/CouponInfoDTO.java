package com.xianzaishi.couponcenter.client.coupon.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by quyang on 2017/3/21.
 */
public class CouponInfoDTO implements Serializable {

  private static final long serialVersionUID = 1L;

  private Long id;

  private String title;

  private String rules;

  private String sendRules;

  private Long crowdId;

  private Short type;

  private Date gmtStart;

  private Date gmtEnd;

  private Integer gmtDayDuration;

  private Date gmtCreate;

  private Date gmtModify;

  private Short status;

  private Double amount;

  private Short channelType;

  private Double amountLimit;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getRules() {
    return rules;
  }

  public void setRules(String rules) {
    this.rules = rules;
  }

  public String getSendRules() {
    return sendRules;
  }

  public void setSendRules(String sendRules) {
    this.sendRules = sendRules;
  }

  public Long getCrowdId() {
    return crowdId;
  }

  public void setCrowdId(Long crowdId) {
    this.crowdId = crowdId;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Date getGmtStart() {
    return gmtStart;
  }

  public void setGmtStart(Date gmtStart) {
    this.gmtStart = gmtStart;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public Integer getGmtDayDuration() {
    return gmtDayDuration;
  }

  public void setGmtDayDuration(Integer gmtDayDuration) {
    this.gmtDayDuration = gmtDayDuration;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModify() {
    return gmtModify;
  }

  public void setGmtModify(Date gmtModify) {
    this.gmtModify = gmtModify;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Double getAmountLimit() {
    return amountLimit;
  }

  public void setAmountLimit(Double amountLimit) {
    this.amountLimit = amountLimit;
  }
}
