package com.xianzaishi.couponcenter.client.discount.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;

/**
 * 优惠服务队外总接口返回对象。三类优惠信息，分别是总价格和优惠价格，总的优惠列表(按照优惠类型归类后)，优惠明细（按照sku归类）
 * @author zhancang
 */
public class DiscountResultDTO implements Serializable {

  private static final long serialVersionUID = 6454608476272836212L;

  /**
   * 最终优惠后付款金额，第一类优惠信息
   */
  private Integer discountPrice;
  
  /**
   * 优惠前总价格,第一类优惠信息
   */
  private Integer orinalPrice;
  /**
   * 全部优惠,第二类优惠信息
   */
  private List<DiscountDetailDTO> totalDiscount;

  /**
   * 享受优惠明细，以skuId为key，第三类优惠信息
   */
  private Map<Long, List<DiscountDetailDTO>> discountDetailMap;

  /**
   * 距离下一优惠还差多少，以skuId为key
   */
  private Map<Long, List<DiscountDetailDTO>> nextDiscountDetailMap;

  /**
   * 当前购买情况还可享用优惠券列表
   */
  private List<UserCouponDTO> userCouponList;
  
  public List<DiscountDetailDTO> getTotalDiscount() {
    return totalDiscount;
  }

  public void setTotalDiscount(List<DiscountDetailDTO> totalDiscount) {
    this.totalDiscount = totalDiscount;
  }

  public Map<Long, List<DiscountDetailDTO>> getDiscountDetailMap() {
    return discountDetailMap;
  }

  public void setDiscountDetailMap(Map<Long, List<DiscountDetailDTO>> discountDetailMap) {
    this.discountDetailMap = discountDetailMap;
  }

  public Map<Long, List<DiscountDetailDTO>> getNextDiscountDetailMap() {
    return nextDiscountDetailMap;
  }

  public void setNextDiscountDetailMap(Map<Long, List<DiscountDetailDTO>> nextDiscountDetailMap) {
    this.nextDiscountDetailMap = nextDiscountDetailMap;
  }

  public List<UserCouponDTO> getUserCouponList() {
    return userCouponList;
  }

  public void setUserCouponList(List<UserCouponDTO> userCouponList) {
    this.userCouponList = userCouponList;
  }

  public Integer getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(Integer discountPrice) {
    this.discountPrice = discountPrice;
  }

  public Integer getOrinalPrice() {
    return orinalPrice;
  }

  public void setOrinalPrice(Integer orinalPrice) {
    this.orinalPrice = orinalPrice;
  }
}
