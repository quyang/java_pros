package com.xianzaishi.couponcenter.client.usercoupon.query;

import java.io.Serializable;
import java.util.List;

/**
 * 传值参数
 * 
 * @author zhancang
 */
public class SendParaDTO implements Serializable {

  private static final long serialVersionUID = 6026590001813723856L;

  /**
   * 用户id
   */
  private Long userId;
  
  /**
   * 按照礼包形式还是单个赠送，参考下面静态变量定义 QueryTypeConstants
   */
  private Integer sendType;

  /**
   * 按照礼包赠送时，赠送礼包id，礼包id为后台配置id
   */
  private Integer packageId;

  /**
   * 按照单个赠送时，赠送的优惠券id
   */
  private List<Long> couponIds;
  
  public Integer getPackageId() {
    return packageId;
  }

  public void setPackageId(Integer packageId) {
    this.packageId = packageId;
  }

  public List<Long> getCouponIds() {
    return couponIds;
  }

  public void setCouponIds(List<Long> couponIds) {
    this.couponIds = couponIds;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }
  
  public Integer getSendType() {
    return sendType;
  }

  public void setSendType(Integer sendType) {
    this.sendType = sendType;
  }

  /**
   * 和服务端交互方式
   * 
   * @author zhancang
   */
  public static class SendTypeConstants {

    /**
     * 按照优惠券列表发放
     */
    public static final Integer BY_COUPON_LIST = 0;

    /**
     * 按照配置好的优惠券包发放
     */
    public static final Integer BY_COUPON_PACKAGE = 1;
  }
}
