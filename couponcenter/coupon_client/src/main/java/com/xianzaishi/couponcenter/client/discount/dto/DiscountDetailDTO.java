package com.xianzaishi.couponcenter.client.discount.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 每一项优惠的详细描述信息
 * @author zhancang
 */
public class DiscountDetailDTO implements Serializable{

  private static final long serialVersionUID = -6976571339034893257L;

  /**
   * 优惠id，优惠券id或者其它优惠id，与type字段合并代表具体id含义
   */
  private Long discountId;
  /**
   * 优惠名称 表字段 : discount.name
   */
  private String discountName;

  /**
   * 0:订单总优惠，1：优惠券满减总优惠，2：商品满N减M总优惠，3:临期商品总优惠，12:商品满N减Msku级别优惠，13:临期商品单品优惠<br/>
   * 订单总优惠暂时无用，因为返回值是价格。
   */
  private Short type;

  /**
   * 使用渠道限制(0通用渠道,1线上渠道,2线下) 表字段 : discount.channel_type
   */
  private Short channelType;

  /**
   * 阈值，若是金额，则为金额阈值；若为件数，则为件数阈值；若为称重，则为称重阈值 单位是分，若为件数=实际件数*100 表字段 : discount_detail.threshold
   */
  private Integer threshold;

  /**
   * 优惠值，单位跟随阈值单位对应 单位是分，若为件数=实际件数*100；若为打折=折扣*100；如0.5*100。优惠后付款金额
   */
  private Integer discountPrice;

  /**
   * 优惠前的价格,符合优惠价格取值
   */
  private Integer originPrice;
  
  /**
   * 不计算优惠时的总价格， ignorePromotionPrice >= originPrice<br/>
   * 如果一个商品参与多个优惠，则每参与一次优惠originPrice会减去优惠金额，而ignorePromotionPrice始终保持总价<br/>
   * 只给单品优惠类型赋值，满减之类总订单级别的优惠无需赋值，这种情况下等于总订单原始价格
   */
  private Integer ignorePromotionPrice;
  
  /**
   * 已享受，或者即将享受**优惠
   */
  private Boolean isGetDiscount;

  /**
   * 距离下一优惠需要的条件描述
   */
  private String nextDiscountDetail;

  /**
   * 优惠对应skuId
   */
  private Long skuId;

  /**
   * 优惠对应商品id
   */
  private Long itemId;
  
  /**
   * 某些优惠情况下，一笔优惠对应多个优惠id，比如临期优惠
   */
  private List<Long> discountIds;
  
  public Long getDiscountId() {
    return discountId;
  }

  public void setDiscountId(Long discountId) {
    this.discountId = discountId;
  }

  public String getDiscountName() {
    return discountName;
  }

  public void setDiscountName(String discountName) {
    this.discountName = discountName;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Integer getThreshold() {
    return threshold;
  }

  public void setThreshold(Integer threshold) {
    this.threshold = threshold;
  }
  
  public Integer getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(Integer discountPrice) {
    this.discountPrice = discountPrice;
  }

  public Integer getOriginPrice() {
    return originPrice;
  }

  public void setOriginPrice(Integer originPrice) {
    this.originPrice = originPrice;
  }

  public Boolean getIsGetDiscount() {
    return isGetDiscount;
  }

  public void setIsGetDiscount(Boolean isGetDiscount) {
    this.isGetDiscount = isGetDiscount;
  }

  public String getNextDiscountDetail() {
    return nextDiscountDetail;
  }

  public void setNextDiscountDetail(String nextDiscountDetail) {
    this.nextDiscountDetail = nextDiscountDetail;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }

  public Integer getIgnorePromotionPrice() {
    return ignorePromotionPrice;
  }

  public void setIgnorePromotionPrice(Integer ignorePromotionPrice) {
    this.ignorePromotionPrice = ignorePromotionPrice;
  }

  public List<Long> getDiscountIds() {
    return discountIds;
  }

  public void setDiscountIds(List<Long> discountIds) {
    this.discountIds = discountIds;
  }
  
}
