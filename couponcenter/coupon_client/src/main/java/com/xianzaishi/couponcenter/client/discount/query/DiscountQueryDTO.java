package com.xianzaishi.couponcenter.client.discount.query;/**
 * Created by Administrator on 2016/12/27 0027.
 */

import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;

import java.io.Serializable;
import java.util.List;

/**
 * 查询所用
 *
 * @author jianmo
 * @create 2016-12-27 下午 4:36
 **/
public class DiscountQueryDTO implements Serializable {

    private static final long serialVersionUID = 5290586777439618186L;

    /**
     * 购买详情，skuid和sku数量pv对
     */
    private List<BuySkuInfoDTO> buySkuDetail;

    /**
     * 会员id
     */
    private Long userId;
    
    /**
     * 优惠券id
     */
    private Long couponId;
    
    /**
     * 支付渠道1代表线上，2代表线下
     */
    private Integer payType;
    
    private Integer discountBasicPrice;

    public List<BuySkuInfoDTO> getBuySkuDetail() {
        return buySkuDetail;
    }

    public void setBuySkuDetail(List<BuySkuInfoDTO> buySkuDetail) {
        this.buySkuDetail = buySkuDetail;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCouponId() {
      return couponId;
    }

    public void setCouponId(Long couponId) {
      this.couponId = couponId;
    }

    public Integer getPayType() {
      return payType;
    }

    public void setPayType(Integer payType) {
      this.payType = payType;
    }

    public Integer getDiscountBasicPrice() {
      return discountBasicPrice;
    }

    public void setDiscountBasicPrice(Integer discountBasicPrice) {
      this.discountBasicPrice = discountBasicPrice;
    }
}
