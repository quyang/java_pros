package com.xianzaishi.couponcenter.client.discount.dto;

import java.io.Serializable;

/**
 * 优惠计算结果信息
 *
 * @author jianmo
 * @create 2016-12-27 下午 4:32
 **/
public class DiscountCalResultDTO implements Serializable {

    private static final long serialVersionUID = 5290586777439618186L;

    private Long skuId;

    private Long itemId;

    /**
     * 可享受的优惠规则信息
     */
    private DiscountInfoDTO discountInfoDTO;

    /**
     *
     */
//    private double differencePrice;



    /**
     * 用户的实际件数或者是重量此值=实际件数*100
     */
    private String itemCount;

    /**
     * 计算时的原价
     */
    private int originPrice;

    /**
     *
     * 当isGetDiscount为true时，计算的优惠价格
     * 当isGetDiscount为false时，其有值。表示可享受的优惠的差价或者差件
     */
    private int discountPrice;

    /**
     * 主要是单个活动的优惠
     */
    private int singleDiscountPrice;

    /**
     * 总优惠价
     */
    private int totalDiscountPrice; 

    /**
     * 计算优惠之后的价格
     */
    private int currPrice;

    /**
     * 相关的描述
     */
    private String desc;


    /**
     * 是否获取到了优惠,默认没有享受优惠
     */
    private boolean isGetDiscount = false;

    public int getTotalDiscountPrice() {
        return totalDiscountPrice;
    }

    public void setTotalDiscountPrice(int totalDiscountPrice) {
        this.totalDiscountPrice = totalDiscountPrice;
    }

    public boolean isGetDiscount() {
        return isGetDiscount;
    }

    public void setGetDiscount(boolean getDiscount) {
        isGetDiscount = getDiscount;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemCount() {
        return itemCount;
    }

    public void setItemCount(String itemCount) {
        this.itemCount = itemCount;
    }

    public void setOriginPrice(int originPrice) {
        this.originPrice = originPrice;
    }


    public void setCurrPrice(int currPrice) {
        this.currPrice = currPrice;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getOriginPrice() {
        return originPrice;
    }

    public int getDiscountPrice() {
        return discountPrice;
    }

    public int getCurrPrice() {
        return currPrice;
    }

    public void setDiscountPrice(int discountPrice) {
//        if(!this.isGetDiscount) this.discountPrice = 0;
//        else
        this.discountPrice = discountPrice;
    }

    public DiscountInfoDTO getDiscountInfoDTO() {
        return discountInfoDTO;
    }

    public void setDiscountInfoDTO(DiscountInfoDTO discountInfoDTO) {
        this.discountInfoDTO = discountInfoDTO;
    }

    public int getSingleDiscountPrice() {
        return singleDiscountPrice;
    }

    public void setSingleDiscountPrice(int singleDiscountPrice) {
        this.singleDiscountPrice = singleDiscountPrice;
    }
}
