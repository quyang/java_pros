package com.xianzaishi.couponcenter.client.usercoupon;

import java.util.Collection;
import java.util.List;

import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDTO;
import com.xianzaishi.couponcenter.client.usercoupon.dto.UserCouponDiscontResultDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.QueryParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendItemCouponParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.SendParaDTO;
import com.xianzaishi.couponcenter.client.usercoupon.query.UserCouponDiscontQueryDTO;
import com.xianzaishi.itemcenter.common.result.Result;

public interface UserCouponService {
  
  /**
   * 根据用户优惠券ID获取优惠券详情
   * 
   * @param couponId
   * @return
   */
  public Result<UserCouponDTO> getUserCouponById(Long couponId);
  
  /**
   * 根据用户优惠券ID和订单ID获取已使用的优惠券详情
   * 
   * @param userId
   *  @param orderId
   * @return
   */
  public Result<UserCouponDTO> getUserCouponByOrderId(Long userId,Long orderId);
  
  /**
   * 查询用户优惠券，默认只查询有效优惠券，不包含过期和失效优惠券
   * @param para
   * @return
   */
  public Result<List<UserCouponDTO>> getUserCoupon(QueryParaDTO para);
  
  /**
   * 查询用户优惠券，默认只查询有效优惠券，不包含过期和失效优惠券
   * @param para
   * @return
   */
  public Result<Integer> countUserCoupon(QueryParaDTO para);
  
  /**
   * 查询商品优惠券，默认只查询有效优惠券，可包含过期和失效优惠券
   * @param para
   * @return
   */
  public Result<List<UserCouponDTO>> getItemCoupon(QueryParaDTO para);
  
  /**
   * 赠送用户优惠券,给用户添加指定优惠券
   * @param para
   * @return
   */
  public Result<Boolean> sendCoupon(SendParaDTO para);
  
  /**
   * 给商品添加优惠券，有优惠id的商品将享受购买优惠信息
   * @param para
   * @return
   */
  public Result<List<Long>> addItemCoupon(SendItemCouponParaDTO para);
  
  /**
   * 更新优惠券状态
   * @param para
   * @return
   */
  public Result<Boolean> updateCouponStatus(Long id, Short status);
  
  /**
   * 更新优惠券关联订单id
   * @param para
   * @return
   */
  public Result<Boolean> updateCouponOrder(Long id, Long orderId);

  /**
   * 根据用户当前交易状态，查询可用优惠，可查询一张也可查询多个
   * @param query
   * @return
   */
  public Result<UserCouponDiscontResultDTO> selectMathCoupon(UserCouponDiscontQueryDTO query);

  /**
   * 根据优惠计算优惠折扣
   * @param query
   * @return
   */
  public Result<UserCouponDiscontResultDTO> calculateDiscountPriceWithCoupon(UserCouponDiscontQueryDTO query);
  
  /**
   * 获取所有可用优惠券
   * 
   * @param couponTypes
   *            优惠券类型,不指定类型则返回所有类型优惠券
   * @return 优惠券详情
   */
  public Result<List<CouponDTO>> getCouponByTypes(Collection<Short> couponTypes);
  
  /**
   * 发放优惠券组
   * 
   * @param userId
   *            目标用户ID
   * @param couponType
   *            优惠券类型(11为新人大礼包)
   * @param duration
   *            优惠券有效持续时间毫秒,例如15天即为15 * 24 * 60 * 60 * 1000
   * @return true成功 false失败
   */
  @Deprecated
  public Result<Boolean> sendCouponPackage(long userId, short couponType, long duration);

  /**
   * 发放优惠券
   * 
   * @param userId
   *            目标用户ID
   * @param couponId
   *            优惠券ID
   * @param duration
   *            优惠券有效持续时间毫秒,例如15天即为15 * 24 * 60 * 60 * 1000
   * @return true成功 false失败
   */
  @Deprecated
  public Result<Boolean> sendCouponByCouponId(long userId, long couponId, long duration);
  
  /**
   * 获取用户优惠券
   * 
   * @param userId
   *            用户ID
   * @param couponTypes
   *            优惠券类型,不指定类型则返回所有类型优惠券
   * @return
   */
  @Deprecated
  public Result<List<UserCouponDTO>> getUserCouponByTypes(long userId, Collection<Short> couponTypes);
  
  /**
   * 根据优惠券id赠送优惠券
   * @param userId
   * @param couponIds
   * @param duration
   * @return
   */
  @Deprecated
  public Result<Boolean> sendCouponPackageByIds(long userId, Collection<Long> couponIds, long duration);

  /**
   * 获取所有用户优惠券
   *
   * @param userId
   *            用户ID
   * @return
   */
  public Result<List<UserCouponDTO>> getUserAllCouponByUserId(long userId);

}
