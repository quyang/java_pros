package com.xianzaishi.couponcenter.client.coupon;

import com.xianzaishi.couponcenter.client.coupon.dto.CouponDTO;
import com.xianzaishi.couponcenter.client.coupon.query.CouponsQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import java.util.Collection;
import java.util.List;


public interface CouponService {
  
  /**
   * 插入优惠券
   * @param couponDTO
   * @return 返回1 表示插入成功 -1 表示失败
   */
  Result<Long> insert(CouponDTO couponDTO);

  Result<Boolean> delete(long id);

  /**
   * 根据优惠券id查询优惠券
   * @param id
   * @return
   */
  Result<CouponDTO> getCouponById(long id);
  
  /**
   * 根据优惠券id列表查询优惠券
   * @param id
   * @return
   */
  Result<List<CouponDTO>> getCouponByIdList(List<Long> ids);

  Result<List<CouponDTO>> getCouponsByType(short type);
  
  Result<List<CouponDTO>> getCouponsByCouponTypes(Collection<Short> couponTypes);

  /**
   * 查询某个时间段内的优惠券
   * @param couponQuery
   * @return
   */
  Result<List<CouponDTO>> query(CouponsQuery couponQuery);


  /**
   * 更新优惠券
   * @param couponDTO
   * @return
   */
  Result<Boolean> update(CouponDTO couponDTO);

  /**
   * 查询某个时间段磊的优惠券数量
   * @param couponQuery
   * @return
   */
  Result<Integer> queryCount(CouponsQuery couponQuery);
}
