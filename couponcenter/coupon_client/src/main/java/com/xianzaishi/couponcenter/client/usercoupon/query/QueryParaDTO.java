package com.xianzaishi.couponcenter.client.usercoupon.query;

import java.io.Serializable;
import java.util.List;

/**
 * 传值参数
 * 
 * @author zhancang
 */
public class QueryParaDTO implements Serializable {

  private static final long serialVersionUID = 6026590001813723856L;

  private Long skuId;
  /**
   * 用户id
   */
  private Long userId;
  /**
   * 按照礼包形式还是单个赠送，参考下面静态变量定义 QueryTypeConstants
   */
  private Integer queryType;

  /**
   * 按照礼包赠送时，赠送礼包id，礼包id为后台配置id
   */
  private Integer packageId;

  /**
   * 按照单个赠送时，赠送的优惠券id
   */
  private List<Long> couponIds;
  
  /**
   * 按照订单id查询
   */
  private Long orderId;
  
  /**
   * 请求时是否包括所有数据,默认只查询有效期之内未使用数据
   */
  private Boolean includeAllCoupon;

  public Integer getPackageId() {
    return packageId;
  }

  public void setPackageId(Integer packageId) {
    this.packageId = packageId;
    this.queryType = QueryTypeConstants.BY_COUPON_PACKAGE;
  }

  public List<Long> getCouponIds() {
    return couponIds;
  }

  public void setCouponIds(List<Long> couponIds) {
    this.couponIds = couponIds;
    this.queryType = QueryTypeConstants.BY_COUPON_PACKAGE;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
    this.queryType = QueryTypeConstants.BY_COUPON_ORDER;
    this.setIncludeAllCoupon(true);
  }

  public Integer getQueryType() {
    return queryType;
  }

  public void setQueryType(Integer queryType) {
    this.queryType = queryType;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }
  
  public Boolean isIncludeAllCoupon() {
    return includeAllCoupon;
  }

  public void setIncludeAllCoupon(Boolean includeAllCoupon) {
    this.includeAllCoupon = includeAllCoupon;
  }
  
  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }


  /**
   * 和服务端交互方式
   * 
   * @author zhancang
   */
  public static class QueryTypeConstants {

    /**
     * 按照优惠券列表查询
     */
    public static final Integer BY_COUPON_LIST = 0;

    /**
     * 按照配置好的优惠券包查询
     */
    public static final Integer BY_COUPON_PACKAGE = 1;
    
    /**
     * 按照配置好的优惠券订单id查询
     */
    public static final Integer BY_COUPON_ORDER = 2;
  }
}
