package com.xianzaishi.couponcenter.client.tmporder;

import java.util.Map;

public interface OrderTmpService {

  public String getOrderInfo(Map<String,String> input);
}
