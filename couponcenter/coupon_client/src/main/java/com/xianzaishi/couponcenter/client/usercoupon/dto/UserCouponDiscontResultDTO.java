package com.xianzaishi.couponcenter.client.usercoupon.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author zhancang
 *
 */
public class UserCouponDiscontResultDTO implements Serializable{

  private static final long serialVersionUID = 5290586777439618186L;

  /**
   * 原始价格
   */
  private Integer originalPrice;
  
  /**
   * 被使用的优惠券id
   */
  private List<UserCouponDTO> usedCouponList;
  
  /**
   * 实际支付价格
   */
  private Integer actualPayPrice;
  
  /**
   * 折扣掉的价格
   */
  private Integer discountPrice;

  public Integer getOriginalPrice() {
    return originalPrice;
  }

  public void setOriginalPrice(Integer originalPrice) {
    this.originalPrice = originalPrice;
  }
  
  public List<UserCouponDTO> getUsedCouponList() {
    return usedCouponList;
  }

  public void setUsedCouponList(List<UserCouponDTO> usedCouponList) {
    this.usedCouponList = usedCouponList;
  }

  public Integer getActualPayPrice() {
    return actualPayPrice;
  }

  public void setActualPayPrice(Integer actualPayPrice) {
    this.actualPayPrice = actualPayPrice;
  }

  public Integer getDiscountPrice() {
    return discountPrice;
  }

  public void setDiscountPrice(Integer discountPrice) {
    this.discountPrice = discountPrice;
  }
}
