package com.xianzaishi.couponcenter.client.usercoupon.query;

import java.io.Serializable;

/**
 * 传值参数，针对设置商品优惠券时使用，商品优惠针对某个商品设置，对所有购买用户有效
 * 
 * @author zhancang
 */
public class SendItemCouponParaDTO implements Serializable {

  private static final long serialVersionUID = 6026590001813723856L;

  /**
   * 针对skuId
   */
  private Long skuId;
  
  /**
   * 限制优惠商品个数
   */
  private Integer limitCount;

  /**
   * 设置购买时优惠价格
   */
  private Integer buyPrice;

  /**
   * 优惠类型id
   */
  private Long couponId = 87L;
  
  /**
   * 优惠多少天，具体时间从优惠设置当天早上0点开始加×天
   */
  private Integer couponDayTime;

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Integer getLimitCount() {
    return limitCount;
  }

  public void setLimitCount(Integer limitCount) {
    this.limitCount = limitCount;
  }

  public Integer getBuyPrice() {
    return buyPrice;
  }

  public void setBuyPrice(Integer buyPrice) {
    this.buyPrice = buyPrice;
  }

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public Integer getCouponDayTime() {
    return couponDayTime;
  }

  public void setCouponDayTime(Integer couponDayTime) {
    this.couponDayTime = couponDayTime;
  }
}
