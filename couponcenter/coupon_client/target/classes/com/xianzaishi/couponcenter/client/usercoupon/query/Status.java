package com.xianzaishi.couponcenter.client.usercoupon.query;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author Croky.Zheng
 * 2016年7月16日
 */
public enum Status {
	INVALID("无效",(short)-1),
	INIT("初始化",(short)0),
	VALID("有效",(short)1);
	
	private String description;
	private short value;
	
	private Status(String description,short value) {
		this.description = description;
		this.value = value;
	}
	
	public static Status from(short value) {
		for (Status tmp : Status.values()) {
			if (tmp.getValue() == value) {
				return tmp;
			}
		}
		return null;
	}
	
	public static Map<Short,String> toMap() {
		Map<Short,String> statusMap = new LinkedHashMap<Short,String>();
		for (Status tmp : Status.values()) {
			statusMap.put(tmp.value, tmp.description);
		}
		return statusMap;
	}

	public String getDescription() {
		return description;
	}

	public short getValue() {
		return value;
	}
}
