package com.xianzaishi.couponcenter.client.discount.discountUtil;/**
 * Created by Administrator on 2016/12/29 0029.
 */


import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

/**
 * 优惠相关的工具
 *
 * @author jianmo
 * @create 2016-12-29 下午 8:46
 **/
public class DiscountUtils {

    /**
     * 将购买件数扩大一百倍，此处主要是用于优惠阈值的比较
     * @param buyCount
     * @return
     */
    public static int buyCountTo100(String buyCount){
        if(StringUtils.isEmpty(buyCount)) return 0;
        return new BigDecimal(buyCount).multiply(new BigDecimal("100")).intValue();
    }
}
