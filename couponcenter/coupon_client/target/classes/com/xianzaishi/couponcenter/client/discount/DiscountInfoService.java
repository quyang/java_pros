package com.xianzaishi.couponcenter.client.discount;

import java.util.List;
import java.util.Map;

import com.xianzaishi.couponcenter.client.discount.dto.DiscountCalResultDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountRefDTO;
import com.xianzaishi.couponcenter.client.discount.dto.DiscountResultDTO;
import com.xianzaishi.couponcenter.client.discount.query.DiscountQueryDTO;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 优惠活动对外暴露的接口
 *
 * @author jianmo
 * @create 2016-12-27 下午 3:22
 **/
public interface DiscountInfoService {

  /**
   * 查询和计算优惠信息，包括优惠券和默认商品优惠等
   * @param query
   * @return
   */
  public Result<DiscountResultDTO> calculate(DiscountQueryDTO query);
  
    /**
     * 满额减计算服务接口
     * @param query
     * @return
     */
    public Result<Map<String,DiscountCalResultDTO>> cal(DiscountQueryDTO query);
    
    /**
     * 更新关系状态
     * @param query
     * @return
     */
    Result<Boolean> update(DiscountRefDTO disocuntRel);
    
    /**
     * 更新用户关系状态
     * @param query
     * @return
     */
    Result<Boolean> updateByUserList(List<Integer> detailId,Long userId,Short status);
    
    /**
     * 插入或者更新关系状态，根据外部提供的关系列表，插入或者更新状态
     * @param query
     * @return
     */
    Result<Boolean> addOrUpdateRelation(List<DiscountRefDTO> disocuntRelList);

    /**
     * 移除相关context
     * @param uuid
     */
    public void removePromotionChainContext(Long uuid);

}
