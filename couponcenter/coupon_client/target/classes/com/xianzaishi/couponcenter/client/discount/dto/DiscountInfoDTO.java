package com.xianzaishi.couponcenter.client.discount.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 优惠的相关信息
 *
 * @author jianmo
 * @create 2016-12-27 下午 4:17
 **/
public class DiscountInfoDTO implements Serializable {

    private static final long serialVersionUID = 5290586777439618186L;

    /**
     * 优惠详情
     * 表字段 : discount_detail.id
     */
    private Integer discountDetailId;

    /**
     * 优惠名称
     * 表字段 : discount.name
     */
    private String discountName;

    /**
     * 优惠类型-（1、所有用户的优惠 2、订单优惠 3、所有商品优惠 4、指定商品的优惠（有外关联表） 5、指定店铺优惠（有外关联表））6、指定用户的优惠（有外关联表）
     * 表字段 : discount.type
     */
    private Short type;

    /**
     * 使用渠道限制(0通用渠道,1线上渠道,2线下)
     * 表字段 : discount.channel_type
     */
    private Short channelType;

    /**
     * 优惠活动开始时间
     * 表字段 : discount.gmt_start
     */
    private Date gmtStart;

    /**
     * 优惠活动结束时间
     * 表字段 : discount.gmt_end
     */
    private Date gmtEnd;

    /**
     * 优惠的状态
     * 表字段 : discount.status
     */
    private Short status;

    /**
     * 扩展标签
     * 表字段 : discount.exclude_tag
     */
    private String excludeTag;

    /**
     *
     * 表字段 : discount.gmt_create
     */
    private Date gmtCreate;

    /**
     *
     * 表字段 : discount.gmt_modify
     */
    private Date gmtModify;

    /**
     *
     * 表字段 : discount.created_by
     */
    private Long createdBy;

    /**
     * 优惠规则
     * 表字段 : discount_detail.dicount_rules
     */
    private String dicountRules;

    /**
     * 阈值，若是金额，则为金额阈值；若为件数，则为件数阈值；若为称重，则为称重阈值
     * 单位是分，若为件数=实际件数*100
     * 表字段 : discount_detail.threshold
     */
    private int threshold;

    /**
     * 优惠值，单位跟随阈值单位对应
     * 单位是分，若为件数=实际件数*100；若为打折=折扣*100；如0.5*100
     */
    private int discountValue;

    /**
     * 获取 优惠名称 字段:discount_detail.discount_name
     *
     * @return discount_detail.discount_name, 优惠名称
     */
    public String getDiscountName() {
        return discountName;
    }

    /**
     * 设置 优惠名称 字段:discount_detail.discount_name
     *
     * @param discountName the value for discount_detail.discount_name, 优惠名称
     */
    public void setDiscountName(String discountName) {
        this.discountName = discountName == null ? null : discountName.trim();
    }

    public Integer getDiscountDetailId() {
        return discountDetailId;
    }

    public void setDiscountDetailId(Integer discountDetailId) {
        this.discountDetailId = discountDetailId;
    }

    /**
     * 获取 优惠规则 字段:discount_detail.dicount_rules
     *
     * @return discount_detail.dicount_rules, 优惠规则
     */
    public String getDicountRules() {
        return dicountRules;
    }

    /**
     * 设置 优惠规则 字段:discount_detail.dicount_rules
     *
     * @param dicountRules the value for discount_detail.dicount_rules, 优惠规则
     */
    public void setDicountRules(String dicountRules) {
        this.dicountRules = dicountRules == null ? null : dicountRules.trim();
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    public int getDiscountValue() {
        return discountValue;
    }

    public void setDiscountValue(int discountValue) {
        this.discountValue = discountValue;
    }

    /**
     * 获取 优惠类型-（1、所有用户的优惠 2、订单优惠 3、所有商品优惠 4、指定商品的优惠（有外关联表） 5、指定店铺优惠（有外关联表））6、指定用户的优惠（有外关联表） 字段:discount.type
     *
     * @return discount.type, 优惠类型-（1、所有用户的优惠 2、订单优惠 3、所有商品优惠 4、指定商品的优惠（有外关联表） 5、指定店铺优惠（有外关联表））6、指定用户的优惠（有外关联表）
     */
    public Short getType() {
        return type;
    }

    /**
     * 设置 优惠类型-（1、所有用户的优惠 2、订单优惠 3、所有商品优惠 4、指定商品的优惠（有外关联表） 5、指定店铺优惠（有外关联表））6、指定用户的优惠（有外关联表） 字段:discount.type
     *
     * @param type the value for discount.type, 优惠类型-（1、所有用户的优惠 2、订单优惠 3、所有商品优惠 4、指定商品的优惠（有外关联表） 5、指定店铺优惠（有外关联表））6、指定用户的优惠（有外关联表）
     */
    public void setType(Short type) {
        this.type = type;
    }

    /**
     * 获取 使用渠道限制(0通用渠道,1线上渠道,2线下) 字段:discount.channel_type
     *
     * @return discount.channel_type, 使用渠道限制(0通用渠道,1线上渠道,2线下)
     */
    public Short getChannelType() {
        return channelType;
    }

    /**
     * 设置 使用渠道限制(0通用渠道,1线上渠道,2线下) 字段:discount.channel_type
     *
     * @param channelType the value for discount.channel_type, 使用渠道限制(0通用渠道,1线上渠道,2线下)
     */
    public void setChannelType(Short channelType) {
        this.channelType = channelType;
    }

    /**
     * 获取 优惠活动开始时间 字段:discount.gmt_start
     *
     * @return discount.gmt_start, 优惠活动开始时间
     */
    public Date getGmtStart() {
        return gmtStart;
    }

    /**
     * 设置 优惠活动开始时间 字段:discount.gmt_start
     *
     * @param gmtStart the value for discount.gmt_start, 优惠活动开始时间
     */
    public void setGmtStart(Date gmtStart) {
        this.gmtStart = gmtStart;
    }

    /**
     * 获取 优惠活动结束时间 字段:discount.gmt_end
     *
     * @return discount.gmt_end, 优惠活动结束时间
     */
    public Date getGmtEnd() {
        return gmtEnd;
    }

    /**
     * 设置 优惠活动结束时间 字段:discount.gmt_end
     *
     * @param gmtEnd the value for discount.gmt_end, 优惠活动结束时间
     */
    public void setGmtEnd(Date gmtEnd) {
        this.gmtEnd = gmtEnd;
    }

    /**
     * 获取 优惠的状态 字段:discount.status
     *
     * @return discount.status, 优惠的状态
     */
    public Short getStatus() {
        return status;
    }

    /**
     * 设置 优惠的状态 字段:discount.status
     *
     * @param status the value for discount.status, 优惠的状态
     */
    public void setStatus(Short status) {
        this.status = status;
    }

    /**
     * 获取 扩展标签 字段:discount.exclude_tag
     *
     * @return discount.exclude_tag, 扩展标签
     */
    public String getExcludeTag() {
        return excludeTag;
    }

    /**
     * 设置 扩展标签 字段:discount.exclude_tag
     *
     * @param excludeTag the value for discount.exclude_tag, 扩展标签
     */
    public void setExcludeTag(String excludeTag) {
        this.excludeTag = excludeTag == null ? null : excludeTag.trim();
    }

    /**
     * 获取  字段:discount.gmt_create
     *
     * @return discount.gmt_create,
     */
    public Date getGmtCreate() {
        return gmtCreate;
    }

    /**
     * 设置  字段:discount.gmt_create
     *
     * @param gmtCreate the value for discount.gmt_create,
     */
    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    /**
     * 获取  字段:discount.gmt_modify
     *
     * @return discount.gmt_modify,
     */
    public Date getGmtModify() {
        return gmtModify;
    }

    /**
     * 设置  字段:discount.gmt_modify
     *
     * @param gmtModify the value for discount.gmt_modify,
     */
    public void setGmtModify(Date gmtModify) {
        this.gmtModify = gmtModify;
    }

    /**
     * 获取  字段:discount.created_by
     *
     * @return discount.created_by,
     */
    public Long getCreatedBy() {
        return createdBy;
    }

    /**
     * 设置  字段:discount.created_by
     *
     * @param createdBy the value for discount.created_by,
     */
    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    @Deprecated
    private Integer discountId;
    @Deprecated
    private Date subGmtStart;
    @Deprecated
    private Date subGmtEnd;
    @Deprecated
    private Short subStatus;
    @Deprecated
    private String discountDetailName;
    
    @Deprecated
    public Integer getDiscountId() {
        return discountId;
    }

    @Deprecated
    public void setDiscountId(Integer discountId) {
        this.discountId = discountId;
    }

    @Deprecated
    public String getDiscountDetailName() {
        return discountDetailName;
    }

    @Deprecated
    public void setDiscountDetailName(String discountDetailName) {
        this.discountDetailName = discountDetailName;
    }
    @Deprecated
    public Date getSubGmtStart() {
        return subGmtStart;
    }

    @Deprecated
    public void setSubGmtStart(Date subGmtStart) {
        this.subGmtStart = subGmtStart;
    }

    @Deprecated
    public Date getSubGmtEnd() {
        return subGmtEnd;
    }

    @Deprecated
    public void setSubGmtEnd(Date subGmtEnd) {
        this.subGmtEnd = subGmtEnd;
    }

    @Deprecated
    public Short getSubStatus() {
        return subStatus;
    }

    @Deprecated
    public void setSubStatus(Short subStatus) {
        this.subStatus = subStatus;
    }
}
