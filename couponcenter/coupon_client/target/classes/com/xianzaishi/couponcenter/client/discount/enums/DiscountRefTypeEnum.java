package com.xianzaishi.couponcenter.client.discount.enums;/**
 * Created by Administrator on 2016/12/27 0027.
 */

/**
 * 优惠引用关系的类型
 *
 * @author jianmo
 * @create 2016-12-27 下午 4:27
 **/
public enum DiscountRefTypeEnum {

  REF_SKU("具体sku优惠","REF_SKU_",1),
  REF_USER("具体会员优惠","REF_USER_",2);
  private String name;
  private String code;
  private int value;
  DiscountRefTypeEnum(String name,String code, int value){
      this.code=code;
      this.name =name;
      this.value = value;
  }


  public String getName() {
      return name;
  }

  public void setName(String name) {
      this.name = name;
  }

  public int getValue() {
      return value;
  }

  public void setValue(int value) {
      this.value = value;
  }

  public String getCode() {
      return code;
  }

  public void setCode(String code) {
      this.code = code;
  }
}
