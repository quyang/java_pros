package com.xianzaishi.couponcenter.client.usercoupon.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 针对类型为1、11、77类型的优惠券，价格单位全部为元为单位的浮点数<br/>
 * 针对其它类型的优惠券，价格单位全部升级为分为单位的整数<br/>
 * 重写by@author dongpo
 *
 */
public class UserCouponDTO implements Serializable{
  private Long id;

  private Long userId;

  private Long couponId;

  private String couponTitle;

  private Date couponStartTime;

  private Date couponEndTime;

  private Long orderId;

  private String couponRules;
  
  private String sendRules;

  private Date gmtCreate;

  private Date gmtModify;

  /**
   * 0代表已经使用过，不可用<br/>
   * 1代表未使用，可使用优惠券<br/>
   */
  private Short status;

  /**
   * 抵扣金额 by zhancang
   */
  private Double amount;
  
  /**
   * 抵扣金额分为单位
   */
  private Integer amountCent;
  
  /**
   * 抵扣金额元为单位
   */
  private String amountYuanDescription;

  /**
   * type=1 看样子是现金券<br/>
   * type=77 早餐券<br/>
   * type=11 新人礼包券<br/>
   */
  private Short couponType;

  /**
   * channelType=1代表线上渠道<br/>
   * channelType=2代表线下渠道<br/>
   */
  private Short channelType;

  /**
   * 折扣使用前置价格条件 by zhancang
   */
  private Double amountLimit;
  
  /**
   * 折扣使用价格条件，分为单位
   */
  private Integer amountLimitCent;

  /**
   * 折扣使用价格条件，元为单位
   */
  private String amountLimitYuanDescription;
  
  private static final long serialVersionUID = 1L;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public String getCouponTitle() {
    return couponTitle;
  }

  public void setCouponTitle(String couponTitle) {
    this.couponTitle = couponTitle == null ? null : couponTitle.trim();
  }

  public Date getCouponStartTime() {
    return couponStartTime;
  }

  public void setCouponStartTime(Date couponStartTime) {
    this.couponStartTime = couponStartTime;
  }

  public Date getCouponEndTime() {
    return couponEndTime;
  }

  public void setCouponEndTime(Date couponEndTime) {
    this.couponEndTime = couponEndTime;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public String getCouponRules() {
    return couponRules;
  }

  public void setCouponRules(String couponRules) {
    this.couponRules = couponRules == null ? null : couponRules.trim();
  }

  public String getSendRules() {
    return sendRules;
  }

  public void setSendRules(String sendRules) {
    this.sendRules = sendRules;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModify() {
    return gmtModify;
  }

  public void setGmtModify(Date gmtModify) {
    this.gmtModify = gmtModify;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Double getAmount() {
    return amount;
  }

  public void setAmount(Double amount) {
    this.amount = amount;
  }

  public Short getCouponType() {
    return couponType;
  }

  public void setCouponType(Short couponType) {
    this.couponType = couponType;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  public Double getAmountLimit() {
    return amountLimit;
  }

  public void setAmountLimit(Double amountLimit) {
    this.amountLimit = amountLimit;
  }

  public Integer getAmountCent() {
    return amountCent;
  }

  public void setAmountCent(Integer amountCent) {
    this.amountCent = amountCent;
  }

  public Integer getAmountLimitCent() {
    return amountLimitCent;
  }

  public void setAmountLimitCent(Integer amountLimitCent) {
    this.amountLimitCent = amountLimitCent;
  }

  public String getAmountYuanDescription() {
    return amountYuanDescription;
  }

  public void setAmountYuanDescription(String amountYuanDescription) {
    this.amountYuanDescription = amountYuanDescription;
  }

  public String getAmountLimitYuanDescription() {
    return amountLimitYuanDescription;
  }

  public void setAmountLimitYuanDescription(String amountLimitYuanDescription) {
    this.amountLimitYuanDescription = amountLimitYuanDescription;
  }

  @Override
  public boolean equals(Object that) {
    if (this == that) {
      return true;
    }
    if (that == null) {
      return false;
    }
    if (getClass() != that.getClass()) {
      return false;
    }
    UserCouponDTO other = (UserCouponDTO) that;
    return (this.getId() == null ? other.getId() == null : this.getId().equals(other.getId()))
        && (this.getUserId() == null ? other.getUserId() == null : this.getUserId().equals(
            other.getUserId()))
        && (this.getCouponId() == null ? other.getCouponId() == null : this.getCouponId().equals(
            other.getCouponId()))
        && (this.getCouponTitle() == null ? other.getCouponTitle() == null : this.getCouponTitle()
            .equals(other.getCouponTitle()))
        && (this.getCouponStartTime() == null ? other.getCouponStartTime() == null : this
            .getCouponStartTime().equals(other.getCouponStartTime()))
        && (this.getCouponEndTime() == null ? other.getCouponEndTime() == null : this
            .getCouponEndTime().equals(other.getCouponEndTime()))
        && (this.getOrderId() == null ? other.getOrderId() == null : this.getOrderId().equals(
            other.getOrderId()))
        && (this.getCouponRules() == null ? other.getCouponRules() == null : this.getCouponRules()
            .equals(other.getCouponRules()))
        && (this.getGmtCreate() == null ? other.getGmtCreate() == null : this.getGmtCreate()
            .equals(other.getGmtCreate()))
        && (this.getGmtModify() == null ? other.getGmtModify() == null : this.getGmtModify()
            .equals(other.getGmtModify()))
        && (this.getStatus() == null ? other.getStatus() == null : this.getStatus().equals(
            other.getStatus()))
        && (this.getAmount() == null ? other.getAmount() == null : this.getAmount().equals(
            other.getAmount()))
        && (this.getCouponType() == null ? other.getCouponType() == null : this.getCouponType()
            .equals(other.getCouponType()))
        && (this.getChannelType() == null ? other.getChannelType() == null : this.getChannelType()
            .equals(other.getChannelType()))
        && (this.getAmountLimit() == null ? other.getAmountLimit() == null : this.getAmountLimit()
            .equals(other.getAmountLimit()))
        && (this.getAmountCent() == null ? other.getAmountCent() == null : this.getAmountCent()
            .equals(other.getAmountCent()))
        && (this.getAmountLimitCent() == null ? other.getAmountLimitCent() == null : this.getAmountLimitCent()
            .equals(other.getAmountLimitCent()))
            ;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
    result = prime * result + ((getUserId() == null) ? 0 : getUserId().hashCode());
    result = prime * result + ((getCouponId() == null) ? 0 : getCouponId().hashCode());
    result = prime * result + ((getCouponTitle() == null) ? 0 : getCouponTitle().hashCode());
    result =
        prime * result + ((getCouponStartTime() == null) ? 0 : getCouponStartTime().hashCode());
    result = prime * result + ((getCouponEndTime() == null) ? 0 : getCouponEndTime().hashCode());
    result = prime * result + ((getOrderId() == null) ? 0 : getOrderId().hashCode());
    result = prime * result + ((getCouponRules() == null) ? 0 : getCouponRules().hashCode());
    result = prime * result + ((getGmtCreate() == null) ? 0 : getGmtCreate().hashCode());
    result = prime * result + ((getGmtModify() == null) ? 0 : getGmtModify().hashCode());
    result = prime * result + ((getStatus() == null) ? 0 : getStatus().hashCode());
    result = prime * result + ((getAmount() == null) ? 0 : getAmount().hashCode());
    result = prime * result + ((getCouponType() == null) ? 0 : getCouponType().hashCode());
    result = prime * result + ((getChannelType() == null) ? 0 : getChannelType().hashCode());
    result = prime * result + ((getAmountLimit() == null) ? 0 : getAmountLimit().hashCode());
    result = prime * result + ((getAmountCent() == null) ? 0 : getAmountCent().hashCode());
    result = prime * result + ((getAmountLimitCent() == null) ? 0 : getAmountLimitCent().hashCode());
    return result;
  }
  
  /**
   * 优惠券是否以分为单位的价格单位
   * true:分为价格单位
   * false:元为价格单位
   * @return
   */
  public boolean isFenPriceTypeUserCoupon(){
    if(null == this.getCouponType()){
      throw new RuntimeException("Coupon type is error");
    }
    
    short couponType = this.getCouponType();
    if(couponType == 1 || couponType == 11 || couponType == 77 || couponType == 12){
      return false;
    }else{
      return true;
    }
  }

}
