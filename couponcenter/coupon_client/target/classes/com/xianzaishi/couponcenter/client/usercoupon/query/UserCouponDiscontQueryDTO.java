package com.xianzaishi.couponcenter.client.usercoupon.query;

import java.io.Serializable;
import java.util.List;

import com.xianzaishi.couponcenter.client.usercoupon.dto.BuySkuInfoDTO;

/**
 * 查询降价参数对象
 * 
 * @author zhancang
 */
public class UserCouponDiscontQueryDTO implements Serializable {

  private static final long serialVersionUID = 5290586777439618186L;

  private static final int PAY_ONLINE = 1;

  private static final int PAY_OFFLINE = 2;
  /**
   * 会员id
   */
  private Long userId;

  /**
   * 优惠券id
   */
  private Long couponId;

  /**
   * 查询数据方式 0:代表查询所有渠道可用优惠券 1:查询当前支付场景下可以用优惠
   */
  private Integer queryType;

  /**
   * 支付渠道1代表线上，2代表线下
   */
  private Integer payType;

  /**
   * 购买详情，skuid和sku数量pv对
   */
  private List<BuySkuInfoDTO> buySkuDetail;
  /**
   * 计算优惠时，并且直接标记为已使用
   */
  private boolean calculateAndUse;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public Integer getPayType() {
    return payType;
  }

  public void setPayType(Integer payType) {
    if (null == payType || (!(payType.equals(PAY_ONLINE) || payType.equals(PAY_OFFLINE)))) {
      throw new RuntimeException("Set pay type error");
    }
    this.payType = payType;
  }

  public List<BuySkuInfoDTO> getBuySkuDetail() {
    return buySkuDetail;
  }

  public void setBuySkuDetail(List<BuySkuInfoDTO> buySkuDetail) {
    this.buySkuDetail = buySkuDetail;
  }

  public Integer getQueryType() {
    return queryType;
  }

  public void setQueryType(Integer queryType) {
    this.queryType = queryType;
  }

  public boolean isCalculateAndUse() {
    return calculateAndUse;
  }

  public void setCalculateAndUse(boolean calculateAndUse) {
    this.calculateAndUse = calculateAndUse;
  }
}
