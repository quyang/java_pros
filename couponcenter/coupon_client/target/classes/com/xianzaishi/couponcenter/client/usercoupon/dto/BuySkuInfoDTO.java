package com.xianzaishi.couponcenter.client.usercoupon.dto;

import java.io.Serializable;
import java.util.List;

public class BuySkuInfoDTO implements Serializable {

  private static final long serialVersionUID = 6923493261995259712L;

  /**
   * skuId
   */
  private long skuId;

  /**
   * 购买的数量，称重类型的传入称重数量，g为单位
   */
  private String buyCount;

  /**
   * 传入商品条形码
   */
  private String skuBarCode;
  
  /**
   * 传入多个条形码
   */
  private List<String> skuBarCodeList;

  public long getSkuId() {
    return skuId;
  }

  public void setSkuId(long skuId) {
    this.skuId = skuId;
  }

  public String getBuyCount() {
    return buyCount;
  }

  public void setBuyCount(String buyCount) {
    this.buyCount = buyCount;
  }

  public String getSkuBarCode() {
    return skuBarCode;
  }

  public void setSkuBarCode(String skuBarCode) {
    this.skuBarCode = skuBarCode;
  }

  public List<String> getSkuBarCodeList() {
    return skuBarCodeList;
  }

  public void setSkuBarCodeList(List<String> skuBarCodeList) {
    this.skuBarCodeList = skuBarCodeList;
  }
}
