package com.xianzaishi.couponcenter.client.discount.enums;/**
 * Created by Administrator on 2016/12/27 0027.
 */

/**
 * 优惠类型
 *
 * @author jianmo
 * @create 2016-12-27 下午 4:26
 **/
public enum DiscountTypeEnum {
    FULL_REDUCE("满减","FULL_REDUCE_",1),
    FULL_N_USE_M("N件N折","FULL_N_USE_M_",2),
    SKU_DISCOUNT("减到X元","SKU_DISCOUNT_TO_",3);
    private String name;
    private String code;
    private int value;
    DiscountTypeEnum(String name,String code, int value){
        this.code=code;
        this.name =name;
        this.value = value;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
