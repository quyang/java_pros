package com.xianzaishi.couponcenter.client.discount.dto;

import java.io.Serializable;

/**
 * 优惠关系表，记录优惠和优惠对应实体关系
 * 
 * @author zhancang
 */
public class DiscountRefDTO implements Serializable {

  private static final long serialVersionUID = -7500840121601383877L;

  /**
   * 自增主键id
   */
  private Integer id;

  /**
   * 优惠id,与优惠详细主键一一对应，参考DiscountInfoDTO.discountDetailId字段
   */
  private Integer discountDetaiId;

  /**
   * 针对优惠的实体类型，参考 DiscountRefTypeEnum
   */
  private Short refType;

  private Long refId;

  /**
   * 状态情况，0代表不可用，状态，1代表可用状态
   */
  private Short refStatus;
  
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getDiscountDetaiId() {
    return discountDetaiId;
  }

  public void setDiscountDetaiId(Integer discountDetaiId) {
    this.discountDetaiId = discountDetaiId;
  }

  public Short getRefType() {
    return refType;
  }

  public void setRefType(Short refType) {
    this.refType = refType;
  }

  public Long getRefId() {
    return refId;
  }

  public void setRefId(Long refId) {
    this.refId = refId;
  }

  public Short getRefStatus() {
    return refStatus;
  }

  public void setRefStatus(Short refStatus) {
    this.refStatus = refStatus;
  }
  
}
